# Test schedule file for the parser of Molonglo automatic mode.
# Known good commands.
# 2014-2018 Fabian Jankowski
#
# The format is semicolon delimited and all RA and DEC are J2000 equatorial
# coordinates in hh:mm:ss notation.
#
# track; $raj; $decj
# posn; $raj; $decj
# fold; $jname; $tobs
# park
#

# track
track; 00:07:01.7;	+73:03:07.4
track; 0:07:01.7;	+73:03:07.4
track; 00:14:17.75;	+47:46:33.4
track; 00:23:16.89;	+09:23:24.18
track; 00:23:50.35311;	-72:04:31.4926
track; 00:24:13.87934;	-72:04:43.8405
track; 00:24:11.1036;	-72:05:20.1377
track; 00:24:03.8539;	-72:04:42.8065
track; 00:24:07.9587;	-72:04:39.6911
track; 00:24:06.7014;	-72:04:06.795
track; 23:13:08.6209;	+42:53:13.043
track; 23;	+58
track; 23:15;	+58
track; 23:16;	+64
track; 23:17:09.23723;	+14:39:31.220
track; 23:17:57.828;	+21:49:48.03

# posn
posn; 00:07:01.7;	+73:03:07.4
posn; 00:14:17.75;	+47:46:33.4
posn; 00:23:16.89;	+09:23:24.18
posn; 00:23:50.35311;	-72:04:31.4926
posn; 00:24:13.87934;	-72:04:43.8405
posn; 00:24:11.1036;	-72:05:20.1377
posn; 00:24:03.8539;	-72:04:42.8065
posn; 00:24:07.9587;	-72:04:39.6911
posn; 00:24:06.7014;	-72:04:06.795
posn; 23:13:08.6209;	+42:53:13.043
posn; 23:15;	+58
posn; 23:16;	+64
posn; 23:17:09.23723;	+14:39:31.220
posn; 23:17:57.828;	+21:49:48.03

# park
park

# indiv
indiv; tracking; J0835-4510; 300
indiv; transiting; J0953+0755; 3000.0
INDIV; stationary; J0953+0755; 1200.
InDiV; STationarY; J0953+0755; 30000.0000

# tb
tb; tracking; J0835-4510; 300
tb; transiting; J0953+0755; 3000.0
TB; stationary; J0953+0755; 1200.
Tb; STationarY; J0953+0755; 30000.0000

# tbsnr
tbsnr; tracking; J0835-4510; 15; 300; 1200
tbsNr; transiting; J0953+0755; 300.0; 3000.0; 4000.
TBSNR; staTionary; J0953+0755; 10; 1200.; 1800.0
TbSnr; STationarY; J0953+0755; 6.; 120.0; 30000.0000

# tbtoa
tbtoa; tracking; J0835-4510; 300; 1200
tbtOa; transiting; J0953+0755; 3000.0; 4000.
TBTOA; staTionary; J0953+0755; 1200.; 1800.0
TbToa; STationarY; J0953+0755; 120.0; 30000.0000

# modtb
modtb; tracking; J0835-4510; 300
modtb; transiting; J0953+0755; 3000.0
MODTB; stationary; J0953+0755; 1200.
ModTb; STationarY; J0953+0755; 30000.0000

# fold
fold; J0837-4135; 32
fold; J0953+0755; 3600.000
fold; J0742-2822; 3600
fold; J0837-4135; 3600
fold; J0953+0755; 3600
fold; J0953+0755; 3600
FOLD; J0953+0755; 3600
FoLd; J0953+0755; 3600

# foldsnr
foldsnr; J0837-4135; 5; 32; 120.0
foldsNr; J0953+0755; 15.0; 240.0; 3600.000
foldsnr; J0742-2822; 25.0; 60.0; 3600
foldsnr; J0837-4135; 40.; 5; 3600
foldsnr; J0953+0755; 150.12; 600; 3600
foldsnr; J0953+0755; 10; 50; 3600
FOLDSNR; J0953+0755; 50; 300; 3600
FoLdSnr; J0953+0755; 6; 120; 3600

# foldtoa
foldtoa; J0837-4135; 32; 120.0
foldtoa; J0953+0755; 240.0; 3600.000
foldToA; J0742-2822; 60.0; 3600
foldtoa; J0837-4135; 5; 3600
foldtoa; J0953+0755; 600; 3600
foldtoa; J0953+0755; 50; 3600
FOLDTOA; J0953+0755; 300; 3600
FoLdToa; J0953+0755; 120; 3600

# foldint
foldint; J0837-4135; 32; 120.0
foldINT; J0953+0755; 240.0; 3600.000
foldInT; J0742-2822; 60.0; 3600
foldint; J0837-4135; 5; 3600
foldint; J0953+0755; 600; 3600
foldiNT; J0953+0755; 50; 3600
FOLDINT; J0953+0755; 300; 3600.999
FoLdInt; J0953+0755; 120; 3600

# fb
fb; tracking; FRB Transit; 12:34:34.03; -56:17:19.123; 300
FB; Transiting; FRB120167; 23:34:34.03; +01:17:9.0; 3000.123
Fb; stationary; FRB_181119; 10:34:34.03; -34:17:9.; 4000.

# corr
corr; tracking; FRB Transit; 12:34:34.03; -56:17:19.123; 300
CORR; Transiting; FRB120167; 23:34:34.03; +01:17:9.0; 3000.123
Corr; stationary; FRB_181119; 10:34:34.03; -34:17:9.; 180.

# map
map; FRB Transit; 12:34:34.03; -56:17:19.123; 300
MAP; FRB120167; 23:34:34.03; +01:17:9.0; 3000.123
Map; CJ1218-4600; 10:34:34.03; -34:17:9.; 180.

# wait sec
wait; sec; 60.0
WAIT; SEC; 600.
Wait; SeC; 120.6

# wait lst
wait; lst; 12:34:56
WAIT; LST; 03:34:01
Wait; Lst; 00:34:06

# wait transit
wait; transit; J0835-4510; 0
WAIT; TRANSIT; J0835-4510; 4.0
Wait; Transit; J0835-4510; -1.0
