#
#   2018 Fabian Jankowski
#

import os

import numpy as np
from numpy.testing import assert_, assert_raises

from molsoft.schedule_helpers import load_schedule, test_schedule_file


KNOWN_GOOD = os.path.join(os.path.dirname(__file__), "test_schedule_good.sch")
KNOWN_BAD = os.path.join(os.path.dirname(__file__), "test_schedule_bad.sch")


def test_load_schedule():
    load_schedule(KNOWN_GOOD)
    with assert_raises(RuntimeError):
        load_schedule(KNOWN_BAD)


def test_test_schedule_file():
    test_schedule_file(KNOWN_GOOD)
    with assert_raises(RuntimeError):
        test_schedule_file(KNOWN_BAD)


def test_convert_to_schedule_line():
    sch = load_schedule(KNOWN_GOOD)

    for item in sch:
        try:
            item.make_schedule()
        except NotImplementedError:
            pass


def test_full_schedule_cycle():
    outfile = os.path.join(os.path.dirname(__file__), "temp_sched.sch")

    sched = load_schedule(KNOWN_GOOD)

    org_sch = []
    new_sch = []

    for item in sched:
        try:
            sitem = item.make_schedule()
        except NotImplementedError:
            pass
        else:
            org_sch.append(item)
            new_sch.append(sitem)

    with open(outfile, "w") as f:
        f.write("\n".join(new_sch))

    new_sch = load_schedule(outfile)

    for itemn, itemo in zip(new_sch, org_sch):
        assert_(itemn.make_schedule() == itemo.make_schedule())

    if os.path.isfile(outfile):
        os.remove(outfile)


# if run directly
if __name__ == "__main__":
    import pytest

    pytest.main([__file__])
