#
#   2018 Fabian Jankowski
#

import numpy as np
from numpy.testing import assert_, assert_raises

from molsoft.db_helpers import dtype_add_fields


def test_dtype_add_fields():
    d1 = np.dtype([("a", "float"), ("b", "|S256")])
    d2 = np.dtype([("c", "float"), ("d", "|S256")])
    d3 = np.dtype([("a", "float"), ("b", "|S256"), ("c", "float"), ("d", "|S256")])

    res = dtype_add_fields(d1, d2)

    assert_(sorted(res.names) == sorted(d3.names))
    assert_(res.str == d3.str)

    assert_(sorted(res.names) != sorted(d1.names))
    assert_(sorted(res.names) != sorted(d2.names))
    assert_(res.str != d1.str)
    assert_(res.str != d2.str)


# if run directly
if __name__ == "__main__":
    import pytest

    pytest.main([__file__])
