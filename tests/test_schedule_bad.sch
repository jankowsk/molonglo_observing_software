# Test schedule file for the parser of Molonglo automatic mode.
# 2014-2016 Fabian Jankowski
#
# The format is semicolon delimited and all RA and DEC are J2000 equatorial
# coordinates in hh:mm:ss notation.
#
# track; $raj; $decj
# posn; $raj; $decj
# fold; $jname; $tobs
# park
#

# bad ones - track
track; 23:13.21:08.6209;    +42:53:13.043
track; 23:13:08.6209;	    +42:53:13.
track; --3;	+58
track; jfasdk;	+64
track; 3

# bad ones - fold 
fold; J0837-4135
fold; J0837-413;	3600
fold; j0837-4135;	3600
fold; J08374135;	3600
fold; J083+4135;	3600
fold; J0953+0755;	3600.
fold; J0953+0755;	.3894
fold; J0953+0755;	AAA
fold; J0742-2822;	3600	m
