#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2014-2018 Fabian Jankowski
#   Sentinel for the automatic observing mode for Molonglo.
#

from __future__ import print_function
import argparse
import datetime
import logging
import os
import shlex
import signal
import subprocess
import sys
import time

import ephem

from molsoft.version import __version__


#
# set up logging
#

# root logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# log to file
try:
    logfile = logging.FileHandler(os.path.expanduser("~/schedule/logs/sentinel.log"))
except IOError as e:
    print("Could not enable logging to file: {0}".format(str(e)))
else:
    logfile.setLevel(logging.INFO)
    logfile_formatter = logging.Formatter("%(asctime)s: %(levelname)s: %(message)s")
    logfile.setFormatter(logfile_formatter)
    logger.addHandler(logfile)

# also log to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console_formatter = logging.Formatter("%(message)s")
console.setFormatter(console_formatter)
logger.addHandler(console)


# time zone the observatory is in
# Molonglo is in AEST = UTC+10
TIME_ZONE_OFFSET = 10

# do we observe Daylight Savings Time at the moment?
# for example: DST = 1 if yes
DST = 0


def local_time_to_utc(time):
    """
    Convert local time to UTC.

    Parameters
    ----------
    time : ephem.Date object
        Local time.

    Returns
    -------
    utc : ephem.Date object
        Local time converted to UTC.
    """

    utc = ephem.Date(time - (TIME_ZONE_OFFSET + DST) * ephem.hour)

    return utc


def stop_observation():
    """
    Stop the current automatic_mode process.
    Stop telescope tracking, backend etc.
    """

    # send INT/SIGINT signal to automatic mode process
    # SIGINT is signal number 2
    command = "pkill -2 -f automatic_mode.py"

    # split into correct tokens for Popen
    args = shlex.split(command)

    # spawn process
    subprocess.call(args)


def park_telescope():
    """
    Slew to park position.
    """

    command = "automatic_mode.py -p bla"

    # split into correct tokens for Popen
    args = shlex.split(command)

    # spawn process
    subprocess.call(args)


def shutdown_observing_system():
    """
    Shutdown the observing system.
    """

    command_tcc = "pkill -f tcc2.py"
    command_sched = "pkill -f automatic_mode.py"
    command_frog = "pkill -f frog.py"

    # split into correct tokens for Popen
    args_tcc = shlex.split(command_tcc)
    args_sched = shlex.split(command_sched)
    args_frog = shlex.split(command_frog)

    # spawn processes
    subprocess.call(args_frog)
    subprocess.call(args_sched)
    subprocess.call(args_tcc)


def signal_handler(signum, frame):
    """
    Handle unix signals sent to the program.
    """

    # treat SIGINT/INT/CRTL-C
    if signum == signal.SIGINT:
        logger.warn("SIGINT received, stopping the sentinel.")

        logger.info("Molonglo sentinel stopped.")
        sys.exit(1)


#
# MAIN
#


def main():
    # start signal handler
    signal.signal(signal.SIGINT, signal_handler)

    help_text = """
example usage:
  sentinel.py "2014/07/23 23:06:00"
"""

    # handle command line arguments
    parser = argparse.ArgumentParser(
        description="Sentinel for the automatic observing mode for Molonglo.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=help_text,
    )
    parser.add_argument("end_time", type=str, help="Local time stamp of end.")
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    print("Molonglo sentinel")
    print("=================")
    print()

    # try to parse date string
    try:
        end_time = ephem.Date(args.end_time)
    except ValueError as error:
        logger.error("Error, {0}".format(error))
        sys.exit(1)

    end_utc = local_time_to_utc(end_time)
    logger.info("End: {0} local time ({1} UTC)".format(end_time, end_utc))
    print()

    # do sanity checks
    if end_utc <= ephem.now():
        logger.error("Error, the end time must be in the future.")
        sys.exit(1)

    # main wait loop
    while True:
        now = ephem.now()
        time_left = datetime.timedelta(
            seconds=(end_utc.datetime() - now.datetime()).seconds
        )
        # the trailing comma has to be there
        # otherwise a newline is added automatically
        print("\r{0} UTC: Time left: {1} hh:mm:ss.".format(now, time_left), end=" ")

        if now >= end_utc:
            print()
            logger.info("Sentinel kicks in at {0} UTC".format(ephem.now()))
            logger.info("Stopping the observation.")
            stop_observation()
            time.sleep(2)
            logger.info("Parking the telescope.")
            park_telescope()
            time.sleep(2)
            logger.info("Shutting down the observing system.")
            shutdown_observing_system()
            break

        sys.stdout.flush()
        time.sleep(30)

    logger.info("Sentinel finished successfully.")


# if run directly
if __name__ == "__main__":
    main()
