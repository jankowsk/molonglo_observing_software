#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2014-2018 Fabian Jankowski
#   Display and change the software status for Molonglo.
#

from __future__ import print_function
import os.path
import shlex
import subprocess

# py2 vs py3
try:
    from Tkinter import Tk, Frame, StringVar, Label, Button
except ImportError:
    from tkinter import Tk, Frame, StringVar, Label, Button
import tkFileDialog
import tkMessageBox


class Application(Frame):
    """
    Software status GUI application.
    """

    def get_status(self):
        """
        Get the status of the observing system.
        """

        command_tcc = "pgrep -f tcc2.py"
        command_sched = "pgrep -f automatic_mode.py"
        command_frog = "pgrep -f frog.py"
        command_sentinel = "pgrep -f sentinel.py"

        # split into correct tokens for Popen
        args_tcc = shlex.split(command_tcc)
        args_sched = shlex.split(command_sched)
        args_frog = shlex.split(command_frog)
        args_sentinel = shlex.split(command_sentinel)

        proc_tcc = subprocess.Popen(args_tcc, stdout=subprocess.PIPE)
        proc_sched = subprocess.Popen(args_sched, stdout=subprocess.PIPE)
        proc_frog = subprocess.Popen(args_frog, stdout=subprocess.PIPE)
        proc_sentinel = subprocess.Popen(args_sentinel, stdout=subprocess.PIPE)

        proc_tcc.wait()
        proc_sched.wait()
        proc_frog.wait()
        proc_sentinel.wait()

        result_tcc = proc_tcc.communicate()
        result_sched = proc_sched.communicate()
        result_frog = proc_frog.communicate()
        result_sentinel = proc_sentinel.communicate()

        result_tcc = result_tcc[0]
        result_sched = result_sched[0]
        result_frog = result_frog[0]
        result_sentinel = result_sentinel[0]

        if result_tcc != "":
            self.tcc_running.set("running")
        elif result_tcc == "":
            self.tcc_running.set("stopped")

        if result_sched != "":
            self.sched_running.set("running")
        elif result_sched == "":
            self.sched_running.set("stopped")

        if result_frog != "":
            self.frog_running.set("running")
        elif result_frog == "":
            self.frog_running.set("stopped")

        if result_sentinel != "":
            self.sentinel_running.set("running")
        elif result_sentinel == "":
            self.sentinel_running.set("stopped")

        self.update_widgets()

        # reshedule event
        self.after(5000, self.get_status)

    def stop_system(self):
        """
        Stop the observing system.
        """

        result = tkMessageBox.askokcancel(
            "Confirmation", "Do you really want to stop the observing system?"
        )

        if result is True:
            command_tcc = "pkill -f tcc2.py"
            command_sched = "pkill -f automatic_mode.py"
            command_frog = "pkill -f frog.py"
            command_sentinel = "pkill -f sentinel.py"

            # split into correct tokens for Popen
            args_tcc = shlex.split(command_tcc)
            args_sched = shlex.split(command_sched)
            args_frog = shlex.split(command_frog)
            args_sentinel = shlex.split(command_sentinel)

            # spawn processes
            proc_tcc = subprocess.Popen(args_tcc, stdout=subprocess.PIPE)
            proc_sched = subprocess.Popen(args_sched, stdout=subprocess.PIPE)
            proc_frog = subprocess.Popen(args_frog, stdout=subprocess.PIPE)
            proc_sentinel = subprocess.Popen(args_sentinel, stdout=subprocess.PIPE)

            proc_tcc.wait()
            proc_sched.wait()
            proc_frog.wait()
            proc_sentinel.wait()

            result_tcc = proc_tcc.communicate()
            result_sched = proc_sched.communicate()
            result_frog = proc_frog.communicate()
            result_sentinel = proc_sentinel.communicate()

            result_tcc = result_tcc[0]
            result_sched = result_sched[0]
            result_frog = result_frog[0]
            result_sentinel = result_sentinel[0]

            self.get_status()

    def start_system(self):
        """
        Start the observing system.
        """

        command_tcc = "gnome-terminal -t 'TCC' -e 'python /home/dada/ewan/Soft/anansi_ver2.0/anansi/tcc2.py'"

        schedule_filename = self.schedule_filename.get()
        if schedule_filename != "None" and os.path.isfile(schedule_filename):
            command_frog = "gnome-terminal -t 'FROG' -e 'frog.py -s {0}'".format(
                schedule_filename
            )
        else:
            command_frog = "gnome-terminal -t 'FROG' -e 'frog.py'"

        # split into correct tokens for Popen
        args_tcc = shlex.split(command_tcc)
        args_frog = shlex.split(command_frog)

        # spawn processes
        subprocess.Popen(args_tcc, stdout=subprocess.PIPE)
        subprocess.Popen(args_frog, stdout=subprocess.PIPE)

        self.get_status()

    def update_widgets(self):
        """
        Update the information displayed in the widgets.
        """

        if self.tcc_running.get() == "stopped":
            self.status_tcc["bg"] = "red"
        elif self.tcc_running.get() == "running":
            self.status_tcc["bg"] = "green"

        if self.sentinel_running.get() == "stopped":
            self.status_sentinel["bg"] = "red"
        elif self.sentinel_running.get() == "running":
            self.status_sentinel["bg"] = "green"

        if self.sched_running.get() == "stopped":
            self.status_sched["bg"] = "red"
        elif self.sched_running.get() == "running":
            self.status_sched["bg"] = "green"

        if self.frog_running.get() == "stopped":
            self.status_frog["bg"] = "red"
        elif self.frog_running.get() == "running":
            self.status_frog["bg"] = "green"

    def create_widgets(self):
        """
        Populate the application window.
        """

        self.status_title = Label(self)
        self.status_title["text"] = "Status"
        self.status_title["padx"] = 10
        self.status_title["font"] = ("Helvetica", 16, "normal")
        self.status_title.grid(row=0, columnspan=4)

        self.TCC = Label(self)
        self.TCC["text"] = "TCC"
        self.TCC["width"] = 10
        self.TCC.grid(row=1, column=0)

        self.sched = Label(self)
        self.sched["text"] = "Scheduler"
        self.sched["width"] = 10
        self.sched.grid(row=1, column=1)

        self.frog = Label(self)
        self.frog["text"] = "FROG"
        self.frog["width"] = 10
        self.frog.grid(row=1, column=2)

        self.sentinel = Label(self)
        self.sentinel["text"] = "Sentinel"
        self.sentinel["width"] = 10
        self.sentinel.grid(row=1, column=3)

        self.status_tcc = Label(self)
        self.status_tcc["textvariable"] = self.tcc_running
        self.status_tcc["width"] = 10
        self.status_tcc.grid(row=2, column=0)

        self.status_sched = Label(self)
        self.status_sched["textvariable"] = self.sched_running
        self.status_sched["width"] = 10
        self.status_sched.grid(row=2, column=1)

        self.status_frog = Label(self)
        self.status_frog["textvariable"] = self.frog_running
        self.status_frog["width"] = 10
        self.status_frog.grid(row=2, column=2)

        self.status_sentinel = Label(self)
        self.status_sentinel["textvariable"] = self.sentinel_running
        self.status_sentinel["width"] = 10
        self.status_sentinel.grid(row=2, column=3)

        self.placeholder = Label(self)
        self.placeholder.grid(row=3, columnspan=4)

        self.schedule = Label(self)
        self.schedule["text"] = "Schedule file"
        self.schedule["font"] = ("Helvetica", 12, "normal")
        self.schedule.grid(row=4, column=1, columnspan=2)

        self.schedule_file = Label(self)
        self.schedule_file["textvariable"] = self.schedule_filename
        self.schedule_file["width"] = 40
        self.schedule_file["background"] = "White"
        self.schedule_file.grid(row=5, column=0, columnspan=4)

        self.schedule_file = Button(self)
        self.schedule_file["text"] = "Load schedule file"
        self.schedule_file["command"] = self.select_schedule_file
        self.schedule_file.grid(row=6, column=1, columnspan=2)

        self.placeholder1 = Label(self)
        self.placeholder1.grid(row=7, columnspan=4)

        self.start = Button(self)
        self.start["padx"] = 10
        self.start["pady"] = 4
        self.start["text"] = "Start the observing system"
        self.start["font"] = ("Helvetica", 12, "normal")
        self.start["fg"] = "dark green"
        self.start["command"] = self.start_system
        self.start.grid(row=8, columnspan=4)

        self.stop = Button(self)
        self.stop["padx"] = 10
        self.stop["pady"] = 4
        self.stop["text"] = "Stop the observing system"
        self.stop["font"] = ("Helvetica", 12, "normal")
        self.stop["fg"] = "red"
        self.stop["command"] = self.stop_system
        self.stop.grid(row=9, columnspan=4)

    def select_schedule_file(self):
        """
        Select filename of schedule file.
        """

        filename = tkFileDialog.askopenfilename(
            title="Open a schedule file",
            initialdir="/home/observer/schedule",
            filetypes=[("Schedules", ".sch"), ("All files", "*")],
        )

        if filename:
            self.schedule_filename.set(filename)

        self.get_status()

    def __init__(self, master=None):
        """
        Initialise the application.
        """

        Frame.__init__(self, master)

        self.tcc_running = StringVar()
        self.tcc_running.set("stopped")

        self.sentinel_running = StringVar()
        self.sentinel_running.set("stopped")

        self.sched_running = StringVar()
        self.sched_running.set("stopped")

        self.frog_running = StringVar()
        self.frog_running.set("stopped")

        self.schedule_filename = StringVar()
        self.schedule_filename.set("None")

        self["padx"] = 10
        self["pady"] = 10
        self.pack()
        self.create_widgets()
        self.update_widgets()


#
# MAIN
#


def main():
    root = Tk()
    app = Application(master=root)

    app.master.title("Swinburne Software Status")

    app.after(2000, app.get_status)
    app.mainloop()


# if run directly
if __name__ == "__main__":
    main()
