#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2014-2018 Fabian Jankowski
#   FROG for Molonglo.
#

from __future__ import print_function
import argparse
import logging
import os.path
import signal
import socket
import sys
import time

# py2 vs py3
try:
    import Queue
except ImportError:
    import queue as Queue

import ephem
from lxml import etree
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np

from molsoft.config_helpers import get_config
from molsoft.coord_helpers import (
    get_coordinate_list,
    get_ra_dec_list,
    compute_tilt_grid,
    update_target_tilt_grid,
    compute_trail_grid,
)
from molsoft.coords import hadec_to_nsew, nsew_to_hadec
from molsoft.schedule_helpers import load_schedule
from molsoft.version import __version__

# anansi
sys.path.append("/home/observer/TCC3_test")
try:
    from anansi.config import config
except ImportError:
    print("Could not import anansi.")


# pylint incorrectly identifies various numpy function to not return
# disable the corresponding pylint test for now
# pylint: disable=E1111

#
# set up logging
#

# root logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.propagate = False

# we want to have a clean root handler
for h in list(logger.handlers):
    logger.removeHandler(h)

# log to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console_formatter = logging.Formatter("%(message)s")
console.setFormatter(console_formatter)
logger.addHandler(console)

DD2R = np.pi / 180
R2S = 43200 / np.pi

CONFIG = get_config()

# coordinates of Molonglo
LAT_MOLONGLO = np.radians(CONFIG["observatory"]["lat"])
LON_MOLONGLO = np.radians(CONFIG["observatory"]["lon"])
ELEVATION_MOLONGLO = CONFIG["observatory"]["elevation"]
MAX_NS_MOLONGLO = CONFIG["observatory"]["max_ns"]
SOFT_NS_MOLONGLO = CONFIG["observatory"]["soft_ns"]
MAX_MD_MOLONGLO = CONFIG["observatory"]["max_md"]


class BaseConnection(object):
    def __init__(self, ip, port, sock_family, sock_type):
        super(BaseConnection, self).__init__()
        self.ip = ip
        self.port = port
        self.sock = socket.socket(sock_family, sock_type)
        self.sock.settimeout(2.0)

    def connect(self):
        self.sock.connect((self.ip, self.port))

    def bind(self):
        if self.sock is not None:
            self.sock.bind((self.ip, self.port))

    def close(self):
        self.__del__()

    def __del__(self):
        if self.sock is not None:
            try:
                self.sock.shutdown(socket.SHUT_RDWR)
            except Exception:
                pass
            try:
                self.sock.close()
            except Exception as error:
                logger.warning(error)


class TCPClient(BaseConnection):
    def __init__(self, ip, port, timeout=2.0):
        super(TCPClient, self).__init__(ip, port, socket.AF_INET, socket.SOCK_STREAM)

        self.sock.settimeout(timeout)
        self.connect()
        logger.debug("Connected TCP client to ({0}, {1})".format(self.ip, self.port))

    def send(self, msg):
        logger.debug(
            "Sending {0} bytes to ({1}, {2})".format(len(msg), self.ip, self.port)
        )
        self.sock.send(msg)

    def receive(self, n=4096):
        msg = self.sock.recv(n)
        logger.debug(
            "Received {0} bytes from ({1}, {2})".format(len(msg), self.ip, self.port)
        )

        return msg


def reduce_psr_list(psr_list, ra, dec, ra_lim=5, dec_lim=5):
    psr_ra_ephem = np.array(map(ephem.hours, psr_list["ra"]))
    psr_dec_ephem = np.array(map(ephem.degrees, psr_list["dec"]))
    ra_ephem = ephem.hours(ra)
    dec_ephem = ephem.degrees(dec)

    ra_lim = ra_lim * np.pi / 180 / np.cos(dec_ephem)
    dec_lim = dec_lim * np.pi / 180

    mask = (np.abs(psr_ra_ephem - ra_ephem) < ra_lim) & (
        np.abs(psr_dec_ephem - dec_ephem) < dec_lim
    )

    return mask


def get_status(status_ip, status_port):
    """
    Get telescope tracking status for TCC3 via TCP socket.

    Parameters
    ----------
    status_ip : str
        IP of the status reporting process.
    status_port : int
        Status port.

    Returns
    -------
    status : numpy rec or None
        This returns the telescope status of None if no connection to TCC3
        could be made.
    """

    raw_data = None
    status = None

    try:
        client = TCPClient(status_ip, status_port, timeout=10.0)
    except socket.error:
        logger.warn("Could not connect to the socket.")
    else:
        try:
            client.send("give me the status")
            raw_data = client.receive(4096)
        except socket.timeout:
            logger.warn("Could not read data from the socket.")
        except socket.error:
            logger.warn("Could not read data from the socket.")
        else:
            logger.debug("Raw data: {0}".format(raw_data))
            client.close()
            status = parse_status(raw_data)

    return status


def parse_status(raw_data):
    """
    Parse the tracking status returned from TCC.

    Parameters
    ----------
    raw_data : str
        The raw data transmitted from the status port.

    Returns
    -------
    status : numpy rec or None
        Status of the telescope. This returns None if the status of the arms
        could not be determined.
    """

    # initialise numpy record
    dtype = [
        ("utc", "float"),
        ("east_ns", "float"),
        ("east_md", "float"),
        ("east_ns_count", "int"),
        ("east_md_count", "int"),
        ("east_ns_driving", "|S32"),
        ("east_md_driving", "|S32"),
        ("east_ns_state", "|S32"),
        ("east_md_state", "|S32"),
        ("east_ns_on_target", "|S32"),
        ("east_md_on_target", "|S32"),
        ("east_ns_system_status", "int"),
        ("east_md_system_status", "int"),
        ("west_ns", "float"),
        ("west_md", "float"),
        ("west_ns_count", "int"),
        ("west_md_count", "int"),
        ("west_ns_driving", "|S32"),
        ("west_md_driving", "|S32"),
        ("west_ns_state", "|S32"),
        ("west_md_state", "|S32"),
        ("west_ns_on_target", "|S32"),
        ("west_md_on_target", "|S32"),
        ("west_ns_system_status", "int"),
        ("west_md_system_status", "int"),
        ("source_ns", "float"),
        ("source_md", "float"),
        ("source_ra", "|S32"),
        ("source_dec", "|S32"),
        ("source_glat", "float"),
        ("source_glon", "float"),
        ("source_alt", "float"),
        ("source_az", "float"),
    ]
    status = np.zeros(1, dtype=dtype)

    # utc time stamp
    status["utc"] = ephem.now()

    xml = ""

    if raw_data is not None:
        try:
            xml = etree.fromstring(raw_data)

            # east arm
            status["east_ns"] = (
                360.0
                / (2 * np.pi)
                * float(xml.find("ns").find("east").find("tilt").text)
            )
            status["east_md"] = (
                360.0
                / (2 * np.pi)
                * float(xml.find("md").find("east").find("tilt").text)
            )
            status["east_ns_count"] = int(
                xml.find("ns").find("east").find("count").text
            )
            status["east_md_count"] = int(
                xml.find("md").find("east").find("count").text
            )
            status["east_ns_driving"] = xml.find("ns").find("east").find("driving").text
            status["east_md_driving"] = xml.find("md").find("east").find("driving").text
            status["east_ns_state"] = xml.find("ns").find("east").find("state").text
            status["east_md_state"] = xml.find("md").find("east").find("state").text
            status["east_ns_on_target"] = (
                xml.find("ns").find("east").find("on_target").text
            )
            status["east_md_on_target"] = (
                xml.find("md").find("east").find("on_target").text
            )
            status["east_ns_system_status"] = (
                xml.find("ns").find("east").find("system_status").text
            )
            status["east_md_system_status"] = (
                xml.find("md").find("east").find("system_status").text
            )

            # west arm
            status["west_ns"] = (
                360.0
                / (2 * np.pi)
                * float(xml.find("ns").find("west").find("tilt").text)
            )
            status["west_md"] = (
                360.0
                / (2 * np.pi)
                * float(xml.find("md").find("west").find("tilt").text)
            )
            status["west_ns_count"] = int(
                xml.find("ns").find("west").find("count").text
            )
            status["west_md_count"] = int(
                xml.find("md").find("west").find("count").text
            )
            status["west_ns_driving"] = xml.find("ns").find("west").find("driving").text
            status["west_md_driving"] = xml.find("md").find("west").find("driving").text
            status["west_ns_state"] = xml.find("ns").find("west").find("state").text
            status["west_md_state"] = xml.find("md").find("west").find("state").text
            status["west_ns_on_target"] = (
                xml.find("ns").find("west").find("on_target").text
            )
            status["west_md_on_target"] = (
                xml.find("md").find("west").find("on_target").text
            )
            status["west_ns_system_status"] = (
                xml.find("ns").find("west").find("system_status").text
            )
            status["west_md_system_status"] = (
                xml.find("md").find("west").find("system_status").text
            )
        except etree.XMLSyntaxError as e:
            logger.error("Error, could not parse the XML: {0}".format(e))
            status = None
        except AttributeError as e:
            logger.error("Error, could not parse the XML: {0}".format(e))
            status = None
        except TypeError as e:
            logger.error("Error, could not parse the XML: {0}".format(e))
            status = None
        else:
            logger.info(
                "East arm tilt [NS, MD]: {0} {1}".format(
                    status["east_ns"][0], status["east_md"][0]
                )
            )
            logger.info(
                "West arm tilt [NS, MD]: {0} {1}".format(
                    status["west_ns"][0], status["west_md"][0]
                )
            )

            # source information
            try:
                status["source_ns"] = (
                    360.0 / (2 * np.pi) * float(xml.find("coordinates").find("NS").text)
                )
                status["source_md"] = (
                    360.0 / (2 * np.pi) * float(xml.find("coordinates").find("EW").text)
                )
                # ra and dec are strings in hhmmss
                # all others are float radians
                status["source_ra"] = xml.find("coordinates").find("RA").text
                status["source_dec"] = xml.find("coordinates").find("Dec").text
                status["source_glat"] = (
                    360.0
                    / (2 * np.pi)
                    * float(xml.find("coordinates").find("Glat").text)
                )
                status["source_glon"] = (
                    360.0
                    / (2 * np.pi)
                    * float(xml.find("coordinates").find("Glon").text)
                )
                status["source_alt"] = (
                    360.0
                    / (2 * np.pi)
                    * float(xml.find("coordinates").find("Alt").text)
                )
                status["source_az"] = (
                    360.0 / (2 * np.pi) * float(xml.find("coordinates").find("Az").text)
                )
            except AttributeError as e:
                logger.error("Error, could not parse the XML: {0}".format(e))
            except TypeError as e:
                logger.error("Error, could not parse the XML: {0}".format(e))
            else:
                logger.info(
                    "Source [NS, MD]: {0} {1}".format(
                        status["source_ns"][0], status["source_md"][0]
                    )
                )

    return status


def get_md_ns_object(name, up_only=True):
    """
    Compute NS and MD of various objects (Sun, Moon, etc.).

    Parameters
    ----------
    name : str
        Name of object.
    up_only : bool (optional)
        Get tilts only for objects that are currently up.

    Returns
    -------
    md_tilt : float or None
        MD tilt in degrees. Returns None if object is not up.
    ns_tilt : float or None
        NS tilt in degrees. Returns None if object is not up.

    Raises
    ------
    NotImplementedError
        If object is not implemented.
    """

    if name not in ["sun", "moon", "jupiter"]:
        raise NotImplementedError("Object not implemented. {0}".format(name))

    # date and time in UTC
    obs = ephem.Observer()
    obs.long = ephem.degrees(LON_MOLONGLO)
    obs.lat = ephem.degrees(LAT_MOLONGLO)
    obs.elevation = ELEVATION_MOLONGLO

    # switch between objects
    if name == "sun":
        source = ephem.Sun()
    elif name == "moon":
        source = ephem.Moon()
    elif name == "jupiter":
        source = ephem.Jupiter()

    source.compute(obs)

    # convert into md and ns tilt
    alt = source.alt

    # only calculate the position if source is up
    if up_only and alt < ephem.degrees(np.radians(0)):
        logger.debug("Source is not up: {0} {1}".format(source.ra, source.dec))
        md_tilt = None
        ns_tilt = None
    else:
        ha = obs.sidereal_time() - source.ra
        ns_tilt, md_tilt = hadec_to_nsew(ha, source.dec)

        # convert to degrees
        md_tilt = np.degrees(md_tilt)
        ns_tilt = np.degrees(ns_tilt)

    return md_tilt, ns_tilt


def plot_status(
    fig,
    data,
    tilt_grid,
    tilt_grid_tad,
    targets,
    psrs,
    psrs_tad,
    qsos,
    frbs,
    daemon=False,
    mode="frog",
):
    """
    Plot the telescope status.
    """

    if mode not in ["frog", "tadpole"]:
        logger.error("Plotting mode not known: {0}".format(mode))
        sys.exit(1)

    fig.clf()
    ax1 = fig.gca()

    # remove empty items from ring buffer
    data = data[np.logical_not(np.isnan(data["utc"]))]

    if len(data) > 0:
        alpha = 1.0 / np.logspace(0, len(data) - 1, num=len(data), base=2)

        # construct rgba array
        # blue
        blue = np.zeros((alpha.shape[0], 4))
        blue[:, 0] = 0
        blue[:, 1] = 0
        blue[:, 2] = 1
        blue[:, 3] = alpha
        # green
        green = np.zeros((alpha.shape[0], 4))
        green[:, 0] = 0
        green[:, 1] = 0.5
        green[:, 2] = 0
        green[:, 3] = alpha
        # red
        red = np.zeros((alpha.shape[0], 4))
        red[:, 0] = 1
        red[:, 1] = 0
        red[:, 2] = 0
        red[:, 3] = alpha

    obs = ephem.Observer()
    obs.lat = ephem.degrees(LAT_MOLONGLO)
    obs.long = ephem.degrees(LON_MOLONGLO)

    # plot tilt grid
    if mode == "frog":
        ax1.plot(tilt_grid["ew"], tilt_grid["ns"], color=(0.7, 0.7, 0.7), zorder=1)
    else:
        ax1.plot(
            tilt_grid_tad["ew"], tilt_grid_tad["ns"], color=(0.7, 0.7, 0.7), zorder=1
        )
        ax1.plot(tilt_grid["ew"], tilt_grid["ns"], color=(0.4, 0.4, 0.4), zorder=2)

    # plot dec labels
    for dec in [-80, -60, -40, -20, 0, +17]:
        ns = dec + 35.37
        ax1.text(
            0,
            ns,
            str(dec),
            horizontalalignment="center",
            verticalalignment="center",
            color="#222222",
            zorder=3,
        )

    ax1.text(
        30,
        36.5,
        "2hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )
    ax1.text(
        30,
        32.5,
        "W",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )
    ax1.text(
        60,
        36.5,
        "4hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )
    ax1.text(
        60,
        32.5,
        "W",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )

    ax1.text(
        -30,
        36.5,
        "2hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )
    ax1.text(
        -30,
        32.5,
        "E",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )
    ax1.text(
        -60,
        36.5,
        "4hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )
    ax1.text(
        -60,
        32.5,
        "E",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=3,
    )

    # plot sun's position
    md, ns = get_md_ns_object("sun")
    if md is not None and ns is not None:
        ax1.scatter(
            md,
            ns,
            color="orange",
            marker="$\\odot$",
            zorder=9,
            sizes=[
                96,
            ],
        )

    # plot moon's position
    md, ns = get_md_ns_object("moon")
    if md is not None and ns is not None:
        ax1.scatter(
            md,
            ns,
            color="blue",
            marker=r"$\u263e$",
            zorder=9,
            sizes=[
                96,
            ],
        )

    # plot jupiter's position
    md, ns = get_md_ns_object("jupiter")
    if md is not None and ns is not None:
        ax1.scatter(
            md,
            ns,
            color="green",
            marker=r"$\u2643$",
            zorder=9,
            sizes=[
                96,
            ],
        )

    # meridian line
    ax1.axvline(x=0.0, color="red", ls="dashed", zorder=2)

    # maximum zenith angle unreachable areas
    ax1.axhline(y=MAX_NS_MOLONGLO, color="red", zorder=10)
    ax1.text(
        0.0,
        MAX_NS_MOLONGLO + 0.5,
        "Max. zenith angle",
        color="red",
        horizontalalignment="center",
        verticalalignment="bottom",
        zorder=10,
    )
    ax1.axhspan(MAX_NS_MOLONGLO, 90.0, color="red", alpha=0.15, zorder=10)
    ax1.axhline(y=-MAX_NS_MOLONGLO, color="red", zorder=10)
    ax1.text(
        0.0,
        -MAX_NS_MOLONGLO - 0.5,
        "Max. zenith angle",
        color="red",
        horizontalalignment="center",
        verticalalignment="top",
        zorder=10,
    )
    ax1.axhspan(-MAX_NS_MOLONGLO, -90.0, color="red", alpha=0.15, zorder=10)

    # soft limits
    if SOFT_NS_MOLONGLO != 0:
        ax1.axhline(y=SOFT_NS_MOLONGLO, color="orange", zorder=10)
        ax1.text(
            0.0,
            SOFT_NS_MOLONGLO + 0.5,
            "Soft limit",
            color="orange",
            horizontalalignment="center",
            verticalalignment="bottom",
            zorder=10,
        )
        ax1.axhspan(SOFT_NS_MOLONGLO, 54.0, color="orange", alpha=0.15, zorder=10)
        ax1.axhline(y=-SOFT_NS_MOLONGLO, color="orange", zorder=10)
        ax1.text(
            0.0,
            -SOFT_NS_MOLONGLO - 0.5,
            "Soft limit",
            color="orange",
            horizontalalignment="center",
            verticalalignment="top",
            zorder=10,
        )
        ax1.axhspan(-SOFT_NS_MOLONGLO, -54.0, color="orange", alpha=0.15, zorder=10)

    # plot targets
    if targets is not None and len(targets) > 0:
        ax1.scatter(
            targets["md"],
            targets["ns"],
            color="gray",
            marker="o",
            label="Targets",
            zorder=5,
        )

        # plot name next to coordinate
        if mode == "frog":
            for item in targets:
                ax1.annotate(
                    "{0}".format(item["name"][0:5]),
                    xy=(item["md"], item["ns"]),
                    color="gray",
                    fontsize=8,
                    clip_on=True,
                    zorder=5,
                )

        elif mode == "tadpole":
            for item in targets:
                ax1.annotate(
                    "{0}".format(item["name"]),
                    xy=(item["md"], item["ns"]),
                    color="gray",
                    clip_on=True,
                    zorder=5,
                )

    # plot pulsars
    if psrs is not None and len(psrs) > 0:
        ax1.scatter(
            psrs["md"], psrs["ns"], color="gray", marker="o", label="pulsars", zorder=5
        )

        # plot name next to coordinate
        if mode == "frog":
            for item in psrs:
                ax1.annotate(
                    "{0}".format(item["name"][0:5]),
                    xy=(item["md"], item["ns"]),
                    color="gray",
                    fontsize=8,
                    clip_on=True,
                    zorder=5,
                )
        elif mode == "tadpole":
            for item in psrs_tad:
                ax1.annotate(
                    "{0}".format(item["name"]),
                    xy=(item["md"], item["ns"]),
                    color="gray",
                    fontsize=8,
                    clip_on=True,
                    zorder=5,
                )
                ax1.scatter(
                    psrs_tad["md"], psrs_tad["ns"], color="gray", marker="o", zorder=5
                )

    # plot qsos
    if qsos is not None and len(qsos) > 0:
        ax1.scatter(
            qsos["md"], qsos["ns"], color="blue", marker="p", label="qsos", zorder=5
        )

        # plot QSO name next to coordinate
        if mode == "frog":
            for item in qsos:
                ax1.annotate(
                    "{0}".format(item["name"][0:5]),
                    xy=(item["md"], item["ns"]),
                    color="blue",
                    fontsize=8,
                    clip_on=True,
                    zorder=5,
                )
        elif mode == "tadpole":
            for item in qsos:
                ax1.annotate(
                    "{0}".format(item["name"]),
                    xy=(item["md"], item["ns"]),
                    color="blue",
                    clip_on=True,
                    zorder=5,
                )

    # plot frbs
    if frbs is not None and len(frbs) > 0:
        ax1.scatter(
            frbs["md"], frbs["ns"], color="green", marker="p", label="frbs", zorder=5
        )

        # plot QSO name next to coordinate
        if mode == "frog":
            for item in frbs:
                ax1.annotate(
                    "{0}".format(item["name"]),
                    xy=(item["md"], item["ns"]),
                    color="green",
                    fontsize=8,
                    clip_on=True,
                    zorder=5,
                )
        elif mode == "tadpole":
            for item in frbs:
                ax1.annotate(
                    "{0}".format(item["name"]),
                    xy=(item["md"], item["ns"]),
                    color="green",
                    clip_on=True,
                    zorder=5,
                )

    # initialize
    tad_center_x = 0.0
    tad_center_y = 0.0

    # plot locations of arms and source
    if data is not None and len(data) > 0:
        ax1.scatter(
            data["east_md"],
            data["east_ns"],
            s=100,
            facecolor="None",
            edgecolor=green,
            marker="o",
            label="East arm",
            zorder=9,
        )

        ax1.scatter(
            data["west_md"],
            data["west_ns"],
            s=100,
            facecolor="None",
            edgecolor=blue,
            marker="x",
            label="West arm",
            zorder=9,
        )

        east_arm = patches.Ellipse(
            xy=(data["east_md"][0], data["east_ns"][0]),
            width=4.0,
            height=2.0,
            facecolor="green",
            edgecolor="black",
            alpha=0.1,
            label="East arm",
            zorder=8,
        )
        ax1.add_patch(east_arm)

        west_arm = patches.Ellipse(
            xy=(data["west_md"][0], data["west_ns"][0]),
            width=4.0,
            height=2.0,
            facecolor="blue",
            edgecolor="black",
            alpha=0.1,
            label="East arm",
            zorder=8,
        )
        ax1.add_patch(west_arm)

        ax1.scatter(
            data["source_md"],
            data["source_ns"],
            s=50,
            facecolor=red,
            edgecolor=red,
            marker="p",
            label="Source",
            zorder=10,
        )

        # location of tadpole box
        # md
        if (
            data["east_md_state"][0] != "disabled"
            and data["west_md_state"][0] != "disabled"
        ):
            tad_center_x = np.mean([data["east_md"][0], data["west_md"][0]])
        elif (
            data["east_md_state"][0] == "disabled"
            and data["west_md_state"][0] != "disabled"
        ):
            tad_center_x = data["west_md"][0]
        elif (
            data["east_md_state"][0] != "disabled"
            and data["west_md_state"][0] == "disabled"
        ):
            tad_center_x = data["east_md"][0]
        else:
            tad_center_x = 0.0

        # ns
        if (
            data["east_ns_state"][0] != "disabled"
            and data["west_ns_state"][0] != "disabled"
        ):
            tad_center_y = np.mean([data["east_ns"][0], data["west_ns"][0]])
        elif (
            data["east_ns_state"][0] == "disabled"
            and data["west_ns_state"][0] != "disabled"
        ):
            tad_center_y = data["west_ns"][0]
        elif (
            data["east_ns_state"][0] != "disabled"
            and data["west_ns_state"][0] == "disabled"
        ):
            tad_center_y = data["east_ns"][0]
        else:
            tad_center_y = 0.0

        ax1.add_patch(
            patches.Rectangle(
                (tad_center_x - 4.0, tad_center_y - 4.0),
                8.0,
                8.0,
                fill=False,
                edgecolor="#800000",
                zorder=10,
            )
        )

    # tweak the plot
    ax1.grid(True)
    ax1.set_xlabel("MD tilt [degrees]")
    ax1.set_ylabel("NS tilt [degrees]")

    if mode == "frog":
        ax1.set_xlim(68, -68)
        ax1.set_ylim(-70, 70)
    elif mode == "tadpole":
        xmin = tad_center_x - 4.02
        xmax = tad_center_x + 4.02
        ymin = tad_center_y - 4.02
        ymax = tad_center_y + 4.02

        ax1.set_xlim(xmax, xmin)
        ax1.set_ylim(ymin, ymax)

        # info bar on right side - everything in one text string
        # use of colours in info bar
        info_text = "UTC {0}\
                \nLMST {1}".format(
            ephem.now(), obs.sidereal_time()
        )
        fig.text(
            0.72,
            0.95,
            info_text,
            horizontalalignment="left",
            verticalalignment="top",
            color="black",
        )

        # info bar on right side - source (red)
        info_text = "Source"
        fig.text(
            0.72,
            0.88,
            info_text,
            horizontalalignment="left",
            verticalalignment="top",
            color="red",
        )

        # info bar on right side - east arm (green text)
        info_text = "East arm"
        fig.text(
            0.72,
            0.55,
            info_text,
            horizontalalignment="left",
            verticalalignment="top",
            color="green",
        )

        # info bar on right side - west arm (blue text)
        info_text = "West arm"
        fig.text(
            0.72,
            0.28,
            info_text,
            horizontalalignment="left",
            verticalalignment="top",
            color="blue",
        )

        if data is not None and len(data) > 0:
            # info bar on right side - source data (black)
            info_text = "RA: {0}\nDEC: {1}\
                    \nGlat: {2:.3f}\nGlon: {3:.3f}\
                    \nNS: {4:.3f}\nMD: {5:.3f}\
                    \nAlt: {6:.3f}\nAz: {7:.3f}".format(
                data["source_ra"][0],
                data["source_dec"][0],
                data["source_glat"][0],
                data["source_glon"][0],
                data["source_ns"][0],
                data["source_md"][0],
                data["source_alt"][0],
                data["source_az"][0],
            )

            fig.text(
                0.72,
                0.84,
                info_text,
                horizontalalignment="left",
                verticalalignment="top",
            )

            # info bar on right side - east arm (black text)
            info_text = "NS: {0:.3f} {1}\
                        \ndriving: {2}\
                        \non target: {3}\
                        \nMD: {4:.3f} {5}\
                        \ndriving: {6}\
                        \non target: {7}".format(
                data["east_ns"][0],
                data["east_ns_state"][0],
                data["east_ns_driving"][0],
                data["east_ns_on_target"][0],
                data["east_md"][0],
                data["east_md_state"][0],
                data["east_md_driving"][0],
                data["east_md_on_target"][0],
            )

            fig.text(
                0.72,
                0.51,
                info_text,
                horizontalalignment="left",
                verticalalignment="top",
                color="black",
            )

            # info bar on right side - west arm (black text)
            info_text = "NS: {0:.3f} {1}\
                    \ndriving: {2}\
                    \non target: {3}\
                    \nMD: {4:.3f} {5}\
                    \ndriving: {6}\
                    \non target: {7}".format(
                data["west_ns"][0],
                data["west_ns_state"][0],
                data["west_ns_driving"][0],
                data["west_ns_on_target"][0],
                data["west_md"][0],
                data["west_md_state"][0],
                data["west_md_driving"][0],
                data["west_md_on_target"][0],
            )

            fig.text(
                0.72,
                0.24,
                info_text,
                horizontalalignment="left",
                verticalalignment="top",
                color="black",
            )

    # fig.savefig("position_{0}.svg".format(mode), bbox_inches="tight")

    if not daemon:
        fig.canvas.draw()


def signal_handler(signum, frame):
    """
    Handle unix signals sent to the program.
    """

    # treat SIGINT/INT/CRTL-C
    if signum == signal.SIGINT:
        logger.warn("SIGINT received, stopping Molonglo FROG.")

        logger.info("Molonglo FROG stopped.")
        sys.exit(1)


def get_targets(schedule):
    """
    Get targets from schedule.
    """

    temp = []

    for item in schedule:
        if item.obs_mode in ["INDIV", "TB", "TBSNR", "TBTOA", "TBINT"]:
            temp.append(item)

    dtype = [
        ("nr", "int"),
        ("name", "|S16"),
        ("ra", "|S32"),
        ("dec", "|S32"),
        ("md", "float"),
        ("ns", "float"),
        ("is_up", "bool"),
    ]
    targets = np.zeros(len(temp), dtype=dtype)

    targets["nr"] = xrange(len(temp))

    for i in xrange(len(temp)):
        targets["name"][i] = temp[i].name
        targets["ra"][i] = temp[i].ra
        targets["dec"][i] = temp[i].dec

    targets = np.sort(targets, order="name")

    return targets


def load_frb_data(filename):
    """
    Load the FRB data from file.

    Parameters
    ----------
    filename : str
        Filename of file with FRB data.

    Returns
    -------
    data : numpy rec
        FRB data.
    """

    # sanity check that file exists
    if not os.path.isfile(filename):
        logger.error("The file does not exist: {0}".format(filename))
        sys.exit(1)

    dtype = [
        ("name", "|S32"),
        ("ra", "|S32"),
        ("dec", "|S32"),
        ("md", "float"),
        ("ns", "float"),
        ("is_up", "bool"),
    ]

    d = np.loadtxt(filename, dtype=str)
    data = np.zeros(len(d), dtype=dtype)

    for i in range(len(d)):
        data[i]["name"] = d[i][0]
        data[i]["ra"] = d[i][1]
        data[i]["dec"] = d[i][2]

    return data


def load_qso_data(filename):
    """
    Load the quasar calibrator data from file.

    Parameters
    ----------
    filename : str
        Filename of file with QSO data.

    Returns
    -------
    data : numpy rec
        QSO data.
    """

    # sanity check that file exists
    if not os.path.isfile(filename):
        logger.error("The file does not exist: {0}".format(filename))
        sys.exit(1)

    dtype = [
        ("ra", "|S32"),
        ("dec", "|S32"),
        ("flux_mjy", "float"),
        ("flux", "float"),
        ("size", "|S32"),
        ("comment", "|S128"),
    ]

    try:
        data = np.genfromtxt(
            filename, dtype=dtype, delimiter="\t", autostrip=True, comments="#"
        )
    except ValueError as e:
        logger.error("Could not parse the qso file: {0}. {1}".format(filename, e))
        return None

    dtype = [
        ("name", "|S32"),
        ("ra", "|S32"),
        ("dec", "|S32"),
        ("flux_mjy", "float"),
        ("flux", "float"),
        ("size", "|S32"),
        ("comment", "|S128"),
        ("md", "float"),
        ("ns", "float"),
        ("is_up", "bool"),
    ]

    new_data = np.zeros(len(data), dtype=dtype)

    # copy data
    for field_name in data.dtype.names:
        new_data[field_name] = data[field_name]

    # construct names from ra, dec
    for i in xrange(len(new_data)):
        ra = new_data["ra"][i].split(":")
        dec = new_data["dec"][i].split(":")
        new_data["name"][i] = "C{0}{1}{2}{3}".format(ra[0], ra[1], dec[0], dec[1])

    # use only the ones that have a flux density above 5 Jy
    new_data = new_data[new_data["flux"] >= 5.0]

    new_data = np.sort(new_data, order="name")

    return new_data


def load_psr_data(filename):
    """
    Load the pulsar data from file.
    The input file is a list of jnames.

    Parameters
    ----------
    filename : str
        Filename of file with PSR data.

    Returns
    -------
    data : numpy rec
        PSR data.
    """

    if not os.path.isfile(filename):
        logger.error("The psr file does not exist: {0}".format(filename))
        sys.exit(1)

    dtype = [("name", "|S32")]

    names = np.genfromtxt(filename, dtype=dtype, comments="#")

    dtype = [
        ("name", "|S32"),
        ("ra", "|S32"),
        ("dec", "|S32"),
        ("md", "float"),
        ("ns", "float"),
        ("is_up", "bool"),
    ]

    data = np.zeros(len(names), dtype=dtype)

    coord_list = get_coordinate_list()

    for i in xrange(len(names)):
        data["name"][i] = names["name"][i]
        ra, dec = get_ra_dec_list(coord_list, data["name"][i])
        data["ra"][i] = ra
        data["dec"][i] = dec

    data = np.sort(data, order="name")

    return data


#
# MAIN
#


def main():
    # start signal handler
    signal.signal(signal.SIGINT, signal_handler)

    # handle command line arguments
    parser = argparse.ArgumentParser(description="Molonglo FROG.")
    parser.add_argument(
        "-s",
        "--schedule",
        dest="schedule_filename",
        type=str,
        default=None,
        help="Filename of schedule file to process.",
    )
    parser.add_argument(
        "-d",
        "--daemon",
        dest="daemon",
        action="store_true",
        default=False,
        help="Run non-interactive without XWindow. Just produce png files.",
    )
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    print("Molonglo FROG")
    print("=============")

    # IP and port configuration
    # TCC3 uses TCP status connection
    tcc_status = config.status_server
    status_ip = tcc_status.ip
    status_port = tcc_status.port

    if args.daemon:
        logger.info("Running as daemon.")

    targets = None
    if args.schedule_filename is not None:
        schedule = load_schedule(args.schedule_filename)
        targets = get_targets(schedule)
        print("Targets: {0}".format(targets))

    # load FROG pulsars from file
    psr_file = "/home/observer/schedule/psr_list_526included.txt"
    psrs = None
    if os.path.isfile(psr_file):
        psrs = load_psr_data(psr_file)

    # load Tadpole pulsars from file
    psr_file_tad = "/home/observer/schedule/psr_list_big_526included.txt"
    psrs_tad_all = None
    if os.path.isfile(psr_file_tad):
        psrs_tad_all = load_psr_data(psr_file_tad)

    # load calibrators from file
    qso_file = "/home/observer/schedule/qso_list.txt"
    qsos = None
    if os.path.isfile(qso_file):
        qsos = load_qso_data(qso_file)

    # load FRBs from file
    frb_file = "/home/observer/schedule/FRBs_list.txt"
    frbs = None
    if os.path.isfile(frb_file):
        frbs = load_frb_data(frb_file)

    # compute trail grid
    grid_psrs = compute_trail_grid(psrs)
    grid_psrs_tad = compute_trail_grid(psrs_tad_all)
    grid_qsos = compute_trail_grid(qsos)
    grid_frbs = compute_trail_grid(frbs)

    if targets is not None:
        grid_targets = compute_trail_grid(targets)

    # compute the tilt grids
    tilt_grid = compute_tilt_grid()
    tilt_grid_tad = compute_tilt_grid(tadpole=True)

    plt.ion()

    # frog window
    fig1 = plt.figure()
    fig1.subplots_adjust(left=0.09, right=0.96, top=0.96, bottom=0.08)
    fig1.canvas.set_window_title("FROG")
    fig1.add_subplot(111)

    # tadpole window
    fig2 = plt.figure()
    fig2.subplots_adjust(left=0.09, right=0.7, top=0.96, bottom=0.08)
    fig2.canvas.set_window_title("TADPOLE")
    fig2.add_subplot(111)

    # create ring buffer
    # length of the ring buffer
    # buf_length = 120
    buf_length = 20
    step = 1
    dtype = [
        ("utc", "float"),
        ("east_ns", "float"),
        ("east_md", "float"),
        ("east_ns_count", "int"),
        ("east_md_count", "int"),
        ("east_ns_driving", "|S32"),
        ("east_md_driving", "|S32"),
        ("east_ns_state", "|S32"),
        ("east_md_state", "|S32"),
        ("east_ns_on_target", "|S32"),
        ("east_md_on_target", "|S32"),
        ("east_ns_system_status", "int"),
        ("east_md_system_status", "int"),
        ("west_ns", "float"),
        ("west_md", "float"),
        ("west_ns_count", "int"),
        ("west_md_count", "int"),
        ("west_ns_driving", "|S32"),
        ("west_md_driving", "|S32"),
        ("west_ns_state", "|S32"),
        ("west_md_state", "|S32"),
        ("west_ns_on_target", "|S32"),
        ("west_md_on_target", "|S32"),
        ("west_ns_system_status", "int"),
        ("west_md_system_status", "int"),
        ("source_ns", "float"),
        ("source_md", "float"),
        ("source_ra", "|S32"),
        ("source_dec", "|S32"),
        ("source_glat", "float"),
        ("source_glon", "float"),
        ("source_alt", "float"),
        ("source_az", "float"),
    ]
    ring_buf = np.zeros(buf_length, dtype=dtype)

    # set utc to None as default
    ring_buf["utc"] = None

    start_status = time.time()
    end_status = start_status + 2.0

    # update window very often
    # but get status only every 2 seconds to ease the load on tcc
    psrs_tad = psrs_tad_all
    molonglo = ephem.Observer()
    molonglo.lat = ephem.degrees(-(35 + 22 / 60.0 + 14.5452 / 3600.0) * DD2R)
    molonglo.long = ephem.degrees((149 + 25 / 60.0 + 28.7682 / 3600.0) * DD2R)
    frog_timer = 0

    while True:
        if time.time() >= end_status:
            status = get_status(status_ip, status_port)

            if status is not None:
                # rotate the buffer
                ring_buf = np.roll(ring_buf, step, axis=0)

                # insert new data a the top
                ring_buf[0] = status[0]

                logger.debug(ring_buf)

            # update the locations
            if targets is not None:
                targets = update_target_tilt_grid(targets, grid_targets, ephem.now())

            if psrs is not None and time.time() - frog_timer > 10:
                psrs = update_target_tilt_grid(psrs, grid_psrs, ephem.now())
                frog_timer = time.time()

            if psrs_tad_all is not None:
                # current_position = get_current_position(status[0])
                av_ns = np.mean([status[0]["east_ns"], status[0]["west_ns"]])
                av_md = np.mean([status[0]["east_md"], status[0]["west_md"]])
                ha, dec = nsew_to_hadec(av_ns * np.pi / 180.0, av_md * np.pi / 180.0)

                molonglo.date = ephem.now()
                ra = (ephem.hours(molonglo.sidereal_time()) + ephem.hours(ha)) % (
                    2 * np.pi
                )

                reduced_mask = reduce_psr_list(psrs_tad_all, ra, dec)
                psrs_tad = update_target_tilt_grid(
                    psrs_tad_all[reduced_mask], grid_psrs_tad, ephem.now()
                )
            else:
                psrs_tad = psrs_tad_all

            if qsos is not None:
                qsos = update_target_tilt_grid(qsos, grid_qsos, ephem.now())

            if frbs is not None:
                frbs = update_target_tilt_grid(frbs, grid_frbs, ephem.now())

            # reschedule
            start_status = time.time()
            end_status = start_status + 2.0

        plot_status(
            fig1,
            ring_buf,
            tilt_grid,
            tilt_grid_tad,
            targets,
            psrs,
            psrs_tad,
            qsos,
            frbs,
            args.daemon,
            "frog",
        )
        plot_status(
            fig2,
            ring_buf,
            tilt_grid,
            tilt_grid_tad,
            targets,
            psrs,
            psrs_tad,
            qsos,
            frbs,
            args.daemon,
            "tadpole",
        )

        # time.sleep(2)


# if run directly
if __name__ == "__main__":
    main()
