#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2015-2018 Fabian Jankowski
#   Make a schedule for Molonglo.
#

from __future__ import print_function
import argparse
from datetime import datetime, timedelta
import logging
import signal
import sys

import ephem
import numpy as np

from molsoft.config_helpers import get_config
from molsoft.db_helpers import get_observations, get_obs_strategy, obs_strat_to_schedule
from molsoft.version import __version__


# pylint incorrectly identifies various numpy function to not return
# disable the corresponding pylint test for now
# pylint: disable=E1111

CONFIG = get_config()
UTC_FORMAT = CONFIG["general"]["utc_format"]

# coordinates of Molonglo
LAT_MOLONGLO = np.radians(CONFIG["observatory"]["lat"])
LON_MOLONGLO = np.radians(CONFIG["observatory"]["lon"])
ELEVATION_MOLONGLO = CONFIG["observatory"]["elevation"]


def signal_handler(signum, frame):
    """
    Handle unix signals sent to the program.
    """

    # treat SIGINT/INT/CRTL-C
    if signum == signal.SIGINT:
        logging.warn("SIGINT received, stopping the program.")

        logging.info("Program stopped.")

        sys.exit(0)


def get_last_observed(data, source_name):
    """
    Determine when a source was last observed.

    Parameters
    ----------
    data : numpy rec
        Data from the observation database.
    source_name : str
        Name of the source.

    Returns
    -------
    age : timedelta or None
        Time since the last observation of an object and now as timedelta
        object. If the object was never observed, return None.
    """

    if source_name not in data["source_name"]:
        return None

    # sort data by utc
    data = np.sort(data, order="utc")

    observations = data[data["source_name"] == source_name]

    if not len(observations) > 1:
        return None

    latest_utc = observations["utc"][-1]
    age = datetime.utcnow() - datetime.strptime(latest_utc, UTC_FORMAT)

    return age


def output_schedule_file(schedule):
    """
    Output the schedule file to disk.

    Parameters
    ----------
    schedule : list
        List of SchedItems.
    """

    filename = "schedule.sch"

    with open(filename, "w") as f:
        # header
        f.write("# schedule generated using make_schedule.py script\n")
        f.write("# on {0} local time\n".format(datetime.now()))
        f.write("\n")

        for item in schedule:
            f.write(item.make_schedule())
            f.write("\n")

    print("The schedule was written to file: {0}".format(filename))


def sort_schedule(schedule, offset):
    """
    Sort the schedule to be applicable for the current LST.
    Sort it by increasing RA after the current LST.

    Parameters
    ----------
    schedule : list
        List of SchedItem.
    offset : float
        Offset in hours to the current LST.

    Returns
    -------
    schedule
        Sorted schedule.
    """

    # select start lst/ra
    obs = ephem.Observer()
    obs.long = ephem.degrees(LON_MOLONGLO)
    obs.lat = ephem.degrees(LAT_MOLONGLO)
    obs.elevation = ELEVATION_MOLONGLO
    lst = obs.sidereal_time()

    print("Current LMST: {0}".format(lst))

    start = ephem.hours(lst + offset * 2.0 * np.pi / 24.0)

    if start < 0:
        start = ephem.hours(start + 2.0 * np.pi)

    print("Requested start: {0}".format(start))

    def sort_function(item):
        """
        Sort by distance to the start lst.
        For past ras observe them in the future.
        """

        dist = ephem.hours(ephem.hours(item.ra) - start)
        if dist < 0:
            dist = ephem.hours(dist + ephem.hours("24:00:00"))

        logging.debug("{0} {1}".format(dist, item.name))

        return dist

    schedule = sorted(schedule, key=sort_function)

    return schedule


def limit_observation_time(schedule, hours):
    """
    Limit the schedule to X hours of observation time.
    It ensures that the schedule tobs will always be less than X hours.

    Parameters
    ----------
    schedule
        Schedule file.
    hours : float
        Number of hours of tobs.
    """

    total_tobs = 0
    new_tobs = 0
    limited_schedule = []

    for item in schedule:
        if item.obs_mode in ["TB", "TBSNR", "TBTOA", "TBINT"]:
            new_tobs = item.tmax

        if (total_tobs + new_tobs) / 3600.0 >= hours:
            print("Total observation time: {0:.2f} hours".format(total_tobs / 3600.0))
            break
        else:
            total_tobs += new_tobs
            limited_schedule.append(item)

    return limited_schedule


#
# MAIN
#


def main():
    # start signal handler
    signal.signal(signal.SIGINT, signal_handler)

    # set up logging
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")

    # handle command line arguments
    parser = argparse.ArgumentParser(description="Make a schedule file for Molonglo.")
    parser.add_argument(
        "--hours",
        type=float,
        default=24.0,
        help="Number of hours the schedule should run for (default: 24 hours).",
    )
    parser.add_argument(
        "--offset",
        type=float,
        default=-1.0,
        help="Offset in hours to the current LMST as start of the schedule "
        + "(default: -1.0 h).",
    )
    parser.add_argument(
        "--daily_only",
        dest="daily_only",
        action="store_true",
        default=False,
        help="Generate the schedule from the pulsars in the daily and glitch_programme "
        + "selection only (default: False).",
    )
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    print("Schedule maker for Molonglo")
    print("===========================")
    print()

    strategy = get_obs_strategy()
    data = get_observations()

    schedule = []

    if args.daily_only:
        mask = (strategy["programme"] == "daily") | (strategy["programme"] == "glitch")
        strategy = strategy[mask]

    for item in strategy:
        age = get_last_observed(data, item["psrj"])
        if age is None or age >= timedelta(days=item["cadence"]):
            schedule.append(item)
            print("Observe: {0}, age: {1}".format(item["psrj"], age))

    # convert to list of SchedItem
    schedule = obs_strat_to_schedule(schedule)

    print()
    schedule = sort_schedule(schedule, args.offset)
    schedule = limit_observation_time(schedule, args.hours)
    print("{0} lines in the schedule.".format(len(schedule)))

    print()

    output_schedule_file(schedule)

    print("All done.")


# if run directly
if __name__ == "__main__":
    main()
