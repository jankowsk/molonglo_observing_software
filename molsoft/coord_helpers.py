#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2016-2018 Fabian Jankowski
#   Coordinate helpers for Molonglo.
#

from __future__ import print_function
import logging
import os.path
import re
import shlex
import subprocess
import tempfile

import ephem
import numpy as np

from molsoft.config_helpers import get_config
from molsoft.coords import hadec_to_nsew, nsew_of_constant_ha, nsew_of_constant_dec


# pylint incorrectly identifies various numpy function to not return
# disable the corresponding pylint test for now
# pylint: disable=E1111

# set up logging
logger = logging.getLogger("automatic_mode." + __name__)
logger.setLevel(logging.INFO)

CONFIG = get_config()

# coordinates of Molonglo
LAT_MOLONGLO = np.radians(CONFIG["observatory"]["lat"])
LON_MOLONGLO = np.radians(CONFIG["observatory"]["lon"])
ELEVATION_MOLONGLO = CONFIG["observatory"]["elevation"]
MAX_NS_MOLONGLO = CONFIG["observatory"]["max_ns"]
MAX_MD_MOLONGLO = CONFIG["observatory"]["max_md"]


def compute_tilt_grid(tadpole=False):
    """
    Compute the NS, EW tilt grid for Molonglo.

    Parameters
    ----------
    tadpole : bool
        Determine whether a finer grid for Tadpole should be generated.

    Returns
    -------
    tilt_grid : numpy rec
        NS, EW tilt grid as numpy record.
    """

    ha = np.linspace(0, 2.0 * np.pi, 2 * 360)
    dec = np.linspace(-np.pi / 2.0, np.pi / 2.0, 2 * 180)
    nss = []
    ews = []

    # switch between frog and tadpole grid
    if tadpole:
        ha_skip = 5
        dec_skip = 4
    else:
        ha_skip = 30
        dec_skip = 20

    for h in ha[::ha_skip]:
        ns, ew = nsew_of_constant_ha(h, dec)
        ns = 180.0 * ns / np.pi
        ew = 180.0 * ew / np.pi
        nss.extend(ns.tolist())
        ews.extend(ew.tolist())
        nss.append(np.nan)
        ews.append(np.nan)

    for d in dec[::dec_skip]:
        ns, ew = nsew_of_constant_dec(ha, d)
        ns = 180.0 * ns / np.pi
        ew = 180.0 * ew / np.pi
        nss.extend(ns.tolist())
        ews.extend(ew.tolist())
        nss.append(np.nan)
        ews.append(np.nan)

    # convert into numpy array
    dtype = [("ew", "float"), ("ns", "float")]
    tilt_grid = np.zeros(len(ews), dtype=dtype)
    tilt_grid["ew"] = ews
    tilt_grid["ns"] = nss

    return tilt_grid


def get_md_ns_tilt(ra, dec, date=ephem.now()):
    """
    Compute NS and MD tilt for a given source at RA and DEC.

    Parameters
    ----------
    ra : str
        RA in hh:mm:ss notation.
    dec : str
        DEC in hh:mm:ss notation.
    date : ephem object
        The requested date as ephem object.

    Returns
    -------
    md_tilt : float
        MD tilt in degrees.
    ns_tilt : float
        NS tilt in degrees.
    is_up : bool
        Returns whether the source is up at Molonglo.
    """

    # date and time in UTC
    date = ephem.date(date)

    obs = ephem.Observer()
    obs.long = ephem.degrees(LON_MOLONGLO)
    obs.lat = ephem.degrees(LAT_MOLONGLO)
    obs.elevation = ELEVATION_MOLONGLO
    # set date and time
    obs.date = date

    source = ephem.readdb("source,f|L,{0},{1},0,2000".format(ra, dec))
    source.compute(obs)

    # convert into md and ns tilt
    ha = obs.sidereal_time() - source.ra
    ns_tilt, md_tilt = hadec_to_nsew(ha, source.dec)

    is_up = False
    max_ns = np.radians(MAX_NS_MOLONGLO)
    max_md = np.radians(MAX_MD_MOLONGLO)

    if -max_ns < ns_tilt < max_ns and -max_md < md_tilt < max_md:
        is_up = True
    else:
        logger.debug("Source {0} {1} is not up, alt: {2}".format(ra, dec, source.alt))

    # convert to degrees
    md_tilt = np.degrees(md_tilt)
    ns_tilt = np.degrees(ns_tilt)

    return md_tilt, ns_tilt, is_up


def compute_trail_grid(t_targets):
    """
    Compute a trail movement grid for each target with a given DEC.

    Parameters
    ----------
    t_targets : numpy rec
        Numpy record of target sources.

    Returns
    -------
    total : numpy rec
        Trail grid for all targets.
    """

    targets = np.copy(t_targets)

    dtype = [
        ("name", "|S32"),
        ("ra", "|S32"),
        ("dec", "|S32"),
        ("lst", "float"),
        ("md", "float"),
        ("ns", "float"),
        ("is_up", "bool"),
        ("lst_rise", "float"),
        ("lst_set", "float"),
    ]

    grid = np.zeros(180, dtype=dtype)

    has = np.linspace(0, 2.0 * np.pi, 180)

    total = None

    for item in targets:
        dec = ephem.degrees(item["dec"])
        ns, md = nsew_of_constant_dec(has, dec, catch_discont=False)
        ns = 180.0 * ns / np.pi
        md = 180.0 * md / np.pi

        # insert data
        grid["name"] = item["name"]
        grid["ra"] = item["ra"]
        grid["dec"] = item["dec"]
        # convert to lst
        # lst = ha + ra
        grid["lst"] = has + ephem.hours(item["ra"])
        # wrap the lst
        grid["lst"][grid["lst"] > 2 * np.pi] -= 2 * np.pi
        grid["ns"] = ns
        grid["md"] = md

        # sort
        grid = np.sort(grid, order="lst")

        if total is None:
            total = np.copy(grid)
        else:
            total = np.concatenate((total, grid))

    return total


def get_lst(utc):
    """
    Compute the LST at Molonglo that corresponds to the given utc.

    Parameters
    ----------
    utc : str
        UTC date string.

    Returns
    -------
    lst
        LST at Molonglo.
    """

    utc = ephem.Date(utc)

    obs = ephem.Observer()
    obs.lat = ephem.degrees(LAT_MOLONGLO)
    obs.long = ephem.degrees(LON_MOLONGLO)
    obs.elevation = ELEVATION_MOLONGLO
    # set date and time
    obs.date = utc

    lst = obs.sidereal_time()

    return lst


def update_target_tilt(targets, utc):
    """
    Update the MD and NS tilt of the targets for the given UTC time stamp.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    utc
        UTC time stamp.

    Returns
    -------
    targets : numpy rec
        Targets numpy record with updated tilts.
    """

    mds = []
    nss = []
    is_ups = []

    for item in targets:
        md, ns, is_up = get_md_ns_tilt(item["ra"], item["dec"], utc)
        mds.append(md)
        nss.append(ns)
        is_ups.append(is_up)

    # update fields
    targets["md"] = mds
    targets["ns"] = nss
    targets["is_up"] = is_ups

    return targets


def update_target_tilt_grid(targets, grid, utc, interp="linear"):
    """
    Update the MD and NS tilt of the targets for the given UTC time stamp.
    This function uses a lookup grid for speed.
    This assumes that both targets and grid are sorted by name
    and that the names are equal.

    Parameters
    ----------
    targets : numpy rec
        The target sources.
    grid : numpy rec
        The trail lookup grid for all target sources.
    utc : str
        The requested UTC.
    interp : str (optional)
        Interpolation strategy to use. Default is linear interpolation to the
        current LST.

    Returns
    -------
    targets : numpy rec
        Targets record with updated tilts.
    """

    lst = get_lst(utc)

    # figure out the index of the current lst
    # XXX: change the value when RA grid spacing changes.
    len_one = 180
    idx_lst = np.argmin(np.abs(grid["lst"][0:179] - lst))

    if interp == "linear":
        # linear interpolation
        for i in xrange(len(targets)):
            idx = idx_lst + i * len_one

            indiv = grid[grid["name"] == targets["name"][i]]
            targets["md"][i] = np.interp(lst, indiv["lst"], indiv["md"])
            targets["ns"][i] = np.interp(lst, indiv["lst"], indiv["ns"])
            targets["is_up"][i] = grid["is_up"][idx]
    else:
        # piecewise constant interpolation
        sel = grid[idx_lst::len_one]
        targets["md"] = sel["md"]
        targets["ns"] = sel["ns"]
        targets["is_up"] = sel["is_up"]

    return targets


def gen_coordinate_list(filename):
    """
    Generate coordinate list of all pulsars from psrcat.
    """

    command = "psrcat -nonumber -nohead -o long_error_csv -all -c 'psrj raj decj'"

    # split into correct tokens for Popen
    args = shlex.split(command)
    logger.debug(args)

    # spawn process
    with open(filename, "w") as f:
        subprocess.check_call(args, stdout=f)


def parse_coordinate_list(filename):
    """
    Parse pulsar coordinate list output by psrcat.
    """

    # sanity check that file exists
    if not os.path.isfile(filename):
        raise RuntimeError("The file does not exist: {0}".format(filename))

    dtype = [
        ("name", "|S32"),
        ("ref_name", "|S32"),
        ("ra", "|S32"),
        ("err_ra", "float"),
        ("ref_ra", "|S32"),
        ("dec", "|S32"),
        ("err_dec", "float"),
        ("ref_dec", "|S32"),
    ]

    data = np.genfromtxt(filename, dtype=dtype, delimiter=";")

    return data


def get_coordinate_list():
    """
    Get the pulsar coordinate list from psrcat.
    """

    # generate temporary file
    f = tempfile.NamedTemporaryFile(prefix="psrcat_coords_temp_output_", suffix=".txt")
    f.close()
    temp_file = f.name

    gen_coordinate_list(temp_file)
    data = parse_coordinate_list(temp_file)

    # delete the temp file
    os.remove(temp_file)

    return data


def get_ra_dec_list(coord_list, name):
    """
    Get the RA and DEC (J2000) for a given pulsar with Jname.
    It works by looking it up in a coordinate list generated from
    psrcat.

    Parameters
    ---------
    coord_list : numpy rec
        Coordinate list generated from psrcat.
    name : str
        J2000 name of pulsar.

    Returns
    -------
    ra : str
        RA in hh:mm:ss notation.
    dec : str
        DEC in hh:mm:ss notation.
    """

    item = coord_list[coord_list["name"] == name]

    if len(item) == 1:
        ra = item["ra"][0]
        dec = item["dec"][0]
    elif len(item) == 0:
        raise RuntimeError("Pulsar is not in psrcat: {0}".format(name))
    else:
        raise RuntimeError("Jname resolves to multiple entries: {0}".format(name))

    return ra, dec


def get_ra_dec(jname):
    """
    Get the RA and DEC of a pulsar from psrcat.

    Parameters
    ----------
    jname : str
        J2000 name of a pulsar.

    Returns
    -------
    ra : str
        RA in hh:mm:ss notation.
    ra : str
        DEC in hh:mm:ss notation.
    """

    # construct psrcat command
    command = "psrcat -all -c 'raj decj' -o long -nohead -nonumber {0}".format(jname)

    # split into correct tokens for Popen
    args = shlex.split(command)
    logger.debug(args)

    # spawn process
    proc = subprocess.Popen(args, stdout=subprocess.PIPE)
    proc.wait()

    # read response
    stdout, _ = proc.communicate()

    ra = None
    dec = None
    # check for error in psrcat run
    if not stdout.startswith("WARNING"):
        # parse stdout
        result = stdout.split()
        ra = result[0].strip()
        dec = result[3].strip()
    else:
        raise RuntimeError("There was an error in psrcat: {0}".format(stdout))

    # sanity check the output
    # regex pattern for equatorial coordinates in hh:mm:ss notation
    # for example:
    # 08:37:21.1818     -41:35:14.37
    re_eq_coord = re.compile(
        r"^[+-]{0,1}[0-9]{1,2}(\:[0-9]{1,2}){0,1}(\:[0-9]{1,2}(\.[0-9]{1,}){0,1}){0,1}$"
    )

    if re_eq_coord.match(ra) and re_eq_coord.match(dec):
        pass
    else:
        raise RuntimeError(
            "psrcat returned coordinates that are invalid: {0}, {1}".format(ra, dec)
        )

    return ra, dec
