#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2014-2018 Fabian Jankowski
#   Automatic observing mode for Molonglo.
#

from __future__ import print_function
import argparse
import logging
from logging.handlers import RotatingFileHandler, SMTPHandler
import os.path
import signal
import sys

from molsoft.am_helpers import (
    run_schedule,
    park_telescope,
    test_connections,
    SignalHandler,
    exit_cleanly,
    check_other_interfaces,
    block_other_interfaces,
)
from molsoft.config_helpers import get_config
from molsoft.schedule_helpers import (
    load_schedule,
    test_schedule_file,
    calculate_times,
    split_max_tobs,
    get_schedule_grammar,
)
from molsoft.version import __version__


#
# set up logging
#

# root logger
logger = logging.getLogger("automatic_mode")
logger.setLevel(logging.DEBUG)
logger.propagate = False

# log to file
try:
    logfile = RotatingFileHandler(
        os.path.expanduser("~/schedule/logs/automatic_mode.log"),
        maxBytes=2.0e7,
        backupCount=5,
    )
except IOError as e:
    print("Could not enable logging to file: {0}".format(str(e)))
else:
    logfile.setLevel(logging.DEBUG)
    logfile_formatter = logging.Formatter(
        "%(asctime)s, %(name)s, %(levelname)s: %(message)s"
    )
    logfile.setFormatter(logfile_formatter)
    logger.addHandler(logfile)

# also log to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console_formatter = logging.Formatter("%(message)s")
console.setFormatter(console_formatter)
logger.addHandler(console)


#
# MAIN
#


def main():
    # send email on severe errors
    config = get_config()
    log_emails = []

    for item in config["automatic_mode"]["log_emails"]:
        log_emails.append(item.replace(" AT ", "@"))

    email = SMTPHandler(
        mailhost="127.0.0.1",
        fromaddr="fjankowski@swin.edu.au",
        toaddrs=log_emails,
        subject="Molonglo automatic mode error log",
    )
    email.setLevel(logging.CRITICAL)
    email_formatter = logging.Formatter(
        "%(asctime)s, %(name)s, %(levelname)s: %(message)s"
    )
    email.setFormatter(email_formatter)
    logger.addHandler(email)

    email_help = """
Email alerts
  The program will send out email alerts when critical errors occur. It will
  send them to the following email adresses:
  {0}
""".format(
        ", ".join(log_emails)
    )

    grammar_help = get_schedule_grammar() + "\n\n" + email_help

    # handle command line arguments
    parser = argparse.ArgumentParser(
        description="Molonglo automatic observing mode.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=grammar_help,
    )
    parser.add_argument(
        "schedule_filename", type=str, help="Filename of schedule file to process."
    )
    parser.add_argument(
        "-t",
        "--test",
        dest="test_schedule",
        action="store_true",
        default=False,
        help="Dry run. Test schedule file for errors.",
    )
    parser.add_argument(
        "-p",
        "--park",
        dest="park",
        action="store_true",
        default=False,
        help="Park the telescope.",
    )
    parser.add_argument(
        "-m",
        "--max_tobs",
        type=int,
        default=1800,
        dest="max_tobs",
        help="Max observation allowed for any schedule line. Will split observation accordingly",
    )
    parser.add_argument(
        "--ns_east",
        dest="ns_east_state",
        type=str,
        default="auto",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the NS movement of the east arm, default auto",
    )
    parser.add_argument(
        "--ns_west",
        dest="ns_west_state",
        type=str,
        default="auto",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the NS movement of the west arm, default auto",
    )
    parser.add_argument(
        "--md_east",
        dest="md_east_state",
        type=str,
        default="disabled",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the MD drive of the east arm, default disabled",
    )
    parser.add_argument(
        "--md_west",
        dest="md_west_state",
        type=str,
        default="disabled",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the MD drive of the west arm, default disabled",
    )
    parser.add_argument(
        "--disable_movement",
        dest="move_telescope",
        action="store_false",
        default=True,
        help="Disable any telescope movement.",
    )
    parser.add_argument(
        "--ns_offset",
        nargs=2,
        type=float,
        default=[0.0, 0.0],
        help="NS offset for both arms, starting with the east arm, 0.0 as defaults "
        + "(ex: --ns_off 2.3 -4.2)",
    )
    parser.add_argument(
        "--md_offset",
        nargs=2,
        type=float,
        default=[0.0, 0.0],
        help="MD offset for both arms, starting with the east arm, 0.0 as defaults "
        + "(ex: --md_off -3.2 4.5)",
    )
    parser.add_argument(
        "--debug",
        dest="debug",
        action="store_true",
        default=False,
        help="Enable debug mode. This is for experts only! (default: false)",
    )
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    # check if another interface is running the observing:
    if not args.test_schedule:
        other_interface_running, other_interface_name = check_other_interfaces()

        if other_interface_running:
            logger.info(
                '"'
                + other_interface_name
                + '"'
                + " is already running observations, aborting."
            )
            sys.exit(0)
        else:
            logger.info("Creating obs.running.")
            block_other_interfaces()

    ns_md_off = {
        "ns_east_offset": args.ns_offset[0],
        "ns_west_offset": args.ns_offset[1],
        "md_east_offset": args.md_offset[0],
        "md_west_offset": args.md_offset[1],
    }

    movement_state = {
        "ns_east": args.ns_east_state,
        "ns_west": args.ns_west_state,
        "md_east": args.md_east_state,
        "md_west": args.md_west_state,
    }

    print("Molonglo automatic observing mode")
    print("=================================")

    # start signal handler
    sh = SignalHandler()
    signal.signal(signal.SIGINT, sh.handle)

    # all telescope movement is disabled
    if not args.move_telescope:
        logger.info("All telescope movement is disabled.")
        args.park = False

    if args.ns_east_state == "disabled":
        logger.info("East arm is disabled")
    if args.ns_west_state == "disabled":
        logger.info("West arm is disabled")

    # test the schedule file
    if args.test_schedule:
        test_schedule_file(args.schedule_filename)
        sys.exit(0)

    # park the telescope
    if args.park:
        logger.info("Parking the telescope")
        park_telescope(movement_state, ns_md_off)
        exit_cleanly()

    schedule = load_schedule(args.schedule_filename)
    # limit the maximum observation time per single integration
    schedule = split_max_tobs(schedule, args.max_tobs)

    if not len(schedule) > 0:
        sys.exit(1)

    print()
    print("Schedule")
    print("========")
    for line in schedule:
        print(
            line.obs_mode, line.name, line.ra, line.dec, line.snr, line.tmin, line.tmax
        )

    calculate_times(schedule)

    logger.info("Automatic mode started.")
    sys.stdout.flush()

    # test connections to TCC and the backend
    if not args.debug:
        test_connections(args.move_telescope)

    run_schedule(schedule, args.debug, args.move_telescope, movement_state, ns_md_off)

    logger.info("Schedule is finished.")
    exit_cleanly()


# if run directly
if __name__ == "__main__":
    main()
