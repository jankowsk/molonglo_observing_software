#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2015-2018 Fabian Jankowski
#   Fully automatic scheduler for Molonglo.
#

from __future__ import print_function
import argparse
from datetime import datetime
import logging
from logging.handlers import RotatingFileHandler
from multiprocessing import Process, Queue, Pool
import os.path
import socket

# py2 vs py3
try:
    import SocketServer
except ImportError:
    import socketserver as SocketServer
import sys
import threading
import time

import ephem
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from molsoft.config_helpers import get_config
from molsoft.coord_helpers import (
    compute_tilt_grid,
    compute_trail_grid,
    update_target_tilt,
    update_target_tilt_grid,
    get_coordinate_list,
    get_ra_dec_list,
    get_lst,
)
from molsoft.db_helpers import (
    get_observations,
    get_fluxes,
    get_obs_strategy,
    dtype_add_fields,
)
from molsoft.version import __version__


# pylint incorrectly identifies various numpy function to not return
# disable the corresponding pylint test for now
# pylint: disable=E1111

#
# set up logging
#

# root logger
logger = logging.getLogger("automatic_mode")
logger.setLevel(logging.DEBUG)
logger.propagate = False

# log to file
try:
    logfile = RotatingFileHandler(
        os.path.expanduser("~/schedule/logs/scheduler.log"),
        maxBytes=2.0e7,
        backupCount=5,
    )
except IOError as e:
    print("Could not enable logging to file: {0}".format(str(e)))
else:
    logfile.setLevel(logging.INFO)
    logfile_formatter = logging.Formatter(
        "%(asctime)s, %(name)s, " + "%(levelname)s, %(processName)s: " + "%(message)s"
    )
    logfile.setFormatter(logfile_formatter)
    logger.addHandler(logfile)

# also log to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console_formatter = logging.Formatter(
    "%(levelname)s, %(processName)s: " + "%(message)s"
)
console.setFormatter(console_formatter)
logger.addHandler(console)


CONFIG = get_config()

# MD and NS constraints for Molonglo
MAX_NS_MOLONGLO = CONFIG["observatory"]["max_ns"]
MAX_EAST_MD_MOLONGLO = CONFIG["scheduler"]["max_east_md"]
MAX_WEST_MD_MOLONGLO = CONFIG["scheduler"]["max_west_md"]


def posn_command(dec):
    return "POSN; 00:00:00; {0}".format(dec)


def frb_command(ra, dec, tobs, mode="stationary"):
    return "fb; {0}; FRB Transit; {1}; {2}; {3}".format(mode, ra, dec, tobs)


def tb_command(psrj, tmax, mode="TRANSITING"):
    return "TBSNR; {0}; {1}; 15.0; {2}; {3}".format(mode, psrj, 300.0, tmax)


def wait_command(t_wait, mode="LST"):
    return "WAIT; {0}; {1}".format(mode, t_wait)


def gen_schedule(pulsar, utc):
    """
    Returns a list of schedule lines

    Parameters
    ----------
    pulsar: numpy rec
        Pulsar to process.
    utc: str
        UTC in format YYYY-MM-DD-HH:MM:SS.

    Returns
    -------
    sched: list
        Schedule file.
    """

    tfmt = "%H:%M:%S"

    lst = pd.to_datetime(str(get_lst(utc)))

    tdiff = get_time_to_transit(pulsar["ra"], utc, pulsar["slewtime"], pulsar["tobs"])

    sched = []

    # 1) position on correct DEC
    sched.append(posn_command(pulsar["dec"]))

    lst += pd.datetools.Second(pulsar["slewtime"]) + pd.datetools.Second(30)

    if tdiff >= 60:
        sched.append(frb_command(lst.strftime(tfmt), pulsar["dec"], str(tdiff)))

    lst_start = (
        pd.to_datetime(pulsar["ra"]) - pd.datetools.Second(0.5 * pulsar["tobs"])
    ).strftime(tfmt)

    sched.append(wait_command(lst_start))
    sched.append(tb_command(pulsar["name"], pulsar["tobs"]))

    return sched


def save_schedule_file(sched, fname):
    """
    Save schedule file to disk.

    Parameters
    ----------
    sched : list of str
        Schedule file.
    fname : str
        Name of schedule file.
    """

    with open(fname, "w") as o:
        o.write("#Automatically generated schedule file\n")

        for item in sched:
            if "Estimated" in item:
                o.write("\n")

            o.write(item + "\n")


def get_tobs(snr_wanted, flux, delta, tsky, fraction=1.0):
    """
    Compute the observation time required to reach a certain S/N for a given
    pulsar.

    Parameters
    ----------
    snr_wanted : float
        S/N that is requested.
    flux : float
        Pulse-average flux density of pulsar given in mJy.
    delta : float
        Duty cycle of the pulsar.
    tsky : float
        Sky temperature at the pulsar's location given in K.
    fraction : float (optional, default=1.0)
        Fraction of sensitivity of the maximum sensitivity.

    Returns
    -------
    tobs : float
        Required observation time to reach the requested S/N given in seconds.
    """

    # for Molonglo
    # Boltzmann constant in J/K
    # from PDG 2013
    kb = 1.3806488e-23

    # convert SI units to Jy
    SI_to_Jy = 1.0e26

    # total geometric area
    # in m2
    A = 88.0 * 4.0 * 4.7 * 11.0

    # antenna efficiency
    f = 0.55

    # gain in K/Jy
    G = fraction * A * f / (2 * kb * SI_to_Jy)

    # centre frequency in Hz
    centre_freq = 843.0e6

    # bandwidth in Hz
    B = 15.0e6

    # number of polarizations
    Np = 1.0

    # system temperature in K
    Tsys = 75.0

    # input flux is given in mJy
    flux = flux / 1000.0

    tobs = (
        1.0
        / (B * Np)
        * (delta / (1 - delta))
        * ((snr_wanted * (Tsys + tsky)) / (flux * G)) ** 2
    )

    return tobs


def get_targets():
    """
    Get the targets for the scheduler from the observation strategy.

    Returns
    -------
    data : numpy rec
        Targets for the scheduler.

    Raises
    ------
    RuntimeError
        If something goes wrong.
    """

    strategy = get_obs_strategy()
    fluxes = get_fluxes()

    # additional info for scheduler
    sched_dtype = [
        ("name", "|S32"),
        ("path_nr", "int"),
        ("node_nr", "int"),
        ("utc", "float"),
        ("type", "|S32"),
        ("snr_wanted", "float"),
        ("tobs", "float"),
        ("ra", "|S32"),
        ("dec", "|S32"),
        ("md", "float"),
        ("ns", "float"),
        ("is_up", "bool"),
        ("lst_rise", "float"),
        ("lst_set", "float"),
        ("distance_md", "float"),
        ("distance_ns", "float"),
        ("distance", "float"),
        ("slewtime", "float"),
        ("score", "float"),
        ("last_observed", "float"),
        ("n_5deg", "float"),
        ("n_10deg", "float"),
        ("n_20deg", "float"),
        ("was_observed", "bool"),
        ("wait_time", "float"),
    ]

    # combine them
    dtype = dtype_add_fields(fluxes.dtype, strategy.dtype)
    dtype = dtype_add_fields(dtype, sched_dtype)

    data = np.zeros(len(strategy), dtype=dtype)

    # initialize default values
    data["snr_wanted"] = 10.0
    data["last_observed"] = ephem.Date(ephem.now() - 28.0)
    # priority goes from 1 to 100 (most important)
    data["priority"] = 1.0
    data["type"] = "pulsar"
    data["lst_rise"] = None
    data["lst_set"] = None

    # fill in the data
    for field in strategy.dtype.names:
        if field in data.dtype.names:
            data[field] = strategy[field]

    # add fluxes
    for i in xrange(len(data)):
        psrj = data["psrj"][i]
        sel = fluxes[fluxes["psrj"] == psrj]

        if len(sel) == 1:
            for field in sel.dtype.names:
                data[field][i] = sel[field][0]
        elif len(sel) == 0:
            logger.error("No match found in flux table: {0}".format(psrj))
        else:
            raise RuntimeError("Something is wrong in the matching code.")

    # use psrj as name
    data["name"] = data["psrj"]

    # fill in ra, dec
    coord_list = get_coordinate_list()
    for i in xrange(len(data)):
        ra, dec = get_ra_dec_list(coord_list, data["psrj"][i])
        data["ra"][i] = ra
        data["dec"][i] = dec

    # compute required tobs for snr_wanted
    for i in xrange(len(data)):
        tobs = get_tobs(
            data["snr_wanted"][i],
            data["flux"][i],
            data["delta"][i],
            data["tsky"][i],
            0.15,
        )
        data["tobs"][i] = tobs

    # treat pulsars that are not in the flux table
    # set the tobs to 1200 s
    data["tobs"][np.isnan(data["tobs"])] = 1200.0

    # check for significant differences in tobs vs. tmax
    # clip tobs to 2*tmax in these cases
    mask = np.abs(data["tobs"] - data["tmax"]) / data["tmax"] >= 4.0
    diff = data[mask]
    for item in diff:
        print(
            "tobs diff: {0}: {1:.1f} h, {2:.1f} h".format(
                item["name"], item["tmax"] / 3600.0, item["tobs"] / 3660.0
            )
        )
    data["tobs"][mask] = 2 * data["tmax"][mask]

    # we need at least x periods to establish
    # a stable profile
    # enforce minimum tobs accordingly
    min_periods = 100
    for i in xrange(len(data)):
        if data["tobs"][i] < min_periods * data["p0"][i]:
            data["tobs"][i] = min_periods * data["p0"][i]
            logger.debug("Increasing tobs to: {0}".format(data["tobs"][i]))

    # observe for at least 2 min
    data["tobs"][data["tobs"] <= 120.0] = 120.0

    # get latest observing data
    data = update_last_observed(data)

    return data


def update_target_distance(targets, position):
    """
    Update the distance of each target from the current position.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    position : numpy rec
        Current pointing position of the telescope in MD, NS space.

    Returns
    -------
    targets : numpy rec
        Updated targets record.
    """

    # create column vector with len(targets)
    pos_md = np.zeros(len(targets))
    pos_md[:] = position["md"]

    pos_ns = np.zeros(len(targets))
    pos_ns[:] = position["ns"]

    targets["distance_md"] = np.abs(targets["md"] - pos_md)
    targets["distance_ns"] = np.abs(targets["ns"] - pos_ns)
    targets["distance"] = np.sqrt(
        targets["distance_md"] ** 2 + targets["distance_ns"] ** 2
    )

    return targets


def update_target_slewtime(targets, transit):
    """
    Update the slew time to each target from the current position.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    transit : bool
        Determines if the telescope should slew in NS only.

    Returns
    -------
    targets : numpy rec
        Updated targets record.
    """

    # slew rates of molonglo in deg/s
    v_md = 2.5 / 60.0
    v_ns_fast = 4.8 / 60.0
    v_ns_slow = 1.0 / 60.0

    # the telescope slews like this:
    # 1) ramp up to v_md and v_ns_fast
    # 2) it slews directly to the source in both dimensions with speeds
    # v_md and v_ns_fast
    # 3) once it gets into 1 deg of distance to the source it
    # slews with speeds v_md and v_ns_slow
    # 4) ramp down

    t_ramp = 3.0

    # create column vector with len(targets)
    t_md = np.zeros(len(targets))
    t_ns = np.zeros(len(targets))

    t_md[:] = targets["distance_md"] / v_md

    mask_1deg = targets["distance_ns"] <= 1.0

    t_ns[mask_1deg] = targets["distance_ns"][mask_1deg] / v_ns_slow
    t_ns[np.logical_not(mask_1deg)] = (
        targets["distance_ns"][np.logical_not(mask_1deg)] - 1.0
    ) / v_ns_fast + 1.0 / v_ns_slow

    # we have to wait for the slowest slew dimension
    if transit:
        t_uniform = t_ns
    else:
        t_uniform = np.maximum(t_md, t_ns)

    targets["slewtime"] = t_ramp + t_uniform + t_ramp

    return targets


def get_can_be_done(targets, utc, transit):
    """
    Determine if the targets can be done. That means that they are up for all
    slewtime + tobs.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    utc
        UTC timestamp.
    transit : bool
        Determines if the telescope operates as a transit intrument.

    Returns
    -------
    can_be_done : numpy arr
        Boolean array that determines whether each target can be observed.
    """

    can_be_done = np.zeros(len(targets), dtype="bool")

    lst_now = get_lst(utc)

    sec = 2.0 * np.pi / (24 * 60 * 60.0)

    # 04:00, 12:00
    # for lst_rise < lst_set:
    # lst_rise < lst_now < lst_set
    # 22:00, 05:00
    # else:
    # lst_set > lst_now > lst_rise

    start = float(lst_now) + sec * targets["slewtime"]
    if transit:
        end = start + 0.5 * sec * targets["tobs"]
    else:
        end = start + sec * targets["tobs"]

    # wrap
    start[start > 2.0 * np.pi] = start[start > 2.0 * np.pi] - 2.0 * np.pi
    end[end > 2.0 * np.pi] = end[end > 2.0 * np.pi] - 2.0 * np.pi

    # check that:
    # * lst_rise < lst_now + slewtime < lst_set
    # and
    # * lst_rise < lst_now + slewtime + tobs < lst_set
    mask_normal = targets["lst_rise"] < targets["lst_set"]
    mask_inv = targets["lst_rise"] > targets["lst_set"]

    mask1 = (
        (targets["lst_rise"] < start)
        & (start < targets["lst_set"])
        & (targets["lst_rise"] < end)
        & (end < targets["lst_set"])
        & mask_normal
    )

    mask2 = (
        (
            ((0.0 < start) & (start < targets["lst_set"]))
            | ((targets["lst_rise"] < start) & (start < 2.0 * np.pi))
        )
        & (
            ((0.0 < end) & (end < targets["lst_set"]))
            | ((targets["lst_rise"] < end) & (end < 2.0 * np.pi))
        )
    ) & mask_inv

    mask_tot = np.logical_or(mask1, mask2)

    can_be_done[mask_tot] = True

    return can_be_done


def get_weight_cadence(t_data, utc):
    """
    Compute the weight for the required cadence.

    Parameters
    ----------
    t_data : numpy rec
        Target data record.
    utc
        UTC timestamp.

    Returns
    -------
    weights : numpy arr
        Cadence weight for each target.
    """

    data = np.copy(t_data)

    # delta t in days
    t = utc - data["last_observed"]

    # t0 = 4h
    # t1 = 1.25*cad
    # w(t) = {0, t < t0; f(t), t0 <= t < t1}
    # f(t) = 1/(t1-t0) * (t-t0)
    weights = np.zeros(len(data))
    t0 = 4.0 / 24.0
    t1 = 1.25 * data["cadence"]

    # 1)
    mask = t < t0
    weights[mask] = 0

    # 2)
    mask = t >= t0
    weights[mask] = (1.0 / (t1[mask] - t0)) * (t[mask] - t0)

    weights = weights**2

    return weights


def update_target_score(targets, trail_grid, position, utc, use_neighbours=False):
    """
    Update the score for each target based on various criteria.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    trail_grid : numpy rec
        Pre-computed trail grid of each target source.
    position : numpy rec
        Current pointing position of the telescope.
    use_neighbours : bool (optional, default=False)
        Recompute the next neighbour density weights.

    Returns
    -------
    targets : numpy rec
        Targets record with updated weights.
    """

    # update all other variables
    # this is the function that takes the longest to compute
    # targets = update_target_tilt(targets, utc)
    targets = update_target_tilt_grid(targets, trail_grid, utc, interp="linear")

    targets = update_target_distance(targets, position)
    targets = update_target_slewtime(targets, transit=True)
    if use_neighbours:
        targets = update_neighbours(targets)

    # we first select only the targets that can be done
    # all scores are not absolute, but relative for that selection
    is_do = get_can_be_done(targets, utc, transit=True)

    # 1) can be done
    # target is up for at least slewtime + tobs
    weight_can_be_done = is_do

    # 2) already observed
    weight_was_observed = np.zeros(len(targets))
    weight_was_observed[is_do] = np.logical_not(targets["was_observed"][is_do])

    # 3) was detected previously with certain snr

    # 4) slewtime
    weight_slewtime = np.zeros(len(targets))
    weight_slewtime[is_do] = 1.0 / (targets["slewtime"][is_do]) ** 2
    weight_slewtime = weight_slewtime / weight_slewtime.max()

    # 5) tobs
    weight_tobs = np.zeros(len(targets))
    weight_tobs[is_do] = 1.0 / targets["tobs"][is_do]
    weight_tobs = weight_tobs / weight_tobs.max()

    # 6 a) neighbours 5deg
    weight_n_5deg = np.zeros(len(targets))
    weight_n_5deg[is_do] = targets["n_5deg"][is_do] + 1
    weight_n_5deg = weight_n_5deg / weight_n_5deg.max()

    # b) neighbours 10deg
    weight_n_10deg = np.zeros(len(targets))
    weight_n_10deg[is_do] = targets["n_10deg"][is_do] + 1
    weight_n_10deg = weight_n_10deg / weight_n_10deg.max()

    # c) neighbours 20deg
    weight_n_20deg = np.zeros(len(targets))
    weight_n_20deg[is_do] = targets["n_20deg"][is_do] + 1
    weight_n_20deg = weight_n_20deg / weight_n_20deg.max()

    # d) neighbours total
    # weight_n = weight_n_5deg * weight_n_10deg * weight_n_20deg
    weight_n = weight_n_10deg
    weight_n = weight_n / weight_n.max()

    # 7) priority
    weight_priority = np.zeros(len(targets))
    weight_priority[is_do] = targets["priority"][is_do]
    weight_priority = weight_priority / weight_priority.max()

    # 8) distance from meridian
    weight_md = np.zeros(len(targets))
    weight_md[is_do] = 1.0 / np.abs(targets["md"][is_do])
    weight_md = weight_md / weight_md.max()

    # 9) cadence wanted
    weight_cadence = np.zeros(len(targets))
    weight_cadence[is_do] = get_weight_cadence(targets, utc)[is_do]
    weight_cadence = weight_cadence / weight_cadence.max()

    # 10) dwell weight, for meridian transit observations
    weight_dwell = np.zeros(len(targets))
    weight_dwell[is_do] = get_dwell_weights(targets, utc)[is_do]
    # weight_slewtime[is_do] = 1.0/(targets["slewtime"][is_do])**2
    weight_dwell = weight_dwell / weight_dwell.max()

    targets["score"] = weight_can_be_done * weight_slewtime * weight_dwell
    # * weight_cadence# * weight_priority
    targets["wait_time"] = weight_dwell
    # normalize
    targets["score"] = targets["score"] / targets["score"].max()

    best_5 = targets[targets["score"] >= np.sort(targets["score"])[::-1][4]]

    # print " BEST_5: "
    # print best_5
    print()
    print("================ Next 5 pulsars ==================")
    print("NAME        slewtime (sec)      wait_time (sec)     score")
    # print np.column_stack((best_5['name'], best_5["score"]))
    for index in range(len(best_5)):
        print(
            best_5["name"][index],
            best_5["slewtime"][index],
            best_5["wait_time"][index],
            best_5["score"][index],
        )
    print("==================================================")

    return targets


def get_dwell_weights(t_targets, utc):
    """
    Compute the dwell weights, which are mainly used for meridian transit
    observations.

    Parameters
    ----------
    t_targets : numpy rec
        Targets record.
    utc
        UTC timestamp.

    Returns
    -------
    weights : numpy arr
        Dwell weights for each target.
    """

    targets = np.copy(t_targets)

    tdeltas = np.zeros(len(targets), dtype="float")

    for i in xrange(len(targets)):
        item = targets[i]
        tdiff = get_time_to_transit(item["ra"], utc, item["slewtime"], item["tobs"])

        tdeltas[i] = tdiff

    tdeltas[tdeltas < 60] = 1e8

    weights = 1.0 / tdeltas**2

    return weights


def get_time_to_transit(ra, utc, slewtime, tobs):
    """
    Return time to transit.

    Parameters
    ----------
    ra
        RA of object.
    utc
        UTC timestamp.
    slewtime : float
        Slewtime to target from current position in seconds.
    tobs : float
        Observing time in seconds.

    Returns
    -------
    tdiff : float
        Time to transit in seconds.
    """

    lst = pd.to_datetime(str(get_lst(utc)))
    corr_md = pd.datetools.Second(48)
    wait_dt = pd.datetools.Second(30)

    ra = pd.to_datetime(ra)
    slew_time = pd.datetools.Second(slewtime)
    obs_time = pd.datetools.Second(0.5 * tobs)

    tdelta = (ra - lst - corr_md) - slew_time - obs_time - wait_dt

    if ra > lst + corr_md:
        tdiff = tdelta.seconds
    else:
        tdiff = tdelta.seconds - 24 * 60 * 60

    return tdiff


def update_neighbours(targets):
    """
    Compute how many sources are neighbours withing a distance of
    X deg around each source.
    diameter is in degrees around a source.
    Use a circle here.

    Parameters
    ----------
    targets : numpy rec
        Targets record.

    Returns
    -------
    targets : numpy rec
        Updated targets record.
    """

    for diameter in [5, 10, 20]:
        neighbours = []

        for item in targets:
            sources = np.copy(targets)

            sources["md"] = np.abs(sources["md"] - item["md"])
            sources["ns"] = np.abs(sources["ns"] - item["ns"])
            sources["distance"] = np.sqrt(sources["md"] ** 2 + sources["ns"] ** 2)

            nr_neighbours = len(sources[sources["distance"] <= diameter / 2.0]) - 1.0

            neighbours.append(nr_neighbours)

        # update fields
        if diameter == 5:
            targets["n_5deg"] = neighbours
        elif diameter == 10:
            targets["n_10deg"] = neighbours
        elif diameter == 20:
            targets["n_20deg"] = neighbours

    return targets


def get_best_sources(targets, nr):
    """
    Return the sources with the best scores at the moment that
    have not been observed yet.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    nr : int
        Number of sources to return.

    Returns
    -------
    best_sources : numpy rec
        Record of number nr best sources.
    """

    # remove sources that are not up
    # and sources that are already observed
    mask = np.logical_and(targets["is_up"], np.logical_not(targets["was_observed"]))
    targets = targets[mask]

    if not len(targets) > 0:
        raise RuntimeError("All sources have been observed.")

    targets = np.sort(targets, order="score")
    targets = np.flipud(targets)

    if len(targets) >= nr:
        best_sources = targets[0 : nr - 1]
    else:
        logger.warning("Not enough targets provided: {0}, {1}".format(len(targets), nr))
        best_sources = targets

    return best_sources


def gen_path(targets, trail_grid, start_position, start_utc=ephem.now()):
    """
    Generate a path through the target cloud for a start position
    and and start utc.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    trail_grid : numpy rec
        Pre-computed trail grid for each target.
    start_position : numpy rec
        MD, NS value of start position.
    start_utc : ephem.Date object (optional, default=now)
        UTC of start of the path generation.

    Returns
    -------
    path : numpy rec
        Path through the target cloud.
    """

    # initialize
    utc = start_utc
    position = np.copy(start_position)

    # add start position and utc
    start_pos = np.zeros_like(targets[0])
    start_pos["node_nr"] = 0
    start_pos["name"] = "start"
    start_pos["type"] = "meta"
    start_pos["utc"] = utc
    start_pos["md"] = position["md"]
    start_pos["ns"] = position["ns"]

    path = np.copy(start_pos)

    depth = 5

    for _ in xrange(depth):
        # compute score
        # use this static score (slewtime, etc.) as a first guess
        targets = update_target_score(targets, trail_grid, position, utc)
        best_sources = get_best_sources(targets, 10)

        print(best_sources)

        # chose next source
        print("Random source on best sources list chosen")
        next_source = np.random.choice(best_sources, 1)
        print(next_source["name"][0])

        # print "First source on best sources list chosen"
        # next_source = best_sources[0]
        # print next_source["name"][0]

        # set node number
        next_source["node_nr"] = path["node_nr"].max() + 1
        # set source as observed
        next_source["was_observed"] = True
        targets["was_observed"][targets["name"] == next_source["name"][0]] = True
        # utc is the time of the start of slew
        next_source["utc"] = utc

        path = np.vstack((path, next_source))

        # print "{0}: {1}, {2}".format(i, utc, next_source["name"][0])

        # forward the time by slewtime and tobs
        t = next_source["slewtime"][0] + next_source["tobs"][0]
        utc = ephem.Date(utc + t * ephem.second)

        # update target position
        # and update telescope position to position of current source
        current_pos = update_target_tilt(next_source, utc)
        position["md"] = current_pos["md"]
        position["ns"] = current_pos["ns"]

    # add end position and utc
    end_pos = np.zeros_like(targets[0])
    end_pos["node_nr"] = path["node_nr"].max() + 1
    end_pos["name"] = "end"
    end_pos["type"] = "meta"
    end_pos["utc"] = utc
    end_pos["md"] = current_pos["md"]
    end_pos["ns"] = current_pos["ns"]

    path = np.vstack((path, end_pos))

    return path


def compute_paths(targets, trail_grid, start_position, start_utc, nr_paths):
    """
    Compute a number of paths.
    The path_nr keyword identifies each individual path.

    Parameters
    ----------
    targets : numpy rec
        Targets record.
    trail_grid : numpy rec
        Trail lookup table for each target.
    start_position : numpy rec
        Start pointing position of the telescope.
    start_utc
        UTC timestamp of the start.
    nr_paths : int
        Total number of paths generated.

    Returns
    -------
    total : numpy rec
        Record of all the paths.
    """

    total = None

    for i in xrange(nr_paths):
        logger.debug("Processing path: {0}".format(i))

        # reset the observed status
        targets["was_observed"] = False

        path = gen_path(targets, trail_grid, start_position, start_utc)
        path["path_nr"] = i

        if total is None:
            total = np.copy(path)
        else:
            total = np.concatenate((total, path))

        logger.debug("Path done: {0}".format(i))

    return total


def get_scores_paths(total, output_stats=False):
    """
    Compute the total scores for each path.

    Parameters
    ----------
    total : numpy rec
        Total record of paths.
    output_stats : bool (optional, default=False)
        Determines whether statistics are output.

    Returns
    -------
    score : list
        List of scores that reference the paths record via the keyword path_nr.
    """

    nr_paths = total["path_nr"].max() + 1

    dtype = [
        ("path_nr", "int"),
        ("score", "float"),
        ("s_per_source", "float"),
        ("slewtime", "float"),
        ("tobs", "float"),
        ("efficiency", "float"),
        ("distance_md", "float"),
        ("distance_ns", "float"),
        ("distance", "float"),
    ]
    score = np.zeros(nr_paths, dtype=dtype)

    # compute total scores for each path
    for i in xrange(nr_paths):
        path = total[total["path_nr"] == i]

        # remove start and end position
        path = path[path["type"] != "meta"]

        score["path_nr"][i] = i
        score["score"][i] = np.sum(path["score"])
        score["s_per_source"][i] = (
            np.sum(path["slewtime"]) + np.sum(path["tobs"])
        ) / len(path)
        score["slewtime"][i] = np.sum(path["slewtime"])
        score["tobs"][i] = np.sum(path["tobs"])
        score["efficiency"][i] = np.sum(path["tobs"]) / (
            np.sum(path["slewtime"]) + np.sum(path["tobs"])
        )
        score["distance_md"][i] = np.sum(path["distance_md"])
        score["distance_ns"][i] = np.sum(path["distance_ns"])
        score["distance"][i] = np.sum(path["distance"])

    # sort
    score = np.sort(score, order=["score", "distance", "efficiency"])

    if output_stats:
        for path in score:
            print(
                "{0:2}: {1:.3f}, {2:.1f} min/source, {3:.1f} min, {4:.1f} min, {5:.1f} %, {6:.1f} deg, {7:.1f} deg, {8:.1f} deg".format(
                    path["path_nr"],
                    path["score"],
                    path["s_per_source"] / 60.0,
                    path["slewtime"] / 60.0,
                    path["tobs"] / 60.0,
                    100.0 * path["efficiency"],
                    path["distance_md"],
                    path["distance_ns"],
                    path["distance"],
                )
            )

        print(
            "Score [min, avg, median, 75%, max]: {0:.2f}, {1:.2f}, {2:.2f}, {3:.2f}, {4:.2f}".format(
                score["score"].min(),
                score["score"].mean(),
                np.median(score["score"]),
                np.percentile(score["score"], 75.0),
                score["score"].max(),
            )
        )

        print(
            "Distance [min, 25%, avg, median, max]: {0:.1f}, {1:.1f}, {2:.1f}, {3:.1f}, {4:.1f}".format(
                score["distance"].min(),
                np.percentile(score["distance"], 25.0),
                score["distance"].mean(),
                np.median(score["distance"]),
                score["distance"].max(),
            )
        )

        print(
            "Efficiency [min, avg, median, 75%, max]: {0:.1f}, {1:.1f}, {2:.1f}, {3:.1f}, {4:.1f}".format(
                100 * score["efficiency"].min(),
                100 * score["efficiency"].mean(),
                100 * np.median(score["efficiency"]),
                100 * np.percentile(score["efficiency"], 75.0),
                100 * score["efficiency"].max(),
            )
        )

    return score


def show_path(fig, tilt_grid, trail_grid, t_targets, p_path):
    """
    Show a path in skyview.
    """

    targets = np.copy(t_targets)
    path = np.copy(p_path)

    path = np.sort(path, order="node_nr")

    for i in xrange(len(path)):
        position = path[i]
        utc = ephem.Date(path["utc"][i])

        # mark it observed
        targets["was_observed"][targets["name"] == position["name"]] = True

        # update the scores
        targets = update_target_score(targets, trail_grid, position, utc)

        if i + 1 < len(path):
            next_source = path[i + 1]
        else:
            next_source = None

        plot_skyview(fig, tilt_grid, targets, position, next_source, utc, path)
        time.sleep(1)


def plot_skyview(fig, tilt_grid, t_targets, position, best_sources, utc, path=None):
    """
    Plot the targets in a skyview.
    """

    targets = np.copy(t_targets)

    fig.clf()
    ax1 = fig.add_subplot(111)

    # meridian line
    ax1.axvline(x=0.0, color="red", lw=2.0, ls="dashed")

    # maximum zenith angle unreachable areas
    ax1.axhline(y=54.0, color="red", zorder=10)
    ax1.text(
        0.0,
        54.5,
        "Max. zenith angle",
        color="red",
        horizontalalignment="center",
        verticalalignment="bottom",
        zorder=10,
    )
    ax1.axhspan(54.0, 90.0, color="red", alpha=0.15, zorder=10)
    ax1.axhline(y=-54.0, color="red", zorder=10)
    ax1.text(
        0.0,
        -54.5,
        "Max. zenith angle",
        color="red",
        horizontalalignment="center",
        verticalalignment="top",
        zorder=10,
    )
    ax1.axhspan(-54.0, -90.0, color="red", alpha=0.15, zorder=10)

    # soft limits
    ax1.axhline(y=MAX_NS_MOLONGLO, color="orange", zorder=10)
    ax1.text(
        0.0,
        MAX_NS_MOLONGLO + 0.5,
        "Soft limit",
        color="orange",
        horizontalalignment="center",
        verticalalignment="bottom",
        zorder=10,
    )
    ax1.axhspan(MAX_NS_MOLONGLO, 54.0, color="orange", alpha=0.15, zorder=10)
    ax1.axhline(y=-MAX_NS_MOLONGLO, color="orange", zorder=10)
    ax1.text(
        0.0,
        -MAX_NS_MOLONGLO - 0.5,
        "Soft limit",
        color="orange",
        horizontalalignment="center",
        verticalalignment="top",
        zorder=10,
    )
    ax1.axhspan(-MAX_NS_MOLONGLO, -54.0, color="orange", alpha=0.15, zorder=10)

    # md soft limits
    ax1.axvspan(
        np.abs(MAX_WEST_MD_MOLONGLO), 90.0, color="orange", alpha=0.15, zorder=10
    )
    ax1.axvspan(
        -np.abs(MAX_EAST_MD_MOLONGLO), -90.0, color="orange", alpha=0.15, zorder=10
    )

    # plot tilt grid
    ax1.plot(tilt_grid["ew"], tilt_grid["ns"], color=(0.7, 0.7, 0.7), zorder=1)

    # plot dec labels
    for dec in [-80, -60, -40, -20, 0, +17]:
        ns = dec + 35.37
        ax1.text(
            0,
            ns,
            str(dec),
            horizontalalignment="center",
            verticalalignment="center",
            color="#222222",
            zorder=5,
        )

    ax1.text(
        30,
        36.5,
        "2hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )
    ax1.text(
        30,
        32.5,
        "W",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )
    ax1.text(
        60,
        36.5,
        "4hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )
    ax1.text(
        60,
        32.5,
        "W",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )

    ax1.text(
        -30,
        36.5,
        "2hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )
    ax1.text(
        -30,
        32.5,
        "E",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )
    ax1.text(
        -60,
        36.5,
        "4hr",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )
    ax1.text(
        -60,
        32.5,
        "E",
        horizontalalignment="center",
        verticalalignment="center",
        color="#222222",
        zorder=5,
    )

    targets_up = targets[targets["is_up"]]
    targets_set = targets[np.logical_not(targets["is_up"])]

    # plot targets that are up
    # suppress zero here
    targets_up["score"] = targets_up["score"] + 1.0e-10

    sc = ax1.scatter(
        targets_up["md"],
        targets_up["ns"],
        c=targets_up["score"],
        vmin=targets_up["score"].min(),
        vmax=targets_up["score"].max(),
        cmap=mpl.cm.jet,
        s=60,
        edgecolors="None",
        norm=mpl.colors.LogNorm(),
        label="Targets",
        zorder=3,
    )

    cb = fig.colorbar(sc)
    cb.set_label(r"Score")

    # targets that are set
    ax1.scatter(targets_set["md"], targets_set["ns"], color="gray", zorder=3)

    # plot current position
    ax1.scatter(
        position["md"],
        position["ns"],
        facecolor="red",
        edgecolor="black",
        marker="*",
        s=180,
        zorder=6,
    )

    # plot best sources
    if best_sources is not None and len(best_sources) > 0:
        ax1.scatter(
            best_sources["md"],
            best_sources["ns"],
            facecolor="blue",
            edgecolor="black",
            marker="^",
            s=100,
            zorder=6,
        )

    # plot path
    if path is not None and len(path) > 0:
        ax1.scatter(path["md"], path["ns"], marker="x", color="black", s=80, zorder=5)
        ax1.plot(path["md"], path["ns"], color="black", lw=2, zorder=5)

    # tweak the plot
    ax1.grid(True)
    ax1.set_title("{0}, {1} up".format(utc, len(targets_up)))
    ax1.set_xlabel("MD tilt [degrees]")
    ax1.set_ylabel("NS tilt [degrees]")
    ax1.set_xlim(68, -68)
    ax1.set_ylim(-70, 70)

    fig.canvas.draw()


def get_best_path(s_score):
    """
    Select the optimum path based on different scores.

    Parameters
    ----------
    s_score : numpy rec
        Score record.

    Returns
    -------
    best : int or None
        Path_nr of the best path or None if no best path was found.
    """

    score = np.copy(s_score)

    # sort
    score = np.sort(score, order=["score", "distance", "efficiency"])
    score = np.flipud(score)

    for i in xrange(len(score)):
        best = score[i]

        if (
            best["score"] >= np.percentile(score["score"], 75.0)
            and best["distance"] <= np.percentile(score["distance"], 25.0)
            and best["efficiency"] >= np.percentile(score["efficiency"], 75.0)
        ):
            break
    else:
        logger.error("No best path found.")
        best = None

    if best is not None:
        print(
            "The best path is: {0} ({1:.1f}, {2:.1f} %, {3:.1f} deg, iteration: {4})".format(
                best["path_nr"],
                best["score"],
                100 * best["efficiency"],
                best["distance"],
                i,
            )
        )
        best = best["path_nr"]

    return best


def pool_compute_paths(args):
    """
    Run the compute_path function by a pool of worker processes.
    """

    # re-seed the random number generator in each process
    # this is very important
    # otherwise you will get the same results from each worker
    np.random.seed()

    return compute_paths(*args)


def combine_paths(t_data):
    """
    Combine paths into a single data structure.
    Increment the path_nr appropriately.
    """

    data = np.copy(t_data)

    total = None
    i = 0

    for item in data:
        nr_paths = len(np.unique(item["path_nr"]))
        item["path_nr"] = item["path_nr"] + i * nr_paths
        i += 1

        if total is None:
            total = np.copy(item)
        else:
            total = np.vstack((total, item))

    return total


def append_paths(total, t_data):
    """
    Append path or score data onto an existing numpy record.
    Increment the path_nr appropriately.
    """

    data = np.copy(t_data)

    if total is None:
        return data

    # print total["path_nr"]
    # print data["path_nr"]

    start = total["path_nr"].max() + 1
    data["path_nr"] = start + data["path_nr"]

    total = np.concatenate((total, data))

    return total


def compute_rise_set(t_trail_grid):
    """
    Add the is_up status and compute the rise and set LST
    for each target.
    """

    grid = np.copy(t_trail_grid)

    max_ns = np.abs(MAX_NS_MOLONGLO)
    max_east_md = np.abs(MAX_EAST_MD_MOLONGLO)
    max_west_md = np.abs(MAX_WEST_MD_MOLONGLO)

    mask_is_up = (
        (-max_ns < grid["ns"])
        & (grid["ns"] < max_ns)
        & (-max_east_md < grid["md"])
        & (grid["md"] < max_west_md)
    )

    grid["is_up"][mask_is_up] = True
    grid["is_up"][np.logical_not(mask_is_up)] = False

    for name in np.unique(grid["name"]):
        temp = grid[grid["name"] == name]

        up = temp[temp["is_up"]]
        if not len(up) >= 1:
            # source is never up
            lst_rise = 0.0
            lst_set = 0.0
        elif len(up) == len(temp):
            # source is always up
            lst_rise = ephem.hours("00:00:00")
            lst_set = ephem.hours("24:00:00")
        else:
            # check transitions
            # rise: false -> true
            # set: true -> false

            for _ in xrange(len(temp)):
                if temp["is_up"][0] == temp["is_up"][1]:
                    pass
                else:
                    if not temp["is_up"][0]:
                        # rise: false -> true
                        lst_rise = temp["lst"][0]
                    else:
                        # set: true -> false
                        lst_set = temp["lst"][0]

                temp = np.roll(temp, 1)

        # insert into data
        grid["lst_rise"][grid["name"] == name] = lst_rise
        grid["lst_set"][grid["name"] == name] = lst_set

    return grid


def update_last_observed(targets):
    """
    Update the last_observed times for each target.
    If a source was never observed, set its last_observed to 1 year
    in the past.
    """

    utc_format = "%Y-%m-%d-%H:%M:%S"

    data = get_observations()

    # sort data by utc
    data = np.sort(data, order="utc")

    now = ephem.now()
    past_1year = ephem.Date(now - 365.0)

    for i in xrange(len(targets)):
        obs = data[data["source_name"] == targets["name"][i]]

        if len(obs) >= 1:
            utc = obs["utc"][-1]
            utc = ephem.Date(datetime.strptime(utc, utc_format))
        else:
            utc = past_1year

        # update fields
        targets["last_observed"][i] = utc

    logger.info("Data parsed from observations database.")

    return targets


def show_best_path(fig, path):
    """
    Show the best path in a window.
    """

    fig.clf()

    info_text = "nr. name        score  tslew  dist  tobs\n"
    info_text += "                       [min]  [deg] [min]\n\n"

    if path is not None and len(path) > 0:
        for node in path:
            node_text = "{0:2}: {1:10} {2:5.2f}   {3:5.2f} {4:6.2f} {5:5.2f}".format(
                node["node_nr"],
                node["name"],
                node["score"],
                node["slewtime"] / 60.0,
                node["distance"],
                node["tobs"] / 60.0,
            )
            info_text = info_text + node_text + "\n"

        fig.text(
            0.05,
            0.9,
            info_text,
            horizontalalignment="left",
            verticalalignment="top",
            color="black",
            family="monospace",
        )

    fig.canvas.draw()


def set_rise_set(targets, grid):
    """
    Set the rise and set LST for each target from
    the trail grid.
    """

    for i in xrange(len(targets)):
        mask = grid["name"] == targets["name"][i]
        targets["lst_rise"][i] = grid["lst_rise"][mask][0]
        targets["lst_set"][i] = grid["lst_set"][mask][0]

    return targets


def worker(task_queue, result_queue):
    """
    A worker process.
    """

    # re-seed the random number generator in each process
    # this is very important
    # otherwise you will get the same results from each worker
    np.random.seed()

    for args in iter(task_queue.get, "STOP"):
        result = compute_paths(*args)
        result_queue.put(result)


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    """
    A server side code for communicating with automatic_mode.
    """

    def handle(self):
        """
        Handle the TCP connection. This is the main processing function.
        """

        mode = self.request.recv(1024)

        if mode != "DynamicSchedule":
            error = "no message received"
            self.request.sendall(error)

        else:
            logger.info("Dynamic mode triggered.")

            targets = get_targets()
            print("Number of targets: {0}".format(len(targets)))

            # set all tobs to 600 for the transit observations
            targets["tobs"] = 600

            position = np.zeros(1, dtype=[("md", "float"), ("ns", "float")])

            # anansi - to obtain the md and ns of the telescope
            sys.path.append("/home/observer/TCC3_test")
            from anansi.config import config

            tcc_status = config.status_server
            status_ip = tcc_status.ip
            status_port = tcc_status.port

            from frog import get_status

            status = get_status(status_ip, status_port)

            position["md"] = status["east_md"][0]
            position["ns"] = status["east_ns"][0]

            # compute the tilt grid
            tilt_grid = compute_tilt_grid()

            # compute trail grid
            trail_grid = compute_trail_grid(targets)
            trail_grid = compute_rise_set(trail_grid)

            # set rise and set lsts
            targets = set_rise_set(targets, trail_grid)

            # make sure that targets and trail_grid are
            # both sorted by name
            targets = np.sort(targets, order="name")
            trail_grid = np.sort(trail_grid, order="name")

            utc = ephem.now()

            # pre-compute all scores
            # including the nearest neighbor densities
            targets = update_target_score(targets, trail_grid, position, utc, True)

            # tasks = [[targets, trail_grid, position, utc, 10] \
            # for i in xrange(4)]

            # pool = Pool(processes=4)
            # results = pool.map(pool_compute_paths, tasks)

            # total = combine_paths(results)

            # scores = get_scores_paths(total, False)
            # path_nr_best = get_best_path(scores)

            # if path_nr_best is not None:
            #    best_path = total[total["path_nr"] == path_nr_best]
            #
            #    # remove start and end meta nodes
            #    output_path = best_path[best_path["type"] != "meta"]

            best_source = get_best_source_transit(targets)
            print(" BEST SOURCE :")
            print(best_source)
            print()

            logger.info("Sending the pulsars to am_client.")
            response = "<xml>Successfully sent.</xml>"
            self.request.sendall(response)

            logger.info("Sending the pulsars to am_server.")
            AM = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

            # save pulsars to file for automatic_mode_server
            pname = "/tmp/scheduler_temp.sch"

            # generates schedule for the given pulsar
            schedule = gen_schedule(best_source, utc)
            save_schedule_file(schedule, pname)

            print(" SCHEDULE :")
            print(schedule)
            print()

            config = get_config()
            server_config = (
                config["automatic_mode"]["server"]["host"],
                config["automatic_mode"]["server"]["port"],
            )

            time.sleep(2)

            try:
                # connect to AM server
                AM.connect(server_config)
                AM.sendall("pulsarsready")
                time.sleep(1)

                AM.sendall(pname)
                logger.info("Pulsar list sent to automatic_mode_server.")
            except Exception as e:
                logger.error("There was a socket error: {0}".format(str(e)))
            finally:
                AM.close()


def get_best_source_transit(targets):
    """
    Determine the best source for transit mode.

    Parameters
    ----------
    targets : numpy rec
        Targets record.

    Returns
    -------
    best_source : numpy rec
        Best source to observe.
    """

    data = np.copy(targets)

    data = np.sort(data, order="score")
    data = np.flipud(data)

    best_source = data[0]

    return best_source


def run_genpaths(targets, trail_grid, position, utc, npath, output_stats):
    """
    Run the processing for genpaths mode.
    """

    # queues
    task_queue = Queue()
    result_queue = Queue()

    # submit tasks
    ntask = 28
    tasks = [[targets, trail_grid, position, utc, npath] for _ in xrange(ntask)]

    for item in tasks:
        task_queue.put(item)

    # measure the execution
    start = time.time()

    # start worker processes
    nproc = 4
    for _ in xrange(nproc):
        proc = Process(target=worker, args=(task_queue, result_queue))
        proc.daemon = True
        proc.start()

    total = None
    scores = None

    for _ in xrange(len(tasks)):
        try:
            print(
                "Queue sizes: {0}, {1}".format(task_queue.qsize(), result_queue.qsize())
            )

            # get the partial results
            result = result_queue.get(0.1)

            total = append_paths(total, result[:, 0])
            score = get_scores_paths(result[:, 0], output_stats)
            scores = append_paths(scores, score)

            get_best_path(scores)

        except KeyboardInterrupt:
            logger.warn("Caught SIGINT, stopping the program.")
            break

    end = time.time()

    print(len(total), len(np.unique(total["path_nr"])), len(scores))
    print(total["path_nr"].max(), scores["path_nr"].max())

    print("Path evaluations per second: {0:.1f}".format(len(total) / (end - start)))


def run_plotpaths(targets, tilt_grid, trail_grid, position, utc, npath):
    """
    Run the processing for plotpaths mode.
    """

    paths = compute_paths(targets, trail_grid, position, utc, npath)
    scores = get_scores_paths(paths, True)
    get_best_path(scores)

    plt.ion()
    fig1 = plt.figure()
    fig1.canvas.set_window_title("Scheduler")

    while True:
        path_nr = input("Which path should be plotted? ")

        test_path = paths[paths["path_nr"] == path_nr]
        show_path(fig1, tilt_grid, trail_grid, targets, test_path)


def run_plotlive(
    targets, tilt_grid, trail_grid, position, utc, npath, output_stats, show_best
):
    """
    Run the processing for plotlive mode.
    """

    # anansi
    sys.path.append("/home/observer/TCC3_test")
    from anansi.config import config

    tcc_status = config.status_server
    status_ip = tcc_status.ip
    status_port = tcc_status.port

    from frog import get_status

    plt.ion()
    fig1 = plt.figure()
    fig1.canvas.set_window_title("Scheduler")

    if show_best:
        fig2 = plt.figure()
        fig2.canvas.set_window_title("Best path")

        pool = Pool(processes=4)
        best_path = None

    status = get_status(status_ip, status_port)

    position["md"] = status["east_md"][0]
    position["ns"] = status["east_ns"][0]

    # update the last_observed data
    targets = update_last_observed(targets)

    # update scores
    targets = update_target_score(targets, trail_grid, position, utc)

    end_status = ephem.now()
    end_paths = end_status
    end_show_paths = end_status

    # update window very often
    # but get status only every 20 seconds to ease the load on tcc
    # and reduce the general processing footprint
    while True:
        utc = ephem.now()

        if utc >= end_status:
            status = get_status(status_ip, status_port)

            position["md"] = status["east_md"][0]
            position["ns"] = status["east_ns"][0]

            # update the last_observed data
            targets = update_last_observed(targets)

            # compute score
            targets = update_target_score(targets, trail_grid, position, utc)

            # reschedule
            end_status = ephem.Date(ephem.now() + 20.0 * ephem.second)

        # re-compute the best path every 15 minutes
        if show_best:
            if utc >= end_paths:
                tasks = [[targets, trail_grid, position, utc, npath] for _ in xrange(4)]

                results = pool.map_async(pool_compute_paths, tasks)

                # reschedule
                end_paths = ephem.Date(ephem.now() + 15 * 60.0 * ephem.second)

            # show the best path every minute
            if results.ready() and utc >= end_show_paths:
                paths = combine_paths(results.get(timeout=1))
                scores = get_scores_paths(paths, output_stats)
                best_nr = get_best_path(scores)

                if best_nr is not None:
                    best_path = paths[paths["path_nr"] == best_nr]
                    show_path(fig1, tilt_grid, trail_grid, targets, best_path)
                    show_path(fig1, tilt_grid, trail_grid, targets, best_path)

                # reschedule
                end_show_paths = ephem.Date(ephem.now() + 60.0 * ephem.second)

        plot_skyview(fig1, tilt_grid, targets, position, None, utc, None)

        if show_best:
            show_best_path(fig2, best_path)

        time.sleep(3)


def run_showtrails(targets, tilt_grid, trail_grid, position, utc):
    """
    Run the processing for showtrails mode.
    """

    plt.ion()
    fig1 = plt.figure()
    fig1.canvas.set_window_title("Scheduler")

    while True:
        utc = ephem.Date(utc + 10 * ephem.minute)
        print(utc)

        # compute score
        targets = update_target_score(targets, trail_grid, position, utc)

        faint = targets[targets["tobs"] / 60.0 > 60.0]
        for item in faint:
            print(
                "{0}: {1:.1f}, {2}, {3}".format(
                    item["name"], item["tobs"] / 3600.0, item["rank"], item["obs_mode"]
                )
            )

        plot_skyview(fig1, tilt_grid, targets, position, None, utc, None)

        _ = raw_input("Press Enter")
        # time.sleep(1)
        position["ns"] = targets["ns"][targets["score"] == 1]


def run_dynamic():
    """
    Run the processing for dynamic mode.
    """

    config = get_config()
    server_config = (
        config["scheduler"]["server"]["host"],
        config["scheduler"]["server"]["port"],
    )

    server = ThreadedTCPServer(server_config, ThreadedTCPRequestHandler)
    server.allow_reuse_address = True

    tcp_thread = threading.Thread(name="tcpserver", target=server.serve_forever)

    # exit the server thread when the main thread terminate
    tcp_thread.daemon = True
    tcp_thread.start()

    print("TCP server running in thread: {0}".format(tcp_thread.name))

    while True:
        try:
            time.sleep(10)
        except KeyboardInterrupt:
            logger.warn("Caught SIGINT, stopping the program.")
            break


#
# MAIN
#


def main():
    # handle command line arguments
    parser = argparse.ArgumentParser(description="Molonglo scheduler.")
    parser.add_argument(
        "mode",
        type=str,
        default=None,
        choices=["genpaths", "plotpaths", "plotlive", "showtrails", "dynamic"],
        help="Select a mode of operation.",
    )
    parser.add_argument(
        "-n",
        "--npath",
        dest="npath",
        type=int,
        default=50,
        help="Number of paths to generate (default: 50).",
    )
    parser.add_argument(
        "--output_stats",
        dest="output_stats",
        action="store_true",
        default=False,
        help="Output statistics from the path selection. " + "default: False)",
    )
    parser.add_argument(
        "--show_best",
        dest="show_best",
        action="store_true",
        default=False,
        help="Compute and show the best " + "path. (default: False)",
    )
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    print("Molonglo scheduler")
    print("==================")

    targets = get_targets()
    print("Number of targets: {0}".format(len(targets)))

    # XXX: set all tobs to 600 for the transit observations
    targets["tobs"] = 600

    position = np.zeros(1, dtype=[("md", "float"), ("ns", "float")])
    position["md"] = 0
    position["ns"] = -15

    # compute the tilt grid
    tilt_grid = compute_tilt_grid()

    # compute trail grid
    trail_grid = compute_trail_grid(targets)
    trail_grid = compute_rise_set(trail_grid)

    # set rise and set lsts
    targets = set_rise_set(targets, trail_grid)

    # make sure that targets and trail_grid are
    # both sorted by name
    targets = np.sort(targets, order="name")
    trail_grid = np.sort(trail_grid, order="name")

    utc = ephem.now()

    # pre-compute all scores
    # including the nearest neighbor densities
    targets = update_target_score(targets, trail_grid, position, utc, True)

    if args.mode == "genpaths":
        print("Generating paths.")
        run_genpaths(targets, trail_grid, position, utc, args.npath, args.output_stats)

    elif args.mode == "plotpaths":
        print("Plotting paths interactively.")
        run_plotpaths(targets, tilt_grid, trail_grid, position, utc, args.npath)

    elif args.mode == "plotlive":
        print("Plotting the scores live.")
        run_plotlive(
            targets,
            tilt_grid,
            trail_grid,
            position,
            utc,
            args.npath,
            args.output_stats,
            args.show_best,
        )

    elif args.mode == "showtrails":
        print("Plotting the trails of targets.")
        run_showtrails(targets, tilt_grid, trail_grid, position, utc)

    elif args.mode == "dynamic":
        print("Running dynamic scheduler.")
        run_dynamic()

    logger.info("All done.")


# if run directly
if __name__ == "__main__":
    main()
