#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2015-2018 Fabian Jankowski
#   SNR extractor for Molonglo - client.
#

from __future__ import print_function
import argparse
from datetime import datetime
import logging
import signal
import socket
import sys
import time

from lxml import etree
import matplotlib.pyplot as plt
import numpy as np

from molsoft.config_helpers import get_config
from molsoft.version import __version__


def get_snr_in_x_min(snr0, t0, t=20 * 60):
    """
    Extrapolate the SNR from SNR0 at t0 to a time t.

    Parameters
    ----------
    snr0 : float
        SNR at t0.
    t0 : float
        Observation time t0 in seconds.
    t : float (optional)
        Observation time t in seconds. Default is 20 min.

    Returns
    -------
    snr : float
        SNR at time t.
    """

    snr = snr0 * np.sqrt(t / t0)

    return snr


def plot_snr(fig, data):
    """
    Plot the SNR evolution.
    """

    fig.clf()

    # SNR
    ax1 = fig.add_subplot(211)

    ax1.scatter(data["tobs"] / 60.0, data["snr"], marker="o", zorder=4, label="S/N")
    ax1.plot(data["tobs"] / 60.0, data["snr"], lw=2, zorder=3)

    # plot running mean
    ax1.scatter(
        data["tobs"] / 60.0,
        data["rmean_snr"],
        marker="d",
        color="green",
        zorder=4,
        label="running mean S/N",
    )
    ax1.plot(data["tobs"] / 60.0, data["rmean_snr"], lw=2, color="green", zorder=3)

    # plot snr_radon
    ax1.scatter(
        data["tobs"] / 60.0,
        data["snr_radon"],
        marker="p",
        color="red",
        zorder=4,
        label="radon S/N",
    )
    ax1.plot(data["tobs"] / 60.0, data["snr_radon"], lw=2, color="red", zorder=3)

    # tweak the plot
    ax1.grid(True)
    ax1.legend(loc="best", frameon=False)
    ax1.set_ylabel("Total S/N ratio")
    ax1.set_title(
        "{0} - {1}\n{2}".format(
            data["name"][0],
            datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
            data["filename"][0],
        )
    )
    ax1.set_xlim(
        0,
    )

    # ToA error
    ax2 = fig.add_subplot(212)
    ax2.scatter(
        data["tobs"] / 60.0, data["toa_err"], marker="o", zorder=4, label="ToA err"
    )
    ax2.plot(data["tobs"] / 60.0, data["toa_err"], lw=2, zorder=3)

    # tweak the plot
    ax2.grid(True)
    ax2.legend(loc="best", frameon=False)
    ax2.set_xlabel("tobs [min]")
    ax2.set_ylabel("ToA error [us]")
    ax2.set_xlim(
        0,
    )

    info_str = "S/N: {0:.1f}\nrmean: {1:.1f}\nt: {2:.1f} min\nthres: {3:.1f}\nstatus: {4}\nToA: {5:.1f} us\nthres: {6:.1f} %\nstatus: {7}\nfinished: {8}".format(
        data["snr"][-1],
        data["rmean_snr"][-1],
        data["tobs"][-1] / 60.0,
        data["snr_threshold"][-1],
        data["snr_status"][-1],
        data["toa_err"][-1],
        data["toa_threshold"][-1],
        data["toa_status"][-1],
        data["finished"][-1],
    )

    fig.text(0.83, 0.7, info_str, horizontalalignment="left", verticalalignment="top")

    fig.canvas.draw()


def parse_xml(raw_data):
    """
    Parse the XML message and determine the status of the observation.

    Parameters
    ----------
    raw_data : str
        Raw status data.
    status : numpy rec
        Status of observation.
    """

    xml = None
    dtype = [
        ("name", "|S32"),
        ("filename", "|S2048"),
        ("snr", "float"),
        ("rmean_snr", "float"),
        ("snr_radon", "float"),
        ("tobs", "float"),
        ("snr_status", "|S32"),
        ("snr_threshold", "float"),
        ("toa_err", "float"),
        ("toa_status", "|S32"),
        ("toa_threshold", "float"),
        ("finished", "|S32"),
    ]
    status = np.zeros(1, dtype=dtype)
    status["name"] = None

    try:
        xml = etree.fromstring(raw_data)
    except etree.XMLSyntaxError:
        logging.error("Could not parse the XML: {0}".format(raw_data))

    try:
        status["name"] = xml.find("name").text
        status["filename"] = xml.find("filename").text
        status["snr"] = float(xml.find("snr").text)
        status["rmean_snr"] = float(xml.find("rmean_snr").text)
        status["snr_radon"] = float(xml.find("snr_radon").text)
        status["tobs"] = float(xml.find("tobs").text)
        status["snr_status"] = xml.find("snr_status").text
        status["snr_threshold"] = float(xml.find("snr_threshold").text)
        status["toa_err"] = float(xml.find("toa_err").text)
        status["toa_status"] = xml.find("toa_status").text
        status["toa_threshold"] = float(xml.find("toa_threshold").text)
        status["finished"] = xml.find("finished").text
    except AttributeError:
        logging.error("Could not parse the XML: {0}".format(raw_data))

    return status


def get_snr_status():
    """
    Connect to SNR extractor server and get status.

    Returns
    -------
    status : numpy rec or None
        Status of current observation. If an error occurs return None.
    """

    config = get_config()
    server_config = (
        config["snr_extractor"]["server"]["host"],
        config["snr_extractor"]["server"]["port"],
    )

    raw_data = ""

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_config)

        # send data
        message = "<xml><command>query</command></xml>"
        sock.sendall(str(message))

        # receive response
        raw_data = sock.recv(1024)

        sock.close()
    except socket.error as e:
        logging.error("There was a socket error: {0}".format(e))

    # parse
    status = parse_xml(raw_data)

    return status


def set_threshold(mode, thresh):
    """
    Send the SNR or ToA threshold to the snr extractor server.

    Parameters
    ----------
    mode : str
        Determines if SNR or ToA threshold is set.
    thresh : float
        Threshold to set.

    Returns
    -------
    worked : bool
        Whether the command was passed successfully.
    """

    if mode not in ["snr", "toa"]:
        logging.error("Threshold mode unknown: {0}".format(mode))
        raise RuntimeError("Threshold mode unknown: {0}".format(mode))

    config = get_config()
    server_config = (
        config["snr_extractor"]["server"]["host"],
        config["snr_extractor"]["server"]["port"],
    )

    reply = None
    worked = False
    raw_data = ""

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_config)

        # send data
        message = "<xml><command>set_{0}_threshold</command><{0}_threshold>{1}</{0}_threshold></xml>".format(
            mode, thresh
        )
        sock.sendall(str(message))

        # receive response
        raw_data = sock.recv(1024)

        sock.close()
    except socket.error as e:
        logging.error("There was a socket error: {0}".format(e))
    else:
        # parse response
        xml = None
        try:
            xml = etree.fromstring(raw_data)
        except etree.XMLSyntaxError as e:
            logging.error("Could not parse the XML: {0}".format(raw_data))

        try:
            reply = xml.find("reply").text
        except AttributeError as e:
            pass
        else:
            if reply == "ok":
                worked = True

    return worked


def signal_handler(signum, frame):
    """
    Handle unix signals sent to the program.
    """

    # treat SIGINT/INT/CRTL-C
    if signum == signal.SIGINT:
        logging.warn("SIGINT received, stopping the program.")

        logging.info("Program stopped.")

        sys.exit(0)


#
# MAIN
#


def main():
    # start signal handler
    signal.signal(signal.SIGINT, signal_handler)

    # set up logging
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")

    # handle command line arguments
    parser = argparse.ArgumentParser(description="Molonglo SNR extractor - client.")
    parser.add_argument("--version", action="version", version=__version__)
    parser.parse_args()

    print("Molonglo SNR extractor - client")
    print("===============================")

    plt.ion()

    fig1 = plt.figure()
    fig1.canvas.set_window_title("SNR extractor - client")
    fig1.subplots_adjust(left=0.11, right=0.8, bottom=0.09)

    # initialise
    data = get_snr_status()
    filename = data["filename"][0]
    total = data

    # update window often, but get snr only every 10 sec
    start_status = time.time()
    end_status = start_status + 10

    while True:
        if time.time() >= end_status:
            data = get_snr_status()

            new_obs = data["filename"][0]
            if new_obs != filename:
                filename = new_obs
                total = None

            if total is None:
                total = np.copy(data)
            else:
                if data["finished"][0] != "True":
                    total = np.append(total, data)

            snr_20min = get_snr_in_x_min(data["snr"][0], data["tobs"][0], 20 * 60)
            snr_30min = get_snr_in_x_min(data["snr"][0], data["tobs"][0], 30 * 60)

            print(
                "tobs: {0:.1f} min, S/N: {1:.1f}. (20 min: {2:.1f} S/N, 30 min: {3:.1f} S/N)".format(
                    data["tobs"][0] / 60.0, data["snr"][0], snr_20min, snr_30min
                )
            )

            # reschedule
            start_status = time.time()
            end_status = start_status + 10

        plot_snr(fig1, total)

        time.sleep(2)


# if run directly
if __name__ == "__main__":
    main()
