#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2014-2018 Fabian Jankowski
#   Automatic mode helpers for Molonglo.
#

from __future__ import print_function
import datetime
import logging
import signal
import socket
import sys
import time
import os.path
from os import remove as osremove

import ephem
from lxml import etree
import numpy as np

from molsoft.config_helpers import get_config
from molsoft.helper_functions import mobs_to_mpsr_driver_encoder
from molsoft.snr_extractor_client import get_snr_status, set_threshold
from molsoft.coord_helpers import get_ra_dec

# local anansi modules
sys.path.append(os.path.expanduser("~/TCC3_test"))
try:
    from anansi.config import config
except ImportError:
    print("Could not import anansi.")
else:
    from anansi.comms import SocketError
    from anansi.comms import TCPClient
    from anansi.tcc.tcc_utils import TCCMessage
    from anansi.coords import hadec_to_nsew


# pylint incorrectly identifies various numpy function to not return
# disable the corresponding pylint test for now
# pylint: disable=E1111

# set up logging
logger = logging.getLogger("automatic_mode." + __name__)
logger.setLevel(logging.INFO)

#
# IP and port configuration
#

try:
    # TCC
    TCC = config.tcc_server
    TCC_STATUS = config.status_server
    # 1) control
    TCC_IP = TCC.ip
    TCC_PORT = TCC.port
    # 2) status
    STATUS_IP = TCC_STATUS.ip
    STATUS_PORT = TCC_STATUS.port
except NameError as e:
    print("Probably anansi was not imported correctly: {0}".format(str(e)))
    print("Setting bogus IPs and ports.")
    TCC_IP = "localhost"
    TCC_PORT = "65444"
    STATUS_IP = "localhost"
    STATUS_PORT = TCC_PORT

CONFIG = get_config()

# backend
BACKEND_IP = CONFIG["backend"]["host"]
BACKEND_PORT = CONFIG["backend"]["port"]
OBSERVER_NAME = CONFIG["automatic_mode"]["observer_name"]

# coordinates of Molonglo
LAT_MOLONGLO = np.radians(CONFIG["observatory"]["lat"])
LON_MOLONGLO = np.radians(CONFIG["observatory"]["lon"])
ELEVATION_MOLONGLO = CONFIG["observatory"]["elevation"]
MAX_NS_MOLONGLO = CONFIG["observatory"]["max_ns"]
MAX_MD_MOLONGLO = CONFIG["observatory"]["max_md"]

# ensure only one accessing client at a time
SEMAPHORE_DIR = CONFIG["tcc"]["semaphore"]["dir"]
SEMAPHORE_FILE = CONFIG["tcc"]["semaphore"]["file"]


class TCCUser(object):
    """
    TCP client to connect to TCC.
    """

    def __init__(self, ip=TCC_IP, port=TCC_PORT, timeout=45.0):
        self.ip = ip
        self.port = port
        self.timeout = timeout

    def send(self, msg):
        """
        Send a command message to TCC.

        Parameters
        ----------
        msg : str
            Message to send out.

        Returns
        -------
        status : bool
            If the command got successfully passed it returns True,
            otherwise False.
        """

        logger.debug(str(msg))

        client = TCPClient(self.ip, self.port, timeout=self.timeout)
        client.send(str(msg))

        raw_data = client.receive()
        client.close()

        # parse the response
        xml = None
        status = False
        try:
            xml = etree.fromstring(raw_data)
            logger.debug(etree.tostring(xml, pretty_print=True))
            if xml.find("success").text == "TCC command passed":
                logger.debug("Command issued successfully.")
                status = True
        except AttributeError as e:
            logger.error("Could not parse the XML reply: {0}".format(e))

        return status


class BackendUser(TCCUser):
    """
    TCP client to connect to the backend.
    """

    def __init__(self, ip=BACKEND_IP, port=BACKEND_PORT, timeout=45.0):
        self.ip = ip
        self.port = port
        self.timeout = timeout

    def send(self, msg):
        """
        Send a command message to the backend.

        Parameters
        ----------
        msg : str
            Message to send out.

        Returns
        -------
        status : bool
            If the command got successfully passed it returns True,
            otherwise False.
        """

        logger.debug(str(msg))

        client = TCPClient(self.ip, self.port, timeout=self.timeout)
        client.send(str(msg))

        raw_data = client.receive()
        client.close()

        # parse the response
        xml = None
        status = False
        try:
            xml = etree.fromstring(raw_data)
            logger.debug(etree.tostring(xml, pretty_print=True))
            if xml.find("reply").text == "ok":
                logger.debug("Command issued successfully.")
                status = True
        except AttributeError as e:
            logger.error("Could not parse the XML reply: {0}".format(e))

        return status, etree.tostring(xml, pretty_print=True)

    def is_idle(self):
        """
        Check if the backend is idle.

        Returns
        -------
        status : bool
            Whether the backend is idle.
        """

        msg = mobs_to_mpsr_driver_encoder("query")

        client = TCPClient(self.ip, self.port, timeout=self.timeout)
        client.send(str(msg))

        raw_data = client.receive()
        client.close()

        # parse the response
        xml = None
        status = False
        try:
            xml = etree.fromstring(raw_data)
            logger.debug(etree.tostring(xml, pretty_print=True))
            if xml.find("response").find("mpsr_status").text == "Idle":
                logger.info("Backend is idle.")
                status = True
        except AttributeError as e:
            logger.error("Could not parse the XML reply: {0}".format(e))

        return status


def parse_cfg(cfg_file, tags=None):
    """
    Return config file with given tags as dictionary.

    Parameters
    ----------
    cfg_file : str
        full directory to config file
    tags : list
        list of tags to search the cgf_file

    Returns
    -------
    config_dict : dict
        dictionary with keys given in tags, and values
        extracted from cfg_file. If one tag doesn't exist,
        value corresponded will be None, else value is of
        type str, or list if multiple values exist for
        same key.
    """

    if tags is None:
        tags = []
        with open(cfg_file) as o:
            for line in o:
                if line[0] in ["\n", "#"]:
                    continue

                tags.append(line.split()[0])

    config_dict = {}

    with open(cfg_file) as o:
        for line in o:
            if line[0] in ["\n", "#"]:
                continue

            for tag in tags:
                if tag in line:
                    i = line.split()
                    assert tag == i[0]
                    config_dict[tag] = []

                    for ii in i[1:]:
                        if "#" in ii:
                            break
                        config_dict[tag].append(ii)

                    if len(config_dict[tag]) == 1:
                        config_dict[tag] = config_dict[tag][0]
                    tags.remove(tag)

    for tag in tags:
        logging.warning("Could not parse tag {0} from file {1}.".format(tag, cfg_file))
        config_dict[tag] = None

    return config_dict


def get_tcc_status():
    """
    Query status of TCC.
    """

    msg = TCCMessage(OBSERVER_NAME)
    msg.server_command("ping")
    client = TCCUser()

    reply = client.send(str(msg))

    return reply


def get_backend_status():
    """
    Query status of the backend.
    """

    client = BackendUser()
    client.is_idle()


def point(x, y, movement_state, ns_md_off, tracking, coord_system="equatorial"):
    """
    Point at RA and DEC using J2000 and hh:mm:ss notation.

    Parameters
    ----------
    x : str
        Horizontal pointing position, normally RA.
    y : str
        Vertial pointing position, normally DEC.
    movement_state : numpy rec
        Either auto, slow or disable for each arm.
    ns_md_off : dict
        Dictionary with keyvalues {ns_east_offset, ns_west_offset,
        md_east_offset, md_west_offset} representing the pointing offsets
        that are sent to anansi.
    tracking : str
        Either on or off. It determines if the requested field should
        actively be tracked.
    coord_system : str (optional, default="equatorial")
        Coordinate system that x and y are specified in. Default is equatorial.

    Raises
    ------
    RuntimeError
        If tracking status is unknown.
    """

    # sanity check the input
    if tracking not in ["on", "off"]:
        raise RuntimeError("Tracking status not known: {0}".format(tracking))

    # sanity check again
    slew_modes = ["auto", "slow", "disabled"]

    if (
        movement_state["ns_east"] in slew_modes
        and movement_state["ns_west"] in slew_modes
    ):
        # all is well
        pass
    else:
        raise RuntimeError(
            "Slew mode not known: {0}, {1}".format(
                movement_state["ns_east"], movement_state["ns_west"]
            )
        )

    # XXX: Fix this
    if x == "00:00:00":
        coord_system = "equatorial_ha"
    else:
        coord_system = "equatorial"

    msg = TCCMessage(OBSERVER_NAME)
    msg.tcc_pointing(
        x,
        y,
        system=coord_system,
        units="hhmmss",
        epoch="2000",
        tracking=tracking,
        ns_east_state=movement_state["ns_east"],
        ns_west_state=movement_state["ns_west"],
        md_east_state=movement_state["md_east"],
        md_west_state=movement_state["md_west"],
        ns_east_offset=ns_md_off["ns_east_offset"],
        ns_west_offset=ns_md_off["ns_west_offset"],
        md_east_offset=ns_md_off["md_east_offset"],
        md_west_offset=ns_md_off["md_west_offset"],
        offset_units="degrees",
    )

    client = TCCUser()

    # initialise
    reply = False

    try:
        # issue command
        reply = client.send(str(msg))
    except SocketError:
        logger.error("Could not connect to TCC.")
    except socket.timeout:
        logger.error("Could not connect to TCC. Socket timeout.")
    except Exception as e:
        logger.error("Could not connect to TCC: {0}".format(e))

    # error checking
    if reply:
        logger.info("Point command issued successfully.")
    else:
        # something went wrong
        logger.critical("Could not issue point command to TCC.")
        exit_cleanly(err="tcc")


def stop_tracking(force=False):
    """
    Stop tracking.

    Parameters
    ----------
    force : bool (optional, default=False)
        Whether tracking should be stopped at all costs, without exiting
        cleanly.
    """

    msg = TCCMessage(OBSERVER_NAME)
    msg.tcc_command("stop")
    client = TCCUser()

    # initialise
    reply = False

    try:
        # issue command
        reply = client.send(str(msg))
    except SocketError:
        logger.error("Could not connect to TCC.")
    except socket.timeout:
        logger.error("Could not connect to TCC. Socket timeout.")
    except Exception as e:
        logger.error("Could not connect to TCC: {0}".format(e))

    # error checking
    if reply:
        logger.info("Stop command issued successfully.")
    else:
        # something went wrong
        logger.critical("Could not issue stop tracking command to TCC.")
        if not force:
            exit_cleanly(err="tcc")


def wind_stow(maintenance=False):
    """
    Send the telescope to wind stow position.

    Parameters
    ----------
    maintenance : bool (optional, default=False)
        If True send the telescope to maintenance position instead.
    """

    msg = TCCMessage(OBSERVER_NAME)

    if maintenance:
        msg.tcc_command("maintenace_stow")
    else:
        msg.tcc_command("wind_stow")

    client = TCCUser()

    # initialise
    reply = False

    try:
        # issue command
        reply = client.send(str(msg))
    except SocketError:
        logger.error("Could not connect to TCC.")
    except socket.timeout:
        logger.error("Could not connect to TCC. Socket timeout.")
    except Exception as e:
        logger.error("Could not connect to TCC: {0}".format(e))

    # error checking
    if reply:
        logger.info("Stow command issued successfully.")
    else:
        # something went wrong
        logger.critical("Could not issue windstow command to TCC.")
        exit_cleanly(err="tcc")


def parse_status(raw_data):
    """
    Parse the tracking status returned from TCC.

    Parameters
    ----------
    raw_data : str
        Raw data as returned from anasi.

    Returns
    -------
    status : numpy rec
        Tracking status of the telescope.
    """

    # initialise
    west_ns = 0
    west_md = 0
    east_ns = 0
    east_md = 0
    source_ns = 0
    source_md = 0
    west_ns_on_source = False
    west_md_on_source = False
    east_ns_on_source = False
    east_md_on_source = False

    # get tilt of the arms
    try:
        # parse the response
        xml = etree.fromstring(raw_data)

        # these are all strings
        west_ns = xml.find("ns").find("west").find("tilt").text
        west_md = xml.find("md").find("west").find("tilt").text
        east_ns = xml.find("ns").find("east").find("tilt").text
        east_md = xml.find("md").find("east").find("tilt").text
        # on source
        west_ns_on_source = xml.find("ns").find("west").find("on_target").text
        west_md_on_source = xml.find("md").find("west").find("on_target").text
        east_ns_on_source = xml.find("ns").find("east").find("on_target").text
        east_md_on_source = xml.find("md").find("east").find("on_target").text
    except AttributeError as e:
        logger.error("Could not parse the XML reply: {0}".format(e))
    else:
        # cast to float
        west_ns = 360.0 / (2 * np.pi) * float(west_ns)
        west_md = 360.0 / (2 * np.pi) * float(west_md)
        east_ns = 360.0 / (2 * np.pi) * float(east_ns)
        east_md = 360.0 / (2 * np.pi) * float(east_md)

        logger.debug("West arm tilt [NS, MD]: {0} {1}".format(west_ns, west_md))
        logger.debug("East arm tilt [NS, MD]: {0} {1}".format(east_ns, east_md))
        logger.debug(
            "West arm on source [NS, MD]: {0} {1}".format(
                west_ns_on_source, west_md_on_source
            )
        )
        logger.debug(
            "East arm on source [NS, MD]: {0} {1}".format(
                east_ns_on_source, east_md_on_source
            )
        )

    # get source tilt
    try:
        # these are all strings
        source_ns = xml.find("coordinates").find("NS").text
        source_md = xml.find("coordinates").find("EW").text
    except AttributeError as e:
        logger.error("Could not parse the XML reply: {0}".format(e))
    else:
        # cast to float
        source_ns = 360.0 / (2 * np.pi) * float(source_ns)
        source_md = 360.0 / (2 * np.pi) * float(source_md)

        logger.debug("Source [NS, MD]: {0} {1}".format(source_ns, source_md))

    dtype = [
        ("west_ns", "float"),
        ("west_md", "float"),
        ("east_ns", "float"),
        ("east_md", "float"),
        ("source_ns", "float"),
        ("source_md", "float"),
        ("west_ns_on_source", "|S8"),
        ("west_md_on_source", "|S8"),
        ("east_ns_on_source", "|S8"),
        ("east_md_on_source", "|S8"),
    ]

    status = np.zeros(1, dtype=dtype)
    status["west_ns"] = west_ns
    status["west_md"] = west_md
    status["east_ns"] = east_ns
    status["east_md"] = east_md
    status["source_ns"] = source_ns
    status["source_md"] = source_md
    status["west_ns_on_source"] = west_ns_on_source
    status["west_md_on_source"] = west_md_on_source
    status["east_ns_on_source"] = east_ns_on_source
    status["east_md_on_source"] = east_md_on_source

    return status


def get_status():
    """
    Get telescope tracking status for TCC3 via TCP socket.

    Returns
    -------
    status : numpy rec or None
        Tracking status of the telescope, or None if no connection to TCC3
        could be made.
    """

    raw_data = None
    status = None

    try:
        client = TCPClient(STATUS_IP, STATUS_PORT, timeout=45.0)
    except socket.error:
        logger.warn("Could not connect to the socket.")
    else:
        try:
            client.send("give me the status")
            raw_data = client.receive(4096)
        except socket.timeout:
            logger.warn("Could not read data from the socket.")
        except socket.error:
            logger.warn("Could not read data from the socket.")
        else:
            logger.debug("Raw data: {0}".format(raw_data))
            client.close()
            status = parse_status(raw_data)

    return status


def is_on_source(movement_state, ns_md_off, is_pulsar=False):
    """
    Check if the telescope is on source.

    Parameters
    ----------
    movement_state : numpy rec
        Slew speed and state of each arm.
    ns_md_off : dict
        Pointing offsets for both arms.
    is_pulsar : bool (optional, default=False)
        Be less conservative with pulsars and report on_source already when
        it is within half-power point of primary beam.

    Returns
    -------
    on_source : bool or None
        Whether the telescope is on source. None if telescope status from TCC
        is unknown.
    slew_time : float or None
        Expected slew time to source. None if telescope status from TCC is
        unknown.
    """

    # initialise
    west_on_source = False
    east_on_source = False
    on_source = None
    slew_time = None

    # threshold for distance to source when telescope is on_source
    # Molonglo primary beam is 4x2 deg2
    # start observing once the source is within half-power point
    # that is OK for pulsars, for quasars and others we need to be
    # more conservative
    if is_pulsar:
        md_thres = 1.0
        ns_thres = 0.5
    else:
        md_thres = 0.1
        ns_thres = 0.1

    # query telescope tracking status
    status = get_status()

    if status is not None:
        # calculate slew times
        west_source_md = status["source_md"][0] + ns_md_off["md_west_offset"]
        west_source_ns = status["source_ns"][0] + ns_md_off["ns_west_offset"]
        east_source_md = status["source_md"][0] + ns_md_off["md_east_offset"]
        east_source_ns = status["source_ns"][0] + ns_md_off["ns_east_offset"]

        slew_time_west = get_slew_time(
            status["west_md"][0], status["west_ns"][0], west_source_md, west_source_ns
        )

        slew_time_east = get_slew_time(
            status["east_md"][0], status["east_ns"][0], east_source_md, east_source_ns
        )

        # check is on_source
        if (
            movement_state["md_west"] == "disabled"
            or np.abs(status["west_md"][0] - west_source_md) <= md_thres
        ) and (
            movement_state["ns_west"] == "disabled"
            or np.abs(status["west_ns"][0] - west_source_ns) <= ns_thres
        ):
            west_on_source = True

        if (
            movement_state["md_east"] == "disabled"
            or np.abs(status["east_md"][0] - east_source_md) <= md_thres
        ) and (
            movement_state["ns_east"] == "disabled"
            or np.abs(status["east_ns"][0] - east_source_ns) <= ns_thres
        ):
            east_on_source = True

        on_source = west_on_source and east_on_source
        slew_time = np.maximum(slew_time_west, slew_time_east)

        logger.debug(
            "Slew times [W, E] arm: {0} {1}".format(slew_time_west, slew_time_east)
        )

    return on_source, slew_time


def get_md_ns_tilt(ra, dec, date=ephem.now()):
    """
    Compute NS and MD tilt for a given source at RA and DEC.

    Parameters
    ----------
    ra : str
        RA in hh:mm:ss notation.
    dec : str
        DEC in hh:mm:ss notation.
    date : ephem.Date object (optional, default=now)
        Current date.

    Returns
    -------
    md_tilt : float
        MD tilt in degrees.
    ns_tilt : float
        NS tilt in degrees.
    is_up : bool
        True if the source is up else false.
    """

    # date and time in UTC
    date = ephem.date(date)

    obs = ephem.Observer()
    obs.long = ephem.degrees(LON_MOLONGLO)
    obs.lat = ephem.degrees(LAT_MOLONGLO)
    obs.elevation = ELEVATION_MOLONGLO
    # set date and time
    obs.date = date

    source = ephem.readdb("source,f|L,{0},{1},0,2000".format(ra, dec))
    source.compute(obs)

    # convert into md and ns tilt
    ha = obs.sidereal_time() - source.ra
    ns_tilt, md_tilt = hadec_to_nsew(ha, source.dec)

    is_up = False
    max_ns = np.radians(MAX_NS_MOLONGLO)
    max_md = np.radians(MAX_MD_MOLONGLO)

    if -max_ns < ns_tilt < max_ns and -max_md < md_tilt < max_md:
        is_up = True
    else:
        logger.debug("Source {0} {1} is not up, alt: {2}".format(ra, dec, source.alt))

    # convert to degrees
    md_tilt = np.degrees(md_tilt)
    ns_tilt = np.degrees(ns_tilt)

    return md_tilt, ns_tilt, is_up


def can_be_observed(ra, dec, start_utc, tobs):
    """
    Check if a given source is up from the given start time
    for all the requested observation time.

    Parameters
    ----------
    ra : str
        RA J2000 in hh:mm:ss notation.
    dec : str
        DEC J2000 in hh:mm:ss notation.
    start_utc : ephem.Date object
        Start UTC.
    tobs : float
        Requested observation time in seconds.

    Returns
    -------
    can_be_observed : bool
        Whether the source is up from the given start time and for all
        tobs.
    """

    # convert strings to ephem.Date objects
    ra = ephem.hours(ra)
    dec = ephem.degrees(dec)

    # initialise
    can_be_observed = True

    for item in np.linspace(0, tobs, num=5):
        current_time = ephem.Date(start_utc + item * ephem.second)
        _, _, is_up = get_md_ns_tilt(ra, dec, current_time)

        if not is_up:
            can_be_observed = False
            break

    return can_be_observed


def stop_backend(force=False):
    """
    Stop the backend.

    Parameters
    ----------
    force : bool (optional, default=False)
        Whether the backend should be stopped at all costs, without exiting
        cleanly.
    """

    # initialise
    is_idle = False
    reply = False
    worked = False

    try:
        client = BackendUser()

        # test if backend is idle
        is_idle = client.is_idle()

        if is_idle:
            # it is already idle
            worked = True
        else:
            msg = mobs_to_mpsr_driver_encoder("stop")
            reply, _ = client.send(msg)

            if reply:
                worked = True

    except SocketError:
        logger.error("Could not connect to the backend.")
    except socket.timeout:
        logger.error("Could not connect to the backend. Socket timeout.")
    except Exception as e:
        logger.error("Could not connect to the backend: {0}".format(e))

    if not worked:
        # something went wrong
        logger.critical("Could not issue stop backend command.")
        if not force:
            exit_cleanly(err="backend")


def get_backend_config(config, obs_type, name, ra, dec):
    """
    Determine the backend config based on the requested
    observation type.

    Parameters
    ----------
    config : str
        Observing config mode.
    obs_type : str
        Either stationary, transiting or tracking mode.
    name : str
        Name of object.
    ra : str
        RA of object to observe in equatorial J2000 hh:mm:ss format.
    dec : str
        DEC of object to observe in equatorial J2000 hh:mm:ss format.

    Returns
    -------
    backend_config : dict
        Config dictionary for backend.

    Raises
    ------
    NotImplementedError
        If request config is not implemented.
    RuntimeError
        On invalid configs.
    """

    # from Andrew's php script
    # /home/dada/psrdada/mopsr/web/tmc_simulator.lib.php
    # defaults are critically sampled PFB
    # var $bandwidth = 100;
    # var $cfreq = 849.609375;
    # var $nchan = 128;
    # var $channel_bandwidth = 0.78125;
    # var $oversampling_ratio = 1;
    # var $sampling_time = 1.28;
    # var $dsb = 1;
    # var $nant = 4;
    # var $resolution;

    # stationary vs tracking modes
    if obs_type == "STATIONARY":
        east_tracking = "false"
        west_tracking = "false"
        delay_tracking = "false"

    elif obs_type == "TRANSITING":
        east_tracking = "false"
        west_tracking = "false"
        delay_tracking = "true"

    elif obs_type == "TRACKING":
        east_tracking = "true"
        west_tracking = "true"
        delay_tracking = "true"

    else:
        raise NotImplementedError("Unrecognized obs_type.")

    # set all the modes correctly
    # if one part is not used set it to "mopsr.null"
    _bs_proc_file = ""
    _bf_proc_file = ""

    if config == "INDIV":
        tb = "false"
        modtb = "false"
        corr = "false"
        aq_proc_file = "mopsr.dspsr.gpu.sk"
        bf_proc_file = "mopsr.null"
        bp_proc_file = "mopsr.null"

    elif config == "TB":
        tb = "true"
        modtb = "false"
        corr = "false"
        _bs_proc_file = "mopsr.aqdsp.hires.gpu"
        tb0_proc_file = "mopsr.dspsr.cpu"

    elif config == "MODTB":
        tb = "true"
        modtb = "true"
        corr = "false"
        _bs_proc_file = "mopsr.aqdsp.hires.gpu"
        # bs_proc_file = "mopsr.dspsr.cpu"
        tb0_proc_file = "mopsr.dspsr.cpu"
        bp_proc_file = "mopsr.null"

    elif config == "CORR":
        tb = "false"
        modtb = "false"
        corr = "true"
        _bs_proc_file = "mopsr.aqdsp.hires.gpu"
        _bf_proc_file = "mopsr.calib.hires.pref16.gpu"
        bp_proc_file = "mopsr.null"
        tb0_proc_file = "mopsr.dspsr.cpu"

    elif config == "FB":
        tb = "false"
        modtb = "false"
        corr = "false"
        _bs_proc_file = "mopsr.aqdsp.hires.gpu"
        tb0_proc_file = "mopsr.dspsr.cpu"

    else:
        raise RuntimeError("Invalid config.")

    backend_config = dict(
        bs_source_name=name,
        tb0_source_name=name,
        bs_ra=ra,
        bs_dec=dec,
        tb0_ra=ra,
        tb0_dec=dec,
        bs_ra_units="hhmmss",
        bs_dec_units="hhmmss",
        tb0_ra_units="hhmmss",
        tb0_dec_units="hhmmss",
        bs_proc_file=_bs_proc_file,
        tb0_proc_file=tb0_proc_file,
        bf_proc_file=_bf_proc_file,
        nant="8",
        oversampling_ratio="1",
        nchan="320",
        bw="31.25",
        cfreq="835.5957031",
        nbits="8",
        observer=OBSERVER_NAME,
        tsamp="10.24",
        dual_sideband="1",
        npol="1",
        ndim="2",
        foff="0.09765625",
        resolution="5120",
        bs_project_id="P000",
        tb0_project_id="P000",
        east_tracking=east_tracking,
        west_tracking=west_tracking,
        delay_tracking=delay_tracking,
        east_md_angle="0.0",
        west_md_angle="0.0",
        east_ns_tilt="0.0",
        west_ns_tilt="0.0",
        tb=tb,
        modtb=modtb,
        corr=corr,
    )

    if config in ["TB", "FB"]:
        # support for 512 fanbeams

        cfg_file = os.path.join(
            os.environ["DADA_ROOT"], "share/mopsr_bp_cornerturn.cfg"
        )
        cfg = parse_cfg(cfg_file)

        if cfg["NBEAM"] == "512":
            backend_config["fb_nbeams"] = "512"
            backend_config["fb_beam_spacing"] = "0.0078277886497065"
        elif cfg["NBEAM"] == "352":
            # use default
            pass
        else:
            raise NotImplementedError(
                "Please add configuration for " + " {0} beams.".format(cfg["NBEAMS"])
            )

    return backend_config


def start_backend(config, obs_type, name, ra, dec, tobs):
    """
    Start the backend.
    """

    backend_config = get_backend_config(config, obs_type, name, ra, dec)

    # control/state variables
    observation_started = False
    observation_stopped = False

    # initialise
    reply_prepare = None
    reply_start = None
    reply_prepare_xml = None
    reply_start_xml = None
    prepare_worked = False

    # 1) stop
    stop_backend()

    time.sleep(2)

    # 2) prepare
    try:
        client = BackendUser()
        msg = mobs_to_mpsr_driver_encoder("prepare", backend_config)
        reply_prepare, reply_prepare_xml = client.send(str(msg))

        if reply_prepare:
            prepare_worked = True
    except SocketError:
        logger.error("Could not connect to the backend.")
    except socket.timeout:
        logger.error("Could not connect to the backend. Socket timeout.")
    except Exception as e:
        logger.error("Could not connect to the backend: {0}".format(e))

    if prepare_worked:
        time.sleep(2)

        # 3) start
        try:
            msg = mobs_to_mpsr_driver_encoder("start")
            reply_start, reply_start_xml = client.send(str(msg))

            if reply_start:
                observation_started = True
        except SocketError:
            logger.error("Could not connect to the backend.")
        except socket.timeout:
            logger.error("Could not connect to the backend. Socket timeout.")
        except Exception as e:
            logger.error("Could not connect to the backend: {0}".format(e))

    if observation_started:
        # start and end time
        start_observation = ephem.now()
        end_observation = ephem.Date(start_observation + tobs * ephem.second)

        logger.info("Start backend command issued successfully.")

        print()
        logger.info("Start observation:\t{0} UTC".format(start_observation))
        logger.info("End observation:\t{0} UTC".format(end_observation))
        logger.info("Observation time:\t{0} s".format(tobs))
        print()

        # main wait loop
        while True:
            now = ephem.now()
            time_left = datetime.timedelta(
                seconds=(end_observation.datetime() - now.datetime()).seconds
            )
            # the trailing comma has to be there
            # otherwise a newline is added automatically
            print("\r{0} UTC: Time left: {1} hh:mm:ss.".format(now, time_left), end=" ")

            if now >= end_observation:
                if not observation_stopped:
                    # stop the backend
                    stop_backend()
                    print()
                    logger.info("Stopping observation " + "at {0}.".format(ephem.now()))
                    observation_stopped = True
                    break

            sys.stdout.flush()
            time.sleep(1)

    else:
        # something went wrong
        logger.critical(
            "Could not issue start backend command.\n\n"
            + "Prepare xml response:\n\n%s\n\nStart xml response:\n\n%s",
            reply_prepare_xml,
            reply_start_xml,
        )

        exit_cleanly(err="backend")


def start_backend_thresh(config, obs_type, name, ra, dec, thresh, tmin, tmax):
    """
    Start the backend for a tied-array beam threshold observation.

    Raises
    ------
    RuntimeError
        On invalid parameters.
    """

    if config not in ["TBSNR", "TBINT", "TBTOA"]:
        raise RuntimeError("This mode only works for threshold mode.")

    if tmin > tmax:
        raise RuntimeError("tmin must be <= tmax: {0}, {1}".format(tmin, tmax))

    if config in ["TBSNR", "TBINT"]:
        field = "snr"
    elif config == "TBTOA":
        field = "toa"

    # try to connect to snr_extractor_server
    # if that fails fall back to timed observation with
    # 1) tobs = tmax for TBSNR and TBTOA
    # 2) tobs = tmin for TBINT
    status = get_snr_status()
    if status["name"] is None:
        if config in ["TBSNR", "TBTOA"]:
            start_backend(config, obs_type, name, ra, dec, tmax)
        elif config == "TBINT":
            start_backend(config, obs_type, name, ra, dec, tmin)
        return

    backend_config = get_backend_config("TB", obs_type, name, ra, dec)

    # control/state variables
    observation_started = False

    # initialise
    reply_prepare = None
    reply_start = None
    reply_prepare_xml = None
    reply_start_xml = None
    prepare_worked = False

    # 1) stop
    stop_backend()

    time.sleep(2)

    # 2) prepare
    try:
        client = BackendUser()
        msg = mobs_to_mpsr_driver_encoder("prepare", backend_config)
        reply_prepare, reply_prepare_xml = client.send(str(msg))

        if reply_prepare:
            prepare_worked = True
    except SocketError:
        logger.error("Could not connect to the backend.")
    except socket.timeout:
        logger.error("Could not connect to the backend. Socket timeout.")
    except Exception as e:
        logger.error("Could not connect to the backend: {0}".format(e))

    if prepare_worked:
        time.sleep(2)

        # 3) start
        try:
            msg = mobs_to_mpsr_driver_encoder("start")
            reply_start, reply_start_xml = client.send(str(msg))

            if reply_start:
                observation_started = True
        except SocketError:
            logger.error("Could not connect to the backend.")
        except socket.timeout:
            logger.error("Could not connect to the backend. Socket timeout.")
        except Exception as e:
            logger.error("Could not connect to the backend: {0}".format(e))

    if observation_started:
        # start and end time
        start_observation = ephem.now()
        tmin_obs = ephem.Date(start_observation + tmin * ephem.second)
        tmax_obs = ephem.Date(start_observation + tmax * ephem.second)

        logger.info("Start backend command issued successfully.")

        print()
        logger.info("Start observation:\t{0} UTC".format(start_observation))
        logger.info("{1} threshold:\t{0}".format(thresh, field.upper()))
        logger.info("tmin:\t{0} s".format(tmin))
        logger.info("tmax:\t{0} s".format(tmax))
        print()

        # main wait loop
        while True:
            thresh_valid = False

            now = ephem.now()
            time_left = datetime.timedelta(
                seconds=(tmax_obs.datetime() - now.datetime()).seconds
            )
            status = get_snr_status()

            # check that source names match and set threshold
            if status["name"][0] == name:
                if status["{0}_threshold".format(field)][0] == thresh:
                    thresh_valid = True
                else:
                    set_threshold(field, thresh)

            if not thresh_valid:
                status["rmean_snr"] = 0
                status["{0}_threshold".format(field)] = 0
                status["toa_err"] = 0

            # the trailing comma has to be there
            # otherwise a newline is added automatically
            if config in ["TBSNR", "TBINT"]:
                print(
                    "\r{0} UTC: SNR: {1:.1f}, threshold: {2:.1f}, left: {3} hh:mm:ss.".format(
                        now,
                        status["rmean_snr"][0],
                        status["snr_threshold"][0],
                        time_left,
                    ),
                    end=" ",
                )
            elif config == "TBTOA":
                print(
                    "\r{0} UTC: ToA error: {1:.1f}, threshold: {2:.1f}, left: {3} hh:mm:ss.".format(
                        now, status["toa_err"][0], status["toa_threshold"][0], time_left
                    ),
                    end=" ",
                )

            # 1) maximum time exceeded
            if now >= tmax_obs:
                stop_backend()
                print()
                logger.info(
                    "tmax reached. Stopping observation at "
                    + "{0}.".format(ephem.now())
                )
                break
            # 2) thresh reached
            elif tmin_obs <= now < tmax_obs:
                if (
                    config in ["TBSNR", "TBTOA"]
                    and thresh_valid
                    and status["{0}_status".format(field)][0] == "stable"
                ):
                    stop_backend()
                    print()
                    logger.info(
                        "{1} threshold reached at {0}.".format(
                            ephem.now(), field.upper()
                        )
                    )
                    break
                elif (
                    config == "TBINT"
                    and thresh_valid
                    and status["{0}_status".format(field)][0] != "stable"
                ):
                    stop_backend()
                    print()
                    logger.info(
                        "Intermittent pulsar is not on. Stopping "
                        + "observation at tmin at "
                        + "{0}.".format(ephem.now())
                    )
                    break

            sys.stdout.flush()
            time.sleep(1)

    else:
        # something went wrong
        logger.critical(
            "Could not issue start backend command.\n\n"
            + "Prepare xml response:\n\n%s\n\nStart xml response:\n\n%s",
            reply_prepare_xml,
            reply_start_xml,
        )

        exit_cleanly(err="backend")


def track_source(ra, dec, movement_state, is_pulsar, ns_md_off):
    """
    Track source but do not start backend.

    Parameters
    ----------
    ra : str
        RA in J2000 equatorial coordinates in hh:mm:ss notation.
    dec : str
        DEC in J2000 equatorial coordinates in hh:mm:ss notation.
    """

    # initialise
    on_source = False
    slew_time = 0

    # slew to source
    point(ra, dec, movement_state, ns_md_off, tracking="on")

    time.sleep(4)

    # safety timeout to protect the telescope
    # if it takes longer than this time there must
    # be something wrong
    on_source, slew_time = is_on_source(movement_state, ns_md_off)
    max_slew_time = np.minimum(1.75 * slew_time, 40 * 60)
    start_time = ephem.now()
    end_time = ephem.Date(start_time + max_slew_time * ephem.second)

    while True:
        on_source, slew_time = is_on_source(movement_state, ns_md_off, is_pulsar)

        if not on_source:
            # the trailing comma has to be there
            # otherwise a newline is added automatically
            print(
                "\rSlewing to coordinate: {0} {1}, slew time: {2} hh:mm:ss.".format(
                    ra, dec, datetime.timedelta(seconds=round(slew_time))
                ),
                end=" ",
            )
        else:
            break

        # safety timeout
        # if reached stop the motors and exit
        if ephem.now() > end_time:
            logger.critical(
                "Safety timeout kicked in. "
                + "The telescope was slewing for more than "
                + "{0:.3f} seconds. ".format(max_slew_time)
                + "Turning off the motors to protect the telescope."
            )
            stop_tracking()
            exit_cleanly(err="tcc")

        sys.stdout.flush()
        time.sleep(2)

    print()
    logger.info("We are tracking source ({0} {1}) now.".format(ra, dec))


def posn_on_source(ra, dec, movement_state, ns_md_off):
    """
    Move to source but do not start the backend.

    Parameters
    ----------
    ra : str
        RA in J2000 equatorial coordinates in hh:mm:ss notation.
    dec : str
        DEC in J2000 equatorial coordinates in hh:mm:ss notation.
    """

    # initialise
    on_source = False
    slew_time = 0

    # slew to source
    point(ra, dec, movement_state, ns_md_off, tracking="off")

    time.sleep(4)

    # safety timeout to protect the telescope
    # if it takes longer than this time there must
    # be something wrong
    on_source, slew_time = is_on_source(movement_state, ns_md_off)
    max_slew_time = np.minimum(1.75 * slew_time, 40 * 60)
    start_time = ephem.now()
    end_time = ephem.Date(start_time + max_slew_time * ephem.second)

    while True:
        on_source, slew_time = is_on_source(movement_state, ns_md_off)

        if not on_source:
            # the trailing comma has to be there
            # otherwise a newline is added automatically
            print(
                "\rSlewing to coordinate: {0} {1}, ".format(ra, dec)
                + "slew time: {0} hh:mm:ss.".format(
                    datetime.timedelta(seconds=round(slew_time))
                ),
                end=" ",
            )
        else:
            break

        # safety timeout
        # if reached stop the motors and exit
        if ephem.now() > end_time:
            logger.critical(
                "Safety timeout kicked in. The telescope was slewing for more than {0} s. Turning off the motors to protect the telescope.".format(
                    max_slew_time
                )
            )
            stop_tracking()
            exit_cleanly(err="tcc")

        sys.stdout.flush()
        time.sleep(2)

    print()
    logger.info(
        "We are positioned on source ({0} {1}) now. NOT tracking it.".format(ra, dec)
    )


def park_telescope(movement_state, ns_md_off):
    """
    Park the telescope.

    Parameters
    ----------
    movement_state : dict
        States the NS/MD movement state of telescope. The keys are {ns_east,
        ns_west, md_east, md_west}.
    ns_md_off : dict
        The NS, MD pointing offsets for each arm.
    """

    # initialise
    on_source = False
    slew_time = 0

    # move to park position which is zenith
    # park is equal to wind_stow
    # there is no proper park function in tcc
    wind_stow()

    time.sleep(4)

    # safety timeout to protect the telescope
    # if it takes longer than this time there must
    # be something wrong
    on_source, slew_time = is_on_source(movement_state, ns_md_off)
    max_slew_time = np.minimum(1.75 * slew_time, 40 * 60)
    start_time = ephem.now()
    end_time = ephem.Date(start_time + max_slew_time * ephem.second)

    while True:
        on_source, slew_time = is_on_source(movement_state, ns_md_off)

        if not on_source:
            # the trailing comma has to be there
            # otherwise a newline is added automatically
            print(
                "\rSlewing to park position. Slew time: {0} hh:mm:ss.".format(
                    datetime.timedelta(seconds=round(slew_time))
                ),
                end=" ",
            )
        else:
            break

        # safety timeout
        # if reached stop the motors and exit
        if ephem.now() > end_time:
            logger.critical(
                "Safety timeout kicked in. The telescope was slewing for more than {0} s. Turning off the motors to protect the telescope.".format(
                    max_slew_time
                )
            )
            stop_tracking()
            exit_cleanly(err="tcc")

        sys.stdout.flush()
        time.sleep(1)

    print()
    logger.info("The telescope is parked now.")

    # turn off the motors
    stop_tracking()


def test_connections(move_telescope):
    """
    Test connectivity to the backend, TCC and so on.

    Parameters
    ----------
    move_telescope : bool
        Determines whether the telescope should currently move or not.

    Raises
    ------
    RuntimeError
        If not connection can be established to TCC or the backend.
    """

    tcc_working = False
    backend_working = False

    if move_telescope:
        logger.info("Testing connection to TCC.")

        # open connection to TCC
        # and send query command as a test
        try:
            reply = get_tcc_status()
            if reply:
                tcc_working = True
        except SocketError:
            logger.error("Could not connect to TCC. Is it running?")

        if tcc_working:
            logger.info("Successfully connected to TCC.")
        else:
            raise RuntimeError("TCC is not working.")

    logger.info("Testing connection to the backend.")

    # just try to connect to backend at the moment
    try:
        get_backend_status()
        backend_working = True
    except SocketError:
        logger.error("Could not connect to the backend. Is it running?")

    if backend_working:
        logger.info("Successfully connected to the backend.")
    else:
        raise RuntimeError("The backend is not working.")


def exit_cleanly(err=None):
    """
    Exit the automatic mode cleanly.
    In case of errors make sure that the telescope is safe.
    """

    logger.info("Stopping the automatic mode.")

    # stop the backend
    stop_backend(force=True)
    logger.info("Backend stopped.")

    # stop tracking
    stop_tracking(force=True)
    logger.info("Telescope movement stopped.")

    logger.info("Automatic observing mode stopped.")

    logger.info("Removing block on other interfaces")
    try:
        osremove(SEMAPHORE_DIR + "/" + SEMAPHORE_FILE)
    except OSError:
        logger.info("Failed to remove " + SEMAPHORE_DIR + "/" + SEMAPHORE_FILE)

    if err is None:
        sys.exit()
    else:
        sys.exit(1)


class SignalHandler(object):
    """
    Handle unix signals sent to the program.
    """

    def __init__(self):
        pass

    def handle(self, signum, frame):
        # treat SIGINT/INT/CRTL-C
        if signum == signal.SIGINT:
            print()
            logger.warn("SIGINT received, stopping the automatic observing mode.")
            exit_cleanly()


def get_transit(ra, dec):
    """
    Calculate next transit time for a source at RA and DEC.

    Parameters
    ----------
    ra : str
        RA in hh:mm:ss notation.
    dec : str
        DEC in hh:mm:ss notation.

    Returns
    -------
    rising
        Rising UTC.
    transit
        Transit UTC.
    setting
        Setting UTC.
    """

    # date and time in UTC
    date = ephem.now()

    obs = ephem.Observer()
    obs.long = ephem.degrees(LON_MOLONGLO)
    obs.lat = ephem.degrees(LAT_MOLONGLO)
    obs.elevation = ELEVATION_MOLONGLO
    # set date and time
    obs.date = date

    source = ephem.readdb("source,f|L,{0},{1},0,2000".format(ra, dec))
    source.compute(obs)

    logger.info("RA: {0}, DEC: {1}".format(ra, dec))
    logger.info("RA: {0}, DEC: {1}".format(source.ra, source.dec))

    rising = obs.next_rising(source, start=date)
    transit = obs.next_transit(source, start=date)
    setting = obs.next_setting(source, start=date)

    # check for correct order
    if rising > transit:
        rising = obs.previous_rising(source, start=date)
    if setting < transit:
        setting = obs.previous_setting(source, start=date)

    return rising, transit, setting


def wait_utc(end_utc):
    """
    Wait until a given UTC.
    """

    molonglo = ephem.Observer()
    molonglo.lat = ephem.degrees(LAT_MOLONGLO)
    molonglo.long = ephem.degrees(LON_MOLONGLO)
    molonglo.date = ephem.now()

    # wait loop
    while True:
        # update utc in observatory object
        now = ephem.now()
        molonglo.date = now
        now_lst = molonglo.sidereal_time()
        time_left = datetime.timedelta(
            seconds=(end_utc.datetime() - now.datetime()).seconds
        )
        # the trailing comma has to be there
        # otherwise a newline is added automatically
        print(
            "\r{0} UTC: {1} LST: Time left: {2} hh:mm:ss.".format(
                now, now_lst, time_left
            ),
            end=" ",
        )

        if now >= end_utc:
            print()
            logger.info("Wait command finished at {0}.".format(ephem.now()))
            break

        sys.stdout.flush()
        time.sleep(1)


def wait_sec(seconds):
    """
    Wait for a given amount of seconds.
    """

    # start and end time
    start_time = ephem.now()
    end_time = ephem.Date(start_time + seconds * ephem.second)

    logger.info("Waiting for {0} seconds.".format(seconds))

    wait_utc(end_time)


def wait_lst(end_lst):
    """
    Wait until a given LST.
    """

    # convert to ephem hours object
    end_lst = ephem.hours(end_lst)
    raw_lst = ephem.hours(end_lst)

    molonglo = ephem.Observer()
    molonglo.lat = ephem.degrees(LAT_MOLONGLO)
    molonglo.long = ephem.degrees(LON_MOLONGLO)
    molonglo.date = ephem.now()
    now_lst = molonglo.sidereal_time()

    logger.info("Waiting until {0} LST.".format(end_lst))

    # wrap to next day if necessary
    if end_lst < now_lst:
        logger.warning(
            "{0} LST is over for today, wrapping to the next day.".format(end_lst)
        )
        end_lst = ephem.hours(end_lst + ephem.hours("24:00:00"))

    # wait time in seconds
    wait_time = (end_lst - now_lst) * (24 * 60 * 60 / (2 * np.pi))
    if wait_time > 20 * 60 * 60:
        logger.warning(
            "Telescope will be waiting for more than 20 hours, ignoring the WAIT; LST; {0} statement".format(
                raw_lst
            )
        )
        wait_time = 5
    end_utc = ephem.Date(ephem.now() + wait_time * ephem.second)

    wait_utc(end_utc)


def wait_transit(jname, offset):
    """
    Wait for the transit of a given pulsar.

    Parameters
    ----------
    jname : str
        J2000 name of the pulsar.
    offset : float
        The offset is given in seconds and can be negative (earlier)
        or positve (later) compared with the transit time.
    """

    ra, dec = get_ra_dec(jname)
    rising, transit, setting = get_transit(ra, dec)
    logger.info("Rising: {0} UTC".format(rising))
    logger.info("Transit: {0} UTC".format(transit))
    logger.info("Setting: {0} UTC".format(setting))

    end_utc = ephem.Date(transit + offset * ephem.second)
    logger.info("Waiting until {0} UTC.".format(end_utc))

    wait_utc(end_utc)


def get_slew_time(md1, ns1, md2, ns2):
    """
    Calculate the slew time between two positions.

    Returns
    -------
    t : str
        Slewtime in seconds between the two positions.
    """

    # slew rates of molonglo in deg/s
    v_md = 2.5 / 60.0
    v_ns_fast = 4.8 / 60.0
    v_ns_slow = 1.0 / 60.0

    # the telescope slews like this:
    # 1) ramp up to v_md and v_ns_fast
    # 2) it slews directly to the source in both dimensions with speeds
    # v_md and v_ns_fast
    # 3) once it gets into 1 deg of distance to the source it
    # slews with speeds v_md and v_ns_slow
    # 4) ramp down

    distance_md = abs(md2 - md1)
    distance_ns = abs(ns2 - ns1)

    t_ramp = 8.0

    t_md = distance_md / v_md

    if distance_ns <= 1.0:
        t_ns = distance_ns / v_ns_slow
    else:
        t_ns = (distance_ns - 1.0) / v_ns_fast + 1.0 / v_ns_slow

    # we have to wait for the slowest slew dimension
    t_uniform = np.maximum(t_md, t_ns)

    t = t_ramp + t_uniform + t_ramp

    logger.debug("Total slew time: {0} s".format(t))

    return t


def sanity_check(line, movement_state):
    """
    Check if a given schedule line can be processed at the current time.
    Check:
    1) Source is up for at least tslew + tobs.

    Returns
    -------
    line_works : bool
        Return True if all is good, otherwise False.
    """

    # allow track and posn to any possible coords
    # park and wait don't have ra, dec
    if line.obs_mode in ["TRACK", "POSN", "PARK", "WAIT"]:
        return True

    # compute position of the source now
    md, ns, _ = get_md_ns_tilt(line.ra, line.dec, date=ephem.now())

    # initialise
    slew_time = None
    line_works = False

    # query telescope tracking status
    status = get_status()

    if status is not None:
        # calculate slew times
        slew_time_west = get_slew_time(
            status["west_md"][0], status["west_ns"][0], md, ns
        )

        slew_time_east = get_slew_time(
            status["east_md"][0], status["east_ns"][0], md, ns
        )

        # check if only one arm is used
        # and calculate total slew time
        if movement_state["ns_west"] == "disabled":
            slew_time = slew_time_east
        elif movement_state["ns_east"] == "disabled":
            slew_time = slew_time_west
        else:
            slew_time = np.maximum(slew_time_west, slew_time_east)

    t = slew_time + line.tmax

    line_works = can_be_observed(line.ra, line.dec, ephem.now(), t)

    return line_works


def check_other_interfaces(state_dir=SEMAPHORE_DIR, state_file=SEMAPHORE_FILE):
    import glob

    state_file_list = glob.glob(state_dir + "/" + state_file)

    if len(state_file_list) == 1:
        with open(state_file_list[0]) as fh:
            running_interface = fh.readlines()[0]

        return True, running_interface

    elif len(state_file_list) > 1:
        return True, "Weird, check " + state_dir + "/" + state_file

    return False, "None"


def block_other_interfaces():
    statfile = os.path.join(SEMAPHORE_DIR, SEMAPHORE_FILE)

    with open(statfile, "w") as f:
        f.write("AUTOMATIC_MODE")


def run_schedule(schedule, debug, move_telescope, movement_state, ns_md_off=None):
    """
    This is the main loop that processes a schedule.

    Parameters
    ----------
    schedule : list
        The schedule to run.
    debug : bool
        Test the operation of the function in debug mode.
    move_telescope : bool
        Determines whether the telescope should be moved.
    movement_state : dict
        States the NS/MD movement state of telescope. The keys are {ns_east,
        ns_west, md_east, md_west}.
    ns_md_off : dict (optional, default=None)
        The NS, MD pointing offsets for each arm.
    """

    if ns_md_off is None:
        ns_md_off = {
            "ns_east_offset": 0.0,
            "ns_west_offset": 0.0,
            "md_east_offset": 0.0,
            "md_west_offset": 0.0,
        }

    # obs_modes that do not use the backend
    no_backend_modes = ["TRACK", "POSN", "PARK", "WAIT"]

    line_number = 1

    for line in schedule:
        # sanity check
        if not sanity_check(line, movement_state):
            logger.error(
                "Source cannot be observed for tslew + tobs. Skipping to next line."
            )
            continue

        # ensure a clean state
        if not debug:
            stop_backend()

        if move_telescope:
            if line.obs_mode == "TRACK":
                logger.info(
                    "Processing line {0}: {1} {2} {3}".format(
                        line_number, line.obs_mode, line.ra, line.dec
                    )
                )
                track_source(line.ra, line.dec, movement_state, False, ns_md_off)

            elif line.obs_mode == "POSN":
                logger.info(
                    "Processing line {0}: {1} {2} {3}".format(
                        line_number, line.obs_mode, line.ra, line.dec
                    )
                )
                posn_on_source(line.ra, line.dec, movement_state, ns_md_off)

            elif line.obs_mode == "PARK":
                logger.info(
                    "Processing line {0}: {1}".format(line_number, line.obs_mode)
                )
                park_telescope(movement_state, ns_md_off)

        if line.obs_mode == "WAIT":
            logger.info(
                "Processing line {0}: {1} {2}".format(
                    line_number, line.obs_mode, line.wait_mode
                )
            )
            # treat different functionality
            if line.wait_mode == "SEC":
                wait_sec(line.wait_sec)
            elif line.wait_mode == "LST":
                wait_lst(line.wait_lst)
            elif line.wait_mode == "TRANSIT":
                # XXX: Fix me
                wait_transit(wait_lst, line.wait_offset)

        # INDIV, TB and MODTB
        elif line.obs_mode in ["INDIV", "TB", "MODTB"]:
            logger.info(
                "Processing line {0}: {1} {2} {3} {4}".format(
                    line_number, line.obs_mode, line.obs_type, line.name, line.tmax
                )
            )
            if line.tracking:
                track_source(line.ra, line.dec, movement_state, False, ns_md_off)

            start_backend(
                line.obs_mode, line.obs_type, line.name, line.ra, line.dec, line.tmax
            )

        # TBSNR and TBINT
        elif line.obs_mode in ["TBSNR", "TBINT"]:
            logger.info(
                "Processing line {0}: {1} {2} {3} {4} {5} {6}".format(
                    line_number,
                    line.obs_mode,
                    line.obs_type,
                    line.name,
                    line.snr,
                    line.tmin,
                    line.tmax,
                )
            )
            if line.tracking:
                track_source(line.ra, line.dec, movement_state, True, ns_md_off)

            start_backend_thresh(
                line.obs_mode,
                line.obs_type,
                line.name,
                line.ra,
                line.dec,
                line.snr,
                line.tmin,
                line.tmax,
            )

        # TBTOA
        elif line.obs_mode == "TBTOA":
            logger.info(
                "Processing line {0}: {1} {2} {3} {4} {5} {6}".format(
                    line_number,
                    line.obs_mode,
                    line.obs_type,
                    line.name,
                    line.toa,
                    line.tmin,
                    line.tmax,
                )
            )
            if line.tracking:
                track_source(line.ra, line.dec, movement_state, True, ns_md_off)

            start_backend_thresh(
                line.obs_mode,
                line.obs_type,
                line.name,
                line.ra,
                line.dec,
                line.toa,
                line.tmin,
                line.tmax,
            )

        # CORR and FB
        elif line.obs_mode in ["CORR", "FB"]:
            logger.info(
                "Processing line {0}: {1} {2} {3} {4} {5} {6}".format(
                    line_number,
                    line.obs_mode,
                    line.obs_type,
                    line.name,
                    line.ra,
                    line.dec,
                    line.tmax,
                )
            )
            if line.tracking:
                track_source(line.ra, line.dec, movement_state, False, ns_md_off)

            start_backend(
                line.obs_mode, line.obs_type, line.name, line.ra, line.dec, line.tmax
            )

        # let backend cool down
        if line.obs_mode not in no_backend_modes:
            logger.info("Letting backend cool down for 30 seconds.")
            time.sleep(30)

        line_number += 1
