#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2014-2018 Fabian Jankowski
#   Automatic observing mode for Molonglo - server.
#

from __future__ import print_function
import argparse
import logging
from logging.handlers import RotatingFileHandler, SMTPHandler
import os.path
import signal

# py2 vs py3
try:
    import SocketServer
except ImportError:
    import socketserver as SocketServer
import socket
import sys
import threading

# py2 vs py3
try:
    import Queue
except ImportError:
    import queue as Queue

from molsoft.am_helpers import (
    run_schedule,
    test_connections,
    SignalHandler,
    exit_cleanly,
)
from molsoft.config_helpers import get_config
from molsoft.schedule_helpers import (
    load_schedule,
    test_schedule_file,
    calculate_times,
    split_max_tobs,
)
from molsoft.coord_helpers import get_lst
from molsoft.version import __version__


#
# set up logging
#

# root logger
logger = logging.getLogger("automatic_mode")
logger.setLevel(logging.DEBUG)
logger.propagate = False

# log to file
try:
    logfile = RotatingFileHandler(
        os.path.expanduser("~/schedule/logs/automatic_mode_server.log"),
        maxBytes=2.0e7,
        backupCount=5,
    )
except IOError as e:
    print("Could not enable logging to file: {0}".format(str(e)))
else:
    logfile.setLevel(logging.DEBUG)
    logfile_formatter = logging.Formatter(
        "%(asctime)s, %(name)s, %(levelname)s: %(message)s"
    )
    logfile.setFormatter(logfile_formatter)
    logger.addHandler(logfile)

# also log to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console_formatter = logging.Formatter("%(message)s")
console.setFormatter(console_formatter)
logger.addHandler(console)


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    """
    A server side code for communicating with dynamic scheduler and am_client.
    """

    def handle(self):
        """
        Handle a TCP connection. This is the main function.
        """

        mode = self.request.recv(4096)
        self.queue1 = self.server.queue1

        if mode == "ManualSchedule":
            logger.info("Manual scheduling activated.")

            pname = self.request.recv(4096)
            man_schedule = load_schedule(pname)

            # limit the maximum observation time per single integration
            schedule = split_max_tobs(man_schedule, 1800)

            for line in schedule:
                self.queue1.put(line)

            self.request.sendall("Manual schedule received.")

        elif mode == "pulsarsready":
            logger.info("Dynamic scheduling activated.")

            pulsars = self.request.recv(4096)
            dynamic_schedule = load_schedule(pulsars)
            for line in dynamic_schedule:
                print(line)

            # limit the maximum observation time per single integration
            schedule = split_max_tobs(dynamic_schedule, 1800)

            for line in schedule:
                self.queue1.put(line)

            self.request.sendall("Dynamic scheduling activated.")

        else:
            logger.error("Mode not implemented: {0}".format(mode))


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    def __init__(self, server_address, RequestHandlerClass, queue1):
        SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)

        self.queue1 = queue1


def request_pulsars(ds_config):
    """
    Request new pulsars from the scheduler.

    Parameters
    ----------
    ds_config: tuple
        Tuple containing (hostname, port) of the scheduler.
    """

    logger.info("Requesting more pulsars from the scheduler.")

    DS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to DS server
        DS.connect(ds_config)
        DS.sendall("DynamicSchedule")

        logger.info("More pulsars requested.")
    finally:
        DS.close()


def worker(pulsars, ds_config, debug, move_telescope, movement_state):
    """
    A worker thread.
    """

    while True:
        item = pulsars.get()
        logger.info("Processing schedule line: {0}".format(item))

        run_schedule([item], debug, move_telescope, movement_state)
        pulsars.task_done()

        # XXX: Fix me
        if item.obs_mode.startswith("TB"):
            request_pulsars(ds_config)


#
# MAIN
#


def main():
    # send email on severe errors
    config = get_config()
    log_emails = []

    for item in config["automatic_mode"]["log_emails"]:
        log_emails.append(item.replace(" AT ", "@"))

    email = SMTPHandler(
        mailhost="127.0.0.1",
        fromaddr="fjankowski@swin.edu.au",
        toaddrs=log_emails,
        subject="Molonglo automatic mode error log",
    )
    email.setLevel(logging.CRITICAL)
    email_formatter = logging.Formatter(
        "%(asctime)s, %(name)s, %(levelname)s: %(message)s"
    )
    email.setFormatter(email_formatter)
    logger.addHandler(email)

    grammar_help = """
Signal handling
  The program handles the following Unix signals sent to it:

  * INT/SIG-INT/CTRL-C:
  If an INT signal is received (i.e. CTRL-C) the program will stop the
  backend, stop the movement of the telescope and exit gracefully. It will not
  park the telescope.

Safety system
  The program has an internal safety system to protect the telescope from
  damage. Before every slewing operation it calculates how long the operation
  should take if everything works correctly. If the physical slewing operation
  takes longer than that (plus a small margin) it will stop the motors to
  protect the telescope, issue a critical error and exit gracefully.

Email alerts
  The program will send out email alerts when critical errors occur. It will
  send them to the following email adresses:
  {0}
""".format(
        ", ".join(log_emails)
    )

    # handle command line arguments
    parser = argparse.ArgumentParser(
        description="Molonglo automatic observing mode - server.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=grammar_help,
    )
    parser.add_argument(
        "--ns_east",
        dest="ns_east_state",
        type=str,
        default="auto",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the NS movement of the east arm, default auto",
    )
    parser.add_argument(
        "--ns_west",
        dest="ns_west_state",
        type=str,
        default="auto",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the NS movement of the west arm, default auto",
    )
    parser.add_argument(
        "--md_east",
        dest="md_east_state",
        type=str,
        default="disabled",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the MD drive of the east arm, default auto",
    )
    parser.add_argument(
        "--md_west",
        dest="md_west_state",
        type=str,
        default="disabled",
        choices=["auto", "slow", "disabled"],
        help="Specify the status of the MD drive of the west arm, default auto",
    )
    parser.add_argument(
        "--disable_movement",
        dest="move_telescope",
        action="store_false",
        default=True,
        help="Disable any telescope movement.",
    )
    parser.add_argument(
        "--npulsars",
        dest="npulsars",
        type=int,
        default=1,
        help="Number of pulsars to ask from scheduler (default: 1).",
    )
    parser.add_argument(
        "--debug",
        dest="debug",
        action="store_true",
        default=False,
        help="Enable debug mode. (default: false)",
    )
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    print("Molonglo automatic observing mode - server")
    print("==========================================")

    movement_state = {
        "ns_east": args.ns_east_state,
        "ns_west": args.ns_west_state,
        "md_east": args.md_east_state,
        "md_west": args.md_west_state,
    }

    # start signal handler
    sh = SignalHandler()
    signal.signal(signal.SIGINT, sh.handle)

    # all telescope movement is disabled
    if not args.move_telescope:
        logger.info("All telescope movement is disabled.")
        args.park = False

    if args.ns_east_state == "disabled":
        logger.info("East arm is disabled.")
    if args.ns_west_state == "disabled":
        logger.info("West arm is disabled.")

    # test connections to TCC and the backend
    if not args.debug:
        test_connections(args.move_telescope)

    logger.info("Automatic mode started.")
    sys.stdout.flush()

    # server configs
    am_config = (
        config["automatic_mode"]["server"]["host"],
        config["automatic_mode"]["server"]["port"],
    )
    ds_config = (
        config["scheduler"]["server"]["host"],
        config["scheduler"]["server"]["port"],
    )

    # dispatcher thread
    worker_queue = Queue.Queue(5)
    t = threading.Thread(
        target=worker,
        args=(worker_queue, ds_config, args.debug, args.move_telescope, movement_state),
    )
    t.daemon = True
    t.start()

    # tcp listener thread
    queue_tcp = Queue.Queue(5)
    server = ThreadedTCPServer(am_config, ThreadedTCPRequestHandler, queue_tcp)
    server_thread = threading.Thread(name="AM_tcpserver", target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()

    # main loop
    while True:
        try:
            test = queue_tcp.get(timeout=0.2)
            logger.info("Sending to worker thread: {0}".format(test))
            worker_queue.put(test)
        except Queue.Empty:
            pass

    server.shutdown()
    server.server_close()

    logger.info("All done.")


# if run directly
if __name__ == "__main__":
    main()
