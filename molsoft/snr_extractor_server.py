#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2015-2018 Fabian Jankowski
#   SNR extractor for Molonglo - server.
#

from __future__ import print_function
import argparse
from datetime import datetime
import glob
import logging
from logging.handlers import RotatingFileHandler
import os.path
from string import Template
import sys
import threading
import time
import shlex

# py2 vs py3
try:
    import SocketServer
except ImportError:
    import socketserver as SocketServer
# py2 vs py3
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
import subprocess

from lxml import etree
import numpy as np

try:
    import psrchive as ps
except ImportError:
    print("Could not import PSRCHIVE python bindings.")

try:
    from anansi.misc.radon import radon_convolve
except ImportError:
    print("Could not import anansi.")

from molsoft.config_helpers import get_config
from molsoft.db_helpers import init_db, insert_into_db, get_observations
from molsoft.version import __version__


# pylint incorrectly identifies various numpy function to not return
# disable the corresponding pylint test for now
# pylint: disable=E1111

#
# set up logging
#

# root logger
logger = logging.getLogger("automatic_mode")
logger.setLevel(logging.DEBUG)
logger.propagate = False

# log to file
try:
    logfile = RotatingFileHandler(
        os.path.expanduser("~/schedule/logs/snr_extractor_server.log"),
        maxBytes=2.0e7,
        backupCount=5,
    )
except IOError as e:
    print("Could not enable logging to file: {0}".format(str(e)))
else:
    logfile.setLevel(logging.DEBUG)
    logfile_formatter = logging.Formatter(
        "%(asctime)s, %(name)s, %(levelname)s, %(threadName)s: %(message)s"
    )
    logfile.setFormatter(logfile_formatter)
    logger.addHandler(logfile)

# also log to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console_formatter = logging.Formatter("%(levelname)s, %(threadName)s: %(message)s")
console.setFormatter(console_formatter)
logger.addHandler(console)


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    """
    A threaded TCP request handler.
    """

    def handle(self):
        raw_data = self.request.recv(1024)

        # parse
        xml = None
        command = None
        reply = "<xml><reply>fail</reply></xml>"
        try:
            xml = etree.fromstring(raw_data)
        except etree.XMLSyntaxError:
            logger.error("Could not parse the XML: {0}".format(raw_data))

        try:
            command = xml.find("command").text
            snr_threshold = float(xml.find("snr_threshold").text)
        except AttributeError:
            pass
        else:
            if command == "set_snr_threshold":
                with self.server.lock:
                    self.server.observation.snr_threshold = snr_threshold
                    logger.info(
                        "The SNR threshold was updated: {0}".format(
                            self.server.observation.snr_threshold
                        )
                    )
                reply = "<xml><reply>ok</reply></xml>"

        try:
            command = xml.find("command").text
            toa_threshold = float(xml.find("toa_threshold").text)
        except AttributeError:
            pass
        else:
            if command == "set_toa_threshold":
                with self.server.lock:
                    self.server.observation.toa_threshold = toa_threshold
                    logger.info(
                        "The ToA threshold was updated: {0}".format(
                            self.server.observation.toa_threshold
                        )
                    )
                reply = "<xml><reply>ok</reply></xml>"

        try:
            command = xml.find("command").text
        except AttributeError:
            pass
        else:
            if command == "query":
                reply = gen_xml(self.server.observation)

        self.request.sendall(str(reply))


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    """
    A threaded TCP Server.
    """

    daemon_threads = True
    allow_reuse_address = True

    def __init__(self, server_address, RequestHandlerClass, lock, observation):
        SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)

        self.lock = lock
        self.observation = observation


def use_gaussian_baseline_estimator(restore=False):
    """
    Adjust PSRCHIVE to use the Gaussian baseline estimator (GaussianBaseline)
    instead of the default one (BaselineWindow), as it is much better at
    estimating the baseline correctly for accurate flux density measurements.

    We do this by setting the corresponding option in
    ~/.psrchive.cfg

    Parameters
    ----------
    restore : bool (optional, default=False)
        If true, restore the backup of the original config file.
    """

    config_file = os.path.expanduser("~/.psrchive.cfg")

    if not restore:
        if os.path.isfile(config_file):
            # make a backup copy of the current configuration file
            os.rename(config_file, config_file + ".bak")

        # install the new one
        with open(config_file, "w") as f:
            f.write("Profile::baseline = normal\n")

    else:
        if os.path.isfile(config_file + ".bak"):
            os.rename(config_file + ".bak", config_file)


class Observation:
    """
    A class for the current observation.
    """

    def __init__(self, basedir, filename):
        self.basedir = basedir
        self.filename = filename
        self.name = None
        self.snr = 0
        self.snr_radon = 0
        self.rmean_snr = 0
        self.tobs = 0
        self.snr_status = None
        self.snr_threshold = 0
        self.toa_err = 0
        self.toa_status = None
        self.toa_threshold = 5.0
        self.finished = False

    def set_snr_threshold(self, lock, threshold):
        """
        Set the S/N threshold.

        Parameters
        ----------
        lock
            The threading lock object.
        threshold : float
            The new threshold.
        """

        with lock:
            self.snr_threshold = threshold

    def check_finished(self):
        """
        Check if the observation is finished.

        Returns
        -------
        finished : bool
            Whether the observation is finished.
        """

        finished = False

        # go up one directory
        up_dir = os.path.dirname(os.path.dirname(self.filename))
        finished_file = os.path.join(up_dir, "obs.finished")

        if os.path.isfile(finished_file):
            finished = True

        return finished

    def estimate_snr(self, lock):
        """
        Estimate the S/N of this observation.
        This is the main loop.

        Parameters
        ----------
        lock
            The threading lock object.
        """

        total = None
        last_mtime = None
        start_check = time.time()
        end_check = start_check + 20.0

        while True:
            if time.time() >= end_check:
                # check for newer observation
                new_obs = get_current_observation(self.basedir)
                if new_obs != self.filename:
                    with lock:
                        self.finished = True
                        self.__init__(self.basedir, new_obs)
                    total = None
                    last_mtime = None
                start_check = time.time()
                end_check = start_check + 20.0

            # check if file has changed
            mtime = os.stat(self.filename).st_mtime

            if last_mtime is None:
                last_mtime = mtime

            if mtime <= last_mtime:
                time.sleep(1)
                continue
            else:
                logger.info("File has changed: {0}".format(self.filename))
                last_mtime = mtime

            try:
                data = get_info(self.filename)
            except RuntimeError as e:
                logger.error("Could not get info from archive: {0}".format(str(e)))
                continue

            # get toa error
            toa_err = get_toa_error(total)
            if toa_err is not None:
                data["toa_err"] = toa_err

            if total is None:
                total = np.copy(data)
            else:
                total = np.append(total, data)

            # compute running mean snr
            total = update_rmean_snr(total)

            # update values
            with lock:
                self.name = total["name"][-1]
                self.snr = total["snr"][-1]
                self.snr_radon = total["snr_radon"][-1]
                self.rmean_snr = total["rmean_snr"][-1]
                self.tobs = total["tobs"][-1]
                self.snr_status = get_snr_status(total, self.snr_threshold)
                self.toa_err = total["toa_err"][-1]
                self.toa_status = get_toa_status(total, self.toa_threshold)
                self.finished = self.check_finished()

            snr_20min = get_snr_in_x_min(
                total["rmean_snr"][-1], total["tobs"][-1], 20 * 60
            )

            logger.info(
                "tobs: {0:.1f} min, S/N: {1:.1f} (20 min: {2:.1f} S/N), ToA err: {3:.1f} us".format(
                    self.tobs / 60.0, self.snr, snr_20min, self.toa_err
                )
            )


def snr_worker(lock, observation):
    """
    A S/N worker thread.
    """

    logger.debug("Starting the SNR worker thread.")
    observation.estimate_snr(lock)
    logger.debug("SNR worker thread done.")


def db_worker(db_filename, basedir, first_run):
    """
    A database worker thread.
    """

    logger.debug("Starting the DB worker thread.")

    # process old_results as well
    if first_run:
        update_dirty_db(db_filename, basedir, True)

    while True:
        update_dirty_db(db_filename, basedir, False)
        time.sleep(60)

    logger.debug("DB worker thread done.")


def gen_xml(obs):
    """
    Generate observation status as XML message.
    """

    template = Template(
        """\
<xml>
<reply>ok</reply>
<name>$name</name>
<filename>$filename</filename>
<snr>$snr</snr>
<rmean_snr>$rmean_snr</rmean_snr>
<snr_radon>$snr_radon</snr_radon>
<tobs>$tobs</tobs>
<snr_status>$snr_status</snr_status>
<snr_threshold>$snr_threshold</snr_threshold>
<toa_err>$toa_err</toa_err>
<toa_status>$toa_status</toa_status>
<toa_threshold>$toa_threshold</toa_threshold>
<finished>$finished</finished>
</xml>
"""
    )

    data = dict(
        name=obs.name,
        filename=obs.filename,
        snr=obs.snr,
        rmean_snr=obs.rmean_snr,
        snr_radon=obs.snr_radon,
        tobs=obs.tobs,
        snr_status=obs.snr_status,
        snr_threshold=obs.snr_threshold,
        toa_err=obs.toa_err,
        toa_status=obs.toa_status,
        toa_threshold=obs.toa_threshold,
        finished=obs.finished,
    )

    filled = template.substitute(data)

    return filled


def update_rmean_snr(data):
    """
    Update the running mean S/N in the data.
    """

    # compute the window so that it is at least a minute
    # and includes a minimum of 4 subintegrations
    window = np.maximum(4 * data["subint_time"][0], 60.0)
    if data["subint_time"][0] > 0:
        window = int(np.ceil(window / data["subint_time"][0]))
    else:
        window = 6

    logger.debug("The running mean window is: {0}".format(window))

    rmean = running_mean(data["snr"], window)
    data["rmean_snr"] = rmean

    return data


def get_snr_status(data, threshold):
    """
    Determine if the S/N is stable and trustworthy.
    Check if the S/N was above the threshold for at least a minute
    and at least 3 subintegrations.

    Parameters
    ----------
    data : numpy rec
        The observation data.
    threshold : float
        The S/N threshold.

    Returns
    -------
    status : str or None
        Stable str if stable, otherwise None.
    """

    status = None

    lookback_time = np.maximum(3 * data["subint_time"], 60.0)
    last_data = data[data["tobs"] >= data["tobs"][-1] - lookback_time]

    if (
        len(data) >= 5
        and threshold > 0
        and np.all(last_data["rmean_snr"] >= threshold)
        and data["tobs"][-1] >= 60.0
    ):
        status = "stable"
        logger.debug("The SNR is stable.")

    return status


def get_toa_status(data, threshold):
    """
    Check if the improvement in ToA error (dtoa) was below the threshold
    for at least a minute and at least 3 subintegrations.

    Parameters
    ----------
    data : numpy rec
        The observation data.
    threshold : float
        The delta ToA threshold.

    Returns
    -------
    status : str or None
        Stable str if stable, otherwise None.
    """

    status = None

    lookback_time = np.maximum(3 * data["subint_time"], 60.0)
    last_data = data[data["tobs"] >= data["tobs"][-1] - lookback_time]

    if np.all(last_data["toa_err"][0:-1] > 0):
        dtoa = 100 * np.diff(last_data["toa_err"]) / last_data["toa_err"][0:-1]
    else:
        dtoa = None

    logger.info("Percentual difference in ToA err: {0}".format(dtoa))

    if (
        len(data) >= 5
        and threshold > 0
        and dtoa is not None
        and np.all(np.abs(dtoa) <= threshold)
        and data["tobs"][-1] >= 60.0
    ):
        status = "stable"
        logger.debug("The ToA error is stable.")

    return status


def get_obs_info(filename):
    """
    Get some basic info about the observation.

    Parameters
    ----------
    filename : str
        The filename of the observation.

    Returns
    -------
    data : numpy rec
        Basic info about observation.
    """

    utc_format = "%Y-%m-%d-%H:%M:%S"

    # sanity check that file exists
    if not os.path.isfile(filename):
        raise RuntimeError("The file does not exist: {0}".format(filename))

    dtype = [
        ("utc", "|S32"),
        ("filename", "|S2048"),
        ("source_name", "|S16"),
        ("nbin", "int"),
        ("nchan", "int"),
        ("npol", "int"),
        ("nsubint", "int"),
        ("freq", "float"),
        ("bw", "float"),
        ("tobs", "float"),
        ("snr", "float"),
        ("finished", "int"),
    ]
    data = np.zeros(1, dtype=dtype)

    ar = ps.Archive_load(filename)

    # check that it's a pulsar observation
    if ar.get_type() != "Pulsar":
        raise RuntimeError("This is not a pulsar observation: {0}".format(filename))

    # utc is the utc timestamp of the start of the observation
    # in caspsr data there might be a one second difference
    # to the naming of the directory
    data["utc"] = ar.start_time().datestr(utc_format)

    # get properties
    data["filename"] = ar.get_filename()
    data["source_name"] = ar.get_source()
    data["nbin"] = ar.get_nbin()
    data["nchan"] = ar.get_nchan()
    data["npol"] = ar.get_npol()
    data["nsubint"] = ar.get_nsubint()
    data["freq"] = ar.get_centre_frequency()
    data["bw"] = ar.get_bandwidth()
    data["tobs"] = ar.integration_length()

    # compute S/N ratio
    ar.fscrunch()
    ar.tscrunch()
    ar.pscrunch()
    profile = ar.get_Profile(0, 0, 0)
    data["snr"] = profile.snr()

    return data


def get_info(filename):
    """
    Compute the current S/N ratio of an archive and extract various
    other information from it.
    """

    # sanity check that file exists
    if not os.path.isfile(filename):
        logger.error("The file does not exist: {0}".format(filename))
        sys.exit(1)

    dtype = [
        ("snr", "float"),
        ("rmean_snr", "float"),
        ("tobs", "float"),
        ("snr_radon", "float"),
        ("name", "|S64"),
        ("filename", "|S2048"),
        ("nsubint", "int"),
        ("subint_time", "float"),
        ("nbin", "int"),
        ("toa_err", "float"),
    ]
    info = np.zeros(1, dtype=dtype)

    ar = ps.Archive_load(filename)

    # check that it's a pulsar observation
    if ar.get_type() != "Pulsar":
        logger.error("This is not a pulsar observation: {0}".format(filename))
        return None

    # get properties
    info["name"] = ar.get_source()
    info["filename"] = ar.get_filename()
    info["nsubint"] = ar.get_nsubint()
    info["nbin"] = ar.get_nbin()

    # remove baseline
    ar.remove_baseline()

    # dedisperse
    ar.dedisperse()

    ar.fscrunch()
    ar.pscrunch()

    # get the snr using the radon transform
    data = ar.get_data().squeeze()
    try:
        info["snr_radon"] = radon_convolve(
            data, 2 ** np.arange(int(np.log2(info["nbin"][0]))).astype("int32")
        )[0]
    except Exception as e:
        logger.error("Could not get radon SNR: {0}".format(str(e)))

    ar.tscrunch()

    # get snr
    command = 'psrstat -j "FT" -c snr=pdmp -c snr -Q -q {0}'.format(filename)
    args = shlex.split(command)

    fnull = open(os.devnull, "w")
    proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=fnull)

    result = proc.communicate()[0]
    info["snr"] = result

    info["tobs"] = ar.integration_length()
    if info["nsubint"] > 0:
        info["subint_time"] = info["tobs"] / info["nsubint"]

    return info


def get_snr_in_x_min(snr0, t0, t=20 * 60):
    """
    Extrapolate the SNR from SNR0 at t0 to a time t.

    Parameters
    ----------
    snr0 : float
        SNR at t0.
    t0 : float
        Observation time t0 in seconds.
    t : float (optional)
        Observation time t in seconds. Default is 20 min.

    Returns
    -------
    snr : float
        SNR at time t.
    """

    snr = snr0 * np.sqrt(t / t0)

    return snr


def get_current_observation(basedir):
    """
    Return the current observation.

    Parameters
    ----------
    basedir : str
        The base directory to operate in.

    Returns
    -------
    current_file : str
        Filename of the current observation. The current observation is the
        newest one that is not finished yet.
    """

    # pre multiple TBs
    files = glob.glob(
        os.path.join(basedir, "results/????-??-??-??:??:??/TB/J*[+-]*_t.tot")
    )

    # including multiple TBs
    files += glob.glob(
        os.path.join(basedir, "results/????-??-??-??:??:??/J*[+-]*/J*[+-]*_t.tot")
    )
    files = sorted(files, reverse=True)

    current_file = files[0]

    # sanity checking
    if os.path.isfile(current_file):
        pass
    else:
        logger.error("Current file is invalid: {0}".format(current_file))
        sys.exit(1)

    return current_file


def running_mean(data, n):
    """
    Compute a running mean over the data with window size n.
    """

    rmean = np.cumsum(data)

    # special case if n > len(data)
    if n > len(data) and len(data) > 0:
        n = len(data)

    rmean[n:] = rmean[n:] - rmean[:-n]
    rmean = rmean / n

    return rmean


def get_std_profile(jname):
    """
    Get the filename of the standard profile for the given pulsar.

    Parameters
    ----------
    jname : str
        J2000 name of the pulsar.

    Returns
    -------
    std_profile : str
        Filename of the standard profile for the given pulsar.

    Raises
    ------
    RuntimeError
        If there is no standard profile for the given pulsar.
    """

    # path to all ephemerides
    config = get_config()
    ephem_path = os.path.expanduser(config["snr_extractor"]["ephem_path"])

    std_profile = os.path.join(ephem_path, jname, "{0}.835.std".format(jname))

    # check if file exists
    if not os.path.isfile(std_profile):
        logger.error("Standard profile does not exist: {0}".format(std_profile))
        raise RuntimeError("Standard profile does not exist: {0}".format(std_profile))

    return std_profile


def get_toa_error(data):
    """
    Compute the ToA error for the current observation.

    Parameters
    ----------
    data : numpy rec
        The observation data.

    Returns
    -------
    toa_err : float or None
        ToA error of the current observation or None if not possible to
        extract.
    """

    if data is None:
        return None

    filename = data["filename"][-1]
    jname = data["name"][-1]

    try:
        std = get_std_profile(jname)
    except Exception as e:
        logger.error("{0}".format(str(e)))
        return None

    command = 'pat -A FDM -u -j "FT" -f tempo2 -s {0} {1}'.format(std, filename)
    args = shlex.split(command)
    fnull = open(os.devnull, "w")
    proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=fnull)

    result = proc.communicate()[0]

    result = StringIO(result)

    # parse result
    dtype = [
        ("filename", "|S2048"),
        ("freq", "float"),
        ("mjd", "float"),
        ("toa_err", "float"),
        ("observatory", "|S16"),
    ]
    toa = np.genfromtxt(
        result, dtype=dtype, delimiter="", autostrip=False, comments="FORMAT"
    )

    return toa["toa_err"]


def update_dirty_db(filename, basedir, first_run):
    """
    Update the dirty db with observation data.

    Parameters
    ----------
    filename : str
        Filename of the database.
    basedir : str
        Base directory to search for observations in.
    first_run : bool
        Determines if old_results are checked as well.
    """

    # pre multiple TBs
    files = glob.glob(
        os.path.join(basedir, "results/????-??-??-??:??:??/TB/J*[+-]*_t.tot")
    )

    # multi TBs
    files += glob.glob(
        os.path.join(basedir, "results/????-??-??-??:??:??/J*[+-]*/J*[+-]*_t.tot")
    )
    files = sorted(files)

    if first_run:
        old_files = glob.glob(
            os.path.join(basedir, "old_results/????-??-??-??:??:??/TB/J*[+-]*_t.tot")
        )
        old_files += glob.glob(
            os.path.join(
                basedir, "old_results/????-??-??-??:??:??/J*[+-]*/J*[+-]*_t.tot"
            )
        )
        old_files = sorted(old_files)

        all_files = old_files + files
    else:
        all_files = files

    all_files = np.array(all_files)

    # remove the already processed ones
    # reprocess the unfinished ones
    processed = get_observations(filename)
    processed = processed[processed["finished"] == 1]
    all_files = all_files[~np.in1d(all_files, processed["filename"])]

    logger.info("Observations to process: {0}".format(len(all_files)))

    dtype_failed = [("filename", "|S2048"), ("error", "|S1024")]

    for item in all_files:
        logger.info("Processing: {0}".format(item))

        try:
            info = get_obs_info(item)

            # all observations are finished except the last one
            if item == all_files[-1]:
                info["finished"] = 0
            else:
                info["finished"] = 1
        except Exception as e:
            failed = np.zeros(1, dtype=dtype_failed)

            failed["filename"] = item
            failed["error"] = str(e)

            insert_into_db(failed, "Failed", filename)
        else:
            insert_into_db(info, "Observations", filename)


#
# MAIN
#


def main():
    # handle command line arguments
    parser = argparse.ArgumentParser(description="Molonglo SNR extractor - server.")
    parser.add_argument(
        "--first_run",
        dest="first_run",
        action="store_true",
        default=False,
        help="Process the archives in old_results as well (default: False).",
    )
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    print("Molonglo SNR extractor - server")
    print("===============================")

    # definitions
    config = get_config()
    server_config = (
        config["snr_extractor"]["server"]["host"],
        config["snr_extractor"]["server"]["port"],
    )
    dirty_db = os.path.expanduser(config["database"]["dirty_file"])
    basedir = config["snr_extractor"]["base_dir"]

    # set gaussian baseline estimator
    use_gaussian_baseline_estimator(restore=False)

    # check if dirty db exists
    if not os.path.isfile(dirty_db):
        init_db(dirty_db)

    lock = threading.Lock()
    filename = get_current_observation(basedir)
    obs = Observation(basedir, filename)

    # snr thread
    snr_thread = threading.Thread(
        name="snrthread",
        target=snr_worker,
        args=(
            lock,
            obs,
        ),
    )
    snr_thread.daemon = True
    snr_thread.start()

    # db thread
    db_thread = threading.Thread(
        name="dbthread",
        target=db_worker,
        args=(
            dirty_db,
            basedir,
            args.first_run,
        ),
    )
    db_thread.daemon = True
    db_thread.start()

    # start the tcp server
    server = ThreadedTCPServer(server_config, ThreadedTCPRequestHandler, lock, obs)
    server_thread = threading.Thread(name="tcpserver", target=server.serve_forever)
    # exit the server thread when the main thread terminates
    server_thread.deamon = True
    server_thread.start()
    print("TCP server running in thread: {0}".format(server_thread.name))

    try:
        while True:
            print(
                "Main thread running: {0}".format(
                    datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                )
            )
            time.sleep(60)
    except KeyboardInterrupt:
        logger.warn("Caught SIGINT, stopping the program.")

    snr_thread.join(1)
    server.shutdown()
    server.server_close()


# if run directly
if __name__ == "__main__":
    main()
