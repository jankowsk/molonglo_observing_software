#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2016-2018 Fabian Jankowski
#   Database helpers for Molonglo.
#

from __future__ import print_function
import os.path
import sqlite3

import numpy as np

from molsoft.config_helpers import get_config
from molsoft.schedule_helpers import parse_schedule

CONFIG = get_config()
DIRTY_DB = os.path.expanduser(CONFIG["database"]["dirty_file"])
STRATEGY = os.path.expanduser(CONFIG["database"]["strategy_file"])


def init_db(filename):
    """
    Initialize observations database.

    Parameters
    ----------
    filename : str
        Filename of database.
    """

    con = sqlite3.connect(filename, timeout=15)

    with con:
        cur = con.cursor()

        sql_observations = """
        CREATE TABLE Observations (
        obsid INTEGER PRIMARY KEY,
        utc TEXT,
        filename TEXT,
        source_name TEXT,
        nbin INTEGER,
        nchan INTEGER,
        npol INTEGER,
        nsubint INTEGER,
        freq REAL,
        bw REAL,
        tobs REAL,
        snr REAL,
        finished BOOLEAN);
        """

        cur.execute(sql_observations)
        cur.execute("CREATE UNIQUE INDEX ObsIndex ON Observations(filename);")

        # failed files
        cur.execute("DROP TABLE IF EXISTS Failed;")

        sql_failed = """
        CREATE TABLE Failed (
        failedid INTEGER PRIMARY KEY,
        filename TEXT,
        error TEXT);
        """

        cur.execute(sql_failed)


def insert_into_db(data, table, filename):
    """
    Insert data into database.

    Parameters
    ----------
    data : numpy rec
        Data to add into database.
    table : str
        Name of table into which the data should be inserted.
    filename : str
        Filename of the database.
    """

    con = sqlite3.connect(filename, timeout=15)

    with con:
        cur = con.cursor()

        if table == "Observations":
            sql = """
                INSERT OR REPLACE INTO {0} (\
                utc, \
                filename, \
                source_name, \
                nbin, \
                nchan, \
                npol, \
                nsubint, \
                freq, \
                bw, \
                tobs, \
                snr, \
                finished)
                VALUES (?,?,?,?,?,?,?,?,?,?, \
                        ?,?)
            """.format(
                table
            )

            t = zip(
                data["utc"],
                data["filename"],
                data["source_name"],
                data["nbin"],
                data["nchan"],
                data["npol"],
                data["nsubint"],
                data["freq"],
                data["bw"],
                data["tobs"],
                data["snr"],
                data["finished"],
            )

            cur.executemany(sql, t)

        elif table == "Failed":
            sql = """
                INSERT INTO {0} (\
                filename, \
                error)
                VALUES (?,?)
            """.format(
                table
            )

            t = zip(data["filename"], data["error"])

            cur.executemany(sql, t)

        else:
            raise RuntimeError("Table name not known: {0}".format(table))

        con.commit()


def get_observations(filename=DIRTY_DB):
    """
    Load all the observations from the database.

    Parameters
    ----------
    filename : str
        Filename of database.

    Returns
    -------
    data : numpy rec
        Data as a numpy record.
    """

    con = sqlite3.connect(filename)

    with con:
        cur = con.cursor()

        sql = """
        SELECT utc,filename,source_name,finished
        FROM Observations
        ORDER BY utc
        """

        cur.execute(sql)
        rows = cur.fetchall()

    # convert to numpy array
    dtype = [
        ("utc", "|S64"),
        ("filename", "|S2048"),
        ("source_name", "|S32"),
        ("finished", "int"),
    ]
    data = np.array(rows, dtype=dtype)

    return data


def get_fluxes(filename=STRATEGY):
    """
    Get the fluxes from the database.

    Parameters
    ----------
    filename : str
        Filename of database.

    Returns
    -------
    data : numpy rec
        Flux densities of sources from database.
    """

    con = sqlite3.connect(filename)

    with con:
        cur = con.cursor()

        sql = """
        SELECT psrj,rank,flux,delta,p0,tsky
        FROM Fluxes
        ORDER BY psrj
        """

        cur.execute(sql)
        rows = cur.fetchall()

    # convert to numpy array
    dtype = [
        ("psrj", "|S32"),
        ("rank", "int"),
        ("flux", "float"),
        ("delta", "float"),
        ("p0", "float"),
        ("tsky", "float"),
    ]
    data = np.array(rows, dtype=dtype)

    return data


def get_obs_strategy(filename=STRATEGY):
    """
    Parse the observing strategy.

    Parameters
    ----------
    filename : str
        Filename of database.

    Returns
    -------
    data : numpy rec
        Observation strategy from database.
    """

    con = sqlite3.connect(filename)

    with con:
        cur = con.cursor()

        sql = """
        SELECT psrj,obs_mode,tmin,tmax,snr_thr,toa_thr,programme,cadence,priority,science
        FROM Strategy
        WHERE observe == "TRUE"
        ORDER BY psrj
        """

        cur.execute(sql)
        rows = cur.fetchall()

    # convert to numpy array
    dtype = [
        ("psrj", "|S32"),
        ("obs_mode", "|S16"),
        ("tmin", "float"),
        ("tmax", "float"),
        ("snr", "float"),
        ("toa", "float"),
        ("programme", "|S32"),
        ("cadence", "float"),
        ("priority", "float"),
        ("science", "|S2048"),
    ]
    data = np.array(rows, dtype=dtype)

    return data


def obs_strat_to_schedule(t_data):
    """
    Convert the output from the observation strategy into a schedule file
    for automatic_mode.

    Parameters
    ----------
    t_data
        Output from observation strategy.

    Returns
    -------
    schedule : list
        List of SchedItems.
    """

    data = np.copy(t_data)

    schedule = []

    for item in data:
        if item["obs_mode"].lower() == "fold":
            line = [item["obs_mode"], item["psrj"], str(item["tmax"])]
        elif item["obs_mode"].lower() == "foldsnr":
            line = [
                item["obs_mode"],
                item["psrj"],
                str(item["snr"]),
                str(item["tmin"]),
                str(item["tmax"]),
            ]
        elif item["obs_mode"].lower() in ["foldtoa", "foldint"]:
            line = [
                item["obs_mode"],
                item["psrj"],
                str(item["tmin"]),
                str(item["tmax"]),
            ]
        else:
            raise RuntimeError("Obs_mode not implemented: {0}".format(item["obs_mode"]))

        schedule.append(line)

    # convert to list of SchedItem
    schedule = parse_schedule(schedule)

    return schedule


def dtype_add_fields(dtype1, dtype2):
    """
    Add fields in dtype2 to dtype1.

    Parameters
    ----------
    dtype1 : dtype
        First dtype object.
    dtype2 : dtype
        Second dtype object.

    Returns
    -------
    r : dtype
        Combined dtype object.
    """

    dtype1 = np.dtype(dtype1)
    dtype2 = np.dtype(dtype2)

    r = set(list(dtype1.descr) + list(dtype2.descr))
    r = np.dtype(list(r))

    return r
