#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2014-2018 Fabian Jankowski
#   Automatic observing mode for Molonglo - client.
#

from __future__ import print_function
import argparse
import logging
import signal
import socket
import sys
import time
import os.path

from lxml import etree
import numpy as np

from molsoft.am_helpers import (
    exit_cleanly,
    check_other_interfaces,
    block_other_interfaces,
)
from molsoft.config_helpers import get_config
from molsoft.schedule_helpers import (
    load_schedule,
    test_schedule_file,
    get_schedule_grammar,
)
from molsoft.version import __version__


#
# set up logging
#

# root logger
logger = logging.getLogger("automatic_mode")
logger.setLevel(logging.DEBUG)
logger.propagate = False

# log to console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console_formatter = logging.Formatter("%(message)s")
console.setFormatter(console_formatter)
logger.addHandler(console)


class SignalHandler(object):
    """
    Handle unix signals sent to the program.
    """

    def __init__(self):
        pass

    def handle(self, signum, frame):
        # treat SIGINT/INT/CRTL-C
        if signum == signal.SIGINT:
            print()
            logger.warn("SIGINT received, stopping the automatic observing mode.")


def xml_parse(response_DS):
    """
    Parse the XML message.
    """

    xml = None
    xml = etree.fromstring(response_DS)
    dtype = [
        ("mode", "|S32"),
        ("name", "|S32"),
        ("snr", "float"),
        ("tmin", "float"),
        ("tmax", "float"),
    ]

    status = np.zeros(1, dtype=dtype)
    final = np.zeros(1, dtype=dtype)
    status["name"] = None

    for child in xml.iter("sno"):
        status["mode"] = child.find("mode").text
        status["name"] = child.find("name").text
        status["snr"] = float(child.find("snr").text)
        status["tmin"] = float(child.find("tmin").text)
        status["tmax"] = float(child.find("tmax").text)
        final = np.vstack((final, status))

    return final


#
# MAIN
#


def main():
    grammar_help = get_schedule_grammar()

    # handle command line arguments
    parser = argparse.ArgumentParser(
        description="Molonglo automatic observing mode - client.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=grammar_help,
    )

    manual = parser.add_argument_group("manual")
    manual.add_argument(
        "-m",
        "--manual",
        dest="schedule_file",
        default=None,
        nargs=1,
        type=str,
        help="Parse the schedule file and enable manual scheduling.",
    )
    manual.add_argument(
        "-t",
        "--test",
        dest="test_schedule",
        action="store_true",
        default=False,
        help="Dry run. Test schedule file for errors.",
    )

    dynamic = parser.add_argument_group("dynamic")
    dynamic.add_argument(
        "-d",
        "--dynamic",
        dest="dynamic",
        action="store_true",
        default=False,
        help="Enable dynamic scheduling.",
    )
    parser.add_argument("--version", action="version", version=__version__)

    args = parser.parse_args()

    # check if another interface is running the observing:
    if not args.test_schedule:
        other_interface_running, other_interface_name = check_other_interfaces()

        if other_interface_running:
            logger.error(
                '"'
                + other_interface_name
                + '"'
                + " is already running observations, aborting."
            )
            sys.exit(0)
        else:
            logger.info("Creating obs.running.")
            block_other_interfaces()

    print("Molonglo automatic observing mode - client")
    print("==========================================")

    # start signal handler
    sh = SignalHandler()
    signal.signal(signal.SIGINT, sh.handle)

    # sanity check
    if args.dynamic and args.schedule_file is not None:
        logger.error("Only one mode can be requested at a time.")
        sys.exit(1)

    # server configs
    config = get_config()
    am_config = (
        config["automatic_mode"]["server"]["host"],
        config["automatic_mode"]["server"]["port"],
    )
    ds_config = (
        config["scheduler"]["server"]["host"],
        config["scheduler"]["server"]["port"],
    )

    # manual mode
    if args.schedule_file is not None:
        logger.info("Manual scheduling activated.")

        if not os.path.isfile(args.schedule_file[0]):
            logger.error(
                "The schedule file does not exist: {0}".format(args.schedule_file[0])
            )
            sys.exit(1)

        # use absolute path here
        schedule_file = os.path.abspath(args.schedule_file[0])

        # stop is only testing
        if args.test_schedule:
            test_schedule_file(schedule_file)
            sys.exit(0)
        else:
            load_schedule(schedule_file)

        # create a TCP socket
        AM = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        AM.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            AM.connect(am_config)
            AM.sendall("ManualSchedule")
            time.sleep(2)

            AM.sendall(schedule_file)

            # receive response
            response_AM = AM.recv(2048)
            logger.info("Server replied: {0}".format(response_AM))
        finally:
            AM.close()

    # dynamic mode
    elif args.dynamic:
        logger.info("Dynamic scheduling activated.")

        DS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        DS.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            # Connect to DS server
            DS.connect(ds_config)
            DS.sendall("DynamicSchedule")

        finally:
            DS.close()

    logger.info("All done.")


# if run directly
if __name__ == "__main__":
    main()
