#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2016-2018 Fabian Jankowski
#   Schedule helpers for Molonglo.
#

from __future__ import print_function
import copy
import csv
from datetime import timedelta
import logging
import os.path
import re

import ephem

from molsoft.coord_helpers import get_coordinate_list, get_ra_dec_list


# set up logging
logger = logging.getLogger("automatic_mode." + __name__)
logger.setLevel(logging.INFO)


class SchedItem(object):
    """
    A schedule item that encapsulates all the necessary data to process an
    observation.
    """

    def __init__(self):
        self.obs_mode = None
        self.obs_type = None
        self.name = None
        self.ra = None
        self.dec = None
        self.tmin = None
        self.tmax = None
        self.snr = None
        self.toa = None
        self.tracking = None

        # wait
        self.wait_mode = None
        self.wait_sec = None
        self.wait_lst = None
        self.wait_offset = None

        # scheduling info
        self.score = None
        self.priority = None
        self.n_5deg = None
        self.n_10deg = None
        self.n_20deg = None
        self.last_observed = None
        self.can_be_observed = None
        self.was_observed = None

    def __str__(self):
        if not self.name:
            if not self.obs_mode:
                return "Unknown"
            else:
                return self.obs_mode

        return self.name

    def make_schedule(self):
        """
        Output the current object as schedule line.

        Returns
        -------
        line : str
            Schedule line that fully represents the SchedItem.
        """

        if self.obs_mode not in ["TB", "TBSNR", "TBINT", "TBTOA", "MODTB", "POSN"]:
            raise NotImplementedError(
                "Schedule output for this mode not implemented: {0}".format(
                    self.obs_mode
                )
            )

        if self.obs_mode == "TB":
            if self.tracking:
                line = "; ".join(["FOLD", self.name, str(self.tmax)])
            else:
                line = "; ".join(["TB", self.obs_type, self.name, str(self.tmax)])

        elif self.obs_mode == "TBSNR":
            if self.tracking:
                line = "; ".join(
                    [
                        "FOLDSNR",
                        self.name,
                        str(self.snr),
                        str(self.tmin),
                        str(self.tmax),
                    ]
                )
            else:
                line = "; ".join(
                    [
                        "TBSNR",
                        self.obs_type,
                        self.name,
                        str(self.snr),
                        str(self.tmin),
                        str(self.tmax),
                    ]
                )

        elif self.obs_mode == "TBINT":
            if self.tracking:
                line = "; ".join(["FOLDINT", self.name, str(self.tmin), str(self.tmax)])
            else:
                line = "; ".join(
                    ["TBINT", self.obs_type, self.name, str(self.tmin), str(self.tmax)]
                )

        elif self.obs_mode == "TBTOA":
            if self.tracking:
                line = "; ".join(["FOLDTOA", self.name, str(self.tmin), str(self.tmax)])
            else:
                line = "; ".join(
                    ["TBTOA", self.obs_type, self.name, str(self.tmin), str(self.tmax)]
                )

        elif self.obs_mode == "MODTB":
            line = "; ".join([self.obs_mode, self.obs_type, self.name, str(self.tmax)])

        elif self.obs_mode == "POSN":
            line = "; ".join(["POSN", self.ra, self.dec])

        return line


def get_schedule_grammar():
    """
    Return the grammar in the schedule file.

    Returns
    -------
    grammar : str
        Grammar in the schedule file.
    """

    grammar = """
Grammar in the schedule file
  One command per line, semicolon delimited, all RA, DEC in J2000 equatorial
  coordinates in hh:mm:ss notation. Blank lines and lines starting with a hash
  (#) are ignored. The commands are case insensitive.

Definitions
  $type = {{tracking,                       Telescope tracks an object both
                                            physically and electronically.
  transiting,                               Backend tracks object transiting
                                            through primary beam
                                            electronically.
  stationary}}                              Synthesised beam is stationary.

  Command                                   Meaning
1) Telescope movement:
  track; $ra; $dec                          Track given ra and dec.
  posn; $ra; $dec                           Position at given ra, dec, but
                                            do not track.
  park                                      Move telescope to park position.

2) Pulsar observing modes:
  indiv; $type; $jname; $tmax               Start backend in folded
                                            individual module mode.
  tb; $type; $jname; $tmax                  Start backend in tied-array
                                            beam mode.
  tbsnr; $type; $jname; $snr; $tmin; $tmax  Same as foldsnr, but without
                                            physical tracking the source.
  tbtoa; $type; $jname; $tmin; $tmax        Same as foldtoa, but without
                                            physical tracking the source.
  modtb; $type; $name; $tmax                Observe in filterbank individual
                                            module plus tied-array beam
                                            mode.
  fold; $jname; $tmax                       Observe pulsar (combines track
                                            and tb).
  foldsnr; $jname; $snr; $tmin; $tmax       Observe pulsar for at least
                                            $tmin and maximum $tmax
                                            seconds until the S/N ratio
                                            $snr is reached (combines track
                                            and tb).
  foldtoa; $jname; $tmin; $tmax             Time pulsar until saturation
                                            and for at least $tmin and
                                            maximum $tmax seconds.
  foldint; $jname; $tmin; $tmax             Observe intermittent pulsar for
                                            $tmin seconds. If it is on,
                                            observe it further until $tmax
                                            is reached.

3) FRB search modes:
  fb; $type; $name; $ra; $dec; $tmax        Do a fan beam FRB search at the
                                            given ra and dec.
4) Mapping observing modes:
  corr; $type; $name; $ra, $dec, $tmax      Do a correlation observation of
                                            an object at ra and dec.
  map; $name; $ra; $dec; $tmax              Map an object at ra, dec
                                            (combines track and corr).

5) Wait modes:
  wait; sec; $seconds                       Wait for a number of seconds.
  wait; lst; $lst                           Wait until a given LST in
                                            hh:mm:ss notation (seconds are
                                            optional). If the LST is over
                                            for the current 24 hour LST
                                            range it will automatically
                                            wrap to the next one.
  wait; transit; $jname; $offset            Wait until transit of a given
                                            pulsar. The $offset from the
                                            transit is given in seconds and
                                            can be both negative (earlier)
                                            and positive (later).

Example usage
  Create a schedule file "test.sch" in your current working directory. You can
  use all the commands above. Also have a look at the example schedules below.
  Then test the schedule file using:
      automatic_mode.py -t test.sch
  this will not do anything, it will just parse the schedule file and check it
  for errors. If there are no errors displayed you can run the schedule file
  like this:
      automatic_mode.py test.sch
  this will execute the commands in the schedule file on the telescope and
  start the observation.

Example schedules
  1) Observe the Vela pulsar for 10 minutes, then park the telescope:
  fold; J0835-4510; 600
  park

  2) Wait for 30 minutes, then do a drift scan on J1644 and park the telescope:
  wait; sec; 1800
  posn; 16:14:49.281; -45:59:09.5
  fold; J1644-4559; 1800
  park

  3) Wait until 16:00 LST then observe J1644 for one hour:
  wait; lst; 16:00
  fold; J1644-4559; 3600

  4) Wait until 30 minutes before Vela transit, then observe it for one hour:
  wait; transit; J0835-4510; -1800
  fold; J0835-4510; 3600

  5) Observe the CJ1218-4600 quasar for 20 minutes around transit:
  wait; lst; 12:08:06
  corr; transiting; CJ1218-4600; 12:18:06.0; -46:00:29.2; 1200

Signal handling
  The program handles the following Unix signals sent to it:

  * INT/SIG-INT/CTRL-C:
  If an INT signal is received (e.g. CTRL-C) the program will stop the
  backend, stop the movement of the telescope and exit gracefully. It will not
  park the telescope.

Safety system
  The program has an internal safety system to protect the telescope from
  damage. Before every slewing operation it calculates how long the operation
  should take if everything works correctly. If the physical slewing operation
  takes longer than that (plus a small margin) it will stop the motors to
  protect the telescope, issue a critical error and exit gracefully.
"""

    return grammar


def parse_schedule(total):
    """
    Parse the schedule and check it for errors.

    Parameters
    ---------
    total : list
        List of str are schedule lines.

    Returns
    -------
    schedule : list
        Schedule that is a list of SchedItems.

    Raises
    -----
    RuntimeError
        If the schedule line could not be parsed.
    """

    # ra, dec list from psrcat
    coord_list = get_coordinate_list()

    # regex pattern for equatorial coordinates in hh:mm:ss notation
    # for example:
    # 08:37:21.1818     -41:35:14.37
    re_eq_coord = re.compile(
        r"^[+-]{0,1}[0-9]{1,2}(\:[0-9]{1,2}){0,1}(\:[0-9]{1,2}(\.[0-9]{0,}){0,1}){0,1}$"
    )

    # allowed obs_types
    types = ["TRACKING", "TRANSITING", "STATIONARY"]

    # regex pattern for jname
    re_jname = re.compile(r"^J[0-9]{4}[+-][0-9]{2,4}([A-Z]){0,1}$")

    # regex pattern for tmin and tmax
    re_tobs = re.compile(r"^[0-9]{1,}(\.[0-9]{0,}){0,1}$")

    # regex pattern for offset
    re_offset = re.compile(r"^[+-]{0,1}[0-9]{1,}(\.[0-9]{1,}){0,1}$")

    # regex pattern for proc_file
    # re_proc_file = re.compile(r"^[a-z]{1,}(\.[a-z]{1,}){1,}$")

    # regex pattern for lst
    # lst in hh:mm:ss (:ss is optional) format
    re_lst = re.compile(r"^[0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2}){0,1}$")

    # remove empty lines and comments
    clean_total = []
    for line in total:
        if len(line) > 0 and not line[0].startswith("#"):
            clean_total.append(line)

    # format commands to uppercase
    for i in xrange(len(clean_total)):
        clean_total[i][0] = clean_total[i][0].upper()

        # obs_type and wait
        if clean_total[i][0] in [
            "INDIV",
            "TB",
            "MODTB",
            "TBSNR",
            "TBTOA",
            "CORR",
            "FB",
            "WAIT",
            "POSN",
        ]:
            clean_total[i][1] = clean_total[i][1].upper()

    schedule = []
    line_number = 1

    for line in clean_total:
        # initialise
        next_command = None
        ra = None
        dec = None

        # TRACK and POSN
        if line[0] in ["TRACK", "POSN"]:
            if (
                len(line) == 3
                and re_eq_coord.match(line[1])
                and re_eq_coord.match(line[2])
            ):
                s = SchedItem()
                s.name = line[0]
                s.obs_mode = line[0]
                s.ra = line[1]
                s.dec = line[2]
                next_command = s

        # PARK
        elif line[0] == "PARK":
            if len(line) == 1:
                s = SchedItem()
                s.obs_mode = line[0]
                next_command = s

        # WAIT
        elif line[0] == "WAIT":
            # SEC
            # WAIT; SEC; $sec
            if len(line) == 3 and line[1] == "SEC" and re_tobs.match(line[2]):
                line[2] = float(line[2])
                s = SchedItem()
                s.name = line[0]
                s.obs_mode = line[0]
                s.wait_mode = line[1]
                s.wait_sec = line[2]
                next_command = s

            # LST
            # WAIT; LST; $lst
            elif len(line) == 3 and line[1] == "LST" and re_lst.match(line[2]):
                s = SchedItem()
                s.name = line[0]
                s.obs_mode = line[0]
                s.wait_mode = line[1]
                s.wait_lst = line[2]
                next_command = s

            # TRANSIT
            # WAIT; TRANSIT; $jname; $offset
            elif (
                len(line) == 4
                and line[1] == "TRANSIT"
                and re_jname.match(line[2])
                and re_offset.match(line[3])
            ):
                line[3] = float(line[3])
                s = SchedItem()
                s.obs_mode = line[0]
                s.wait_mode = line[1]
                ra, dec = get_ra_dec_list(coord_list, line[2])
                s.wait_lst = ra
                s.wait_offset = line[3]
                next_command = s

        # INDIV, TB and MODTB
        # INDIV; $type; $jname; $tmax
        # TB; $type; $jname; $tmax
        # MODTB; $type; $name; $tmax
        elif line[0] in ["INDIV", "TB", "MODTB"]:
            if (
                len(line) == 4
                and line[1] in types
                and re_jname.match(line[2])
                and re_tobs.match(line[3])
            ):
                line[3] = float(line[3])
                s = SchedItem()
                s.obs_mode = line[0]
                s.obs_type = line[1]
                s.name = line[2]
                ra, dec = get_ra_dec_list(coord_list, line[2])
                s.ra = ra
                s.dec = dec
                s.tmax = line[3]
                next_command = s

        # TBSNR; $type; $jname; $snr; $tmin; $tmax
        elif line[0] == "TBSNR":
            if (
                len(line) == 6
                and line[1] in types
                and re_jname.match(line[2])
                and re_tobs.match(line[3])
                and re_tobs.match(line[4])
                and re_tobs.match(line[5])
                and float(line[4]) <= float(line[5])
            ):
                line[3] = float(line[3])
                line[4] = float(line[4])
                line[5] = float(line[5])
                s = SchedItem()
                s.obs_mode = line[0]
                s.obs_type = line[1]
                s.name = line[2]
                ra, dec = get_ra_dec_list(coord_list, line[2])
                s.ra = ra
                s.dec = dec
                s.snr = line[3]
                s.tmin = line[4]
                s.tmax = line[5]
                next_command = s

        # TBTOA; $type; $jname; $tmin; $tmax
        elif line[0] == "TBTOA":
            if (
                len(line) == 5
                and line[1] in types
                and re_jname.match(line[2])
                and re_tobs.match(line[3])
                and re_tobs.match(line[4])
                and float(line[3]) <= float(line[4])
            ):
                line[3] = float(line[3])
                line[4] = float(line[4])
                s = SchedItem()
                s.obs_mode = line[0]
                s.obs_type = line[1]
                s.name = line[2]
                ra, dec = get_ra_dec_list(coord_list, line[2])
                s.ra = ra
                s.dec = dec
                s.toa = 7.0
                s.tmin = line[3]
                s.tmax = line[4]
                next_command = s

        # FOLD
        # FOLD; $jname; $tmax
        # gets replaced with:
        # 1) TRACK
        # 2) TB
        elif line[0] == "FOLD":
            if len(line) == 3 and re_jname.match(line[1]) and re_tobs.match(line[2]):
                line[2] = float(line[2])
                s = SchedItem()
                s.obs_mode = "TB"
                s.obs_type = "TRACKING"
                s.name = line[1]
                ra, dec = get_ra_dec_list(coord_list, line[1])
                s.ra = ra
                s.dec = dec
                s.tmax = line[2]
                s.tracking = True
                next_command = s

        # FOLDSNR; $jname; $snr; $t_min; $t_max
        # gets replaced with:
        # 1) TRACK
        # 2) TBSNR
        elif line[0] == "FOLDSNR":
            if (
                len(line) == 5
                and re_jname.match(line[1])
                and re_tobs.match(line[2])
                and re_tobs.match(line[3])
                and re_tobs.match(line[4])
                and float(line[3]) <= float(line[4])
            ):
                line[2] = float(line[2])
                line[3] = float(line[3])
                line[4] = float(line[4])
                s = SchedItem()
                s.obs_mode = "TBSNR"
                s.obs_type = "TRACKING"
                s.name = line[1]
                ra, dec = get_ra_dec_list(coord_list, line[1])
                s.ra = ra
                s.dec = dec
                s.tracking = True
                s.snr = line[2]
                s.tmin = line[3]
                s.tmax = line[4]
                next_command = s

        # FOLDTOA; $jname; $t_min; $t_max
        # gets replaced with:
        # 1) TRACK
        # 2) TBTOA
        elif line[0] == "FOLDTOA":
            if (
                len(line) == 4
                and re_jname.match(line[1])
                and re_tobs.match(line[2])
                and re_tobs.match(line[3])
                and float(line[2]) <= float(line[3])
            ):
                line[2] = float(line[2])
                line[3] = float(line[3])
                s = SchedItem()
                s.obs_mode = "TBTOA"
                s.obs_type = "TRACKING"
                s.name = line[1]
                ra, dec = get_ra_dec_list(coord_list, line[1])
                s.ra = ra
                s.dec = dec
                s.tracking = True
                s.toa = 7.0
                s.tmin = line[2]
                s.tmax = line[3]
                next_command = s

        # FOLDINT; $jname; $tmin; $tmax
        # gets replaced with:
        # 1) TRACK
        # 2) TBINT
        elif line[0] == "FOLDINT":
            if (
                len(line) == 4
                and re_jname.match(line[1])
                and re_tobs.match(line[2])
                and re_tobs.match(line[3])
                and float(line[2]) <= float(line[3])
            ):
                line[2] = float(line[2])
                line[3] = float(line[3])
                s = SchedItem()
                s.obs_mode = "TBINT"
                s.obs_type = "TRACKING"
                s.name = line[1]
                ra, dec = get_ra_dec_list(coord_list, line[1])
                s.ra = ra
                s.dec = dec
                s.tracking = True
                s.snr = 9.0
                s.tmin = line[2]
                s.tmax = line[3]
                next_command = s

        # CORR and FB
        # CORR; $type; $name; $ra, $dec, $tmax
        # FB; $type; $name; $ra, $dec, $tmax
        elif line[0] in ["CORR", "FB"]:
            if (
                len(line) == 6
                and line[1] in types
                and re_eq_coord.match(line[3])
                and re_eq_coord.match(line[4])
                and re_tobs.match(line[5])
            ):
                extra_wait_seconds = 30.0
                line[5] = float(line[5])
                while line[5] > 3600.0 + extra_wait_seconds:
                    s = SchedItem()
                    s.obs_mode = line[0]
                    s.obs_type = line[1]
                    if s.obs_type == "TRACKING":
                        s.tracking = True
                    s.name = line[2]
                    s.ra = line[3]
                    s.dec = line[4]
                    s.tmax = 3600.0
                    line[5] = line[5] - 3600.0 - extra_wait_seconds
                    next_command = s
                    schedule.append(next_command)
                    # add a wait before next transit

                    s = SchedItem()
                    s.name = "WAIT"
                    s.obs_mode = "WAIT"
                    s.wait_mode = "SEC"
                    s.wait_sec = extra_wait_seconds
                    next_command = s
                    schedule.append(next_command)

                s = SchedItem()
                s.obs_mode = line[0]
                s.obs_type = line[1]
                s.name = line[2]
                s.ra = line[3]
                s.dec = line[4]
                if s.obs_type == "TRACKING":
                    s.tracking = True
                s.tmax = line[5]
                next_command = s

        # MAP
        # MAP; $name; $ra, $dec, $tmax
        # gets replaced with:
        # 1) TRACK
        # 2) CORR
        elif line[0] == "MAP":
            if (
                len(line) == 5
                and re_eq_coord.match(line[2])
                and re_eq_coord.match(line[3])
                and re_tobs.match(line[4])
            ):
                line[4] = float(line[4])
                s = SchedItem()
                s.obs_mode = "CORR"
                s.obs_type = "TRACKING"
                s.name = line[1]
                s.ra = line[2]
                s.dec = line[3]
                s.tracking = True
                s.tmax = line[4]
                next_command = s

        # check that line got properly parsed
        if next_command is not None:
            schedule.append(next_command)
        else:
            print(line)
            raise RuntimeError(
                "There is an error on line number {0}.".format(line_number)
            )

        line_number += 1

    # check for empty schedule files
    if not len(schedule) > 0:
        logger.error("The schedule file is empty.")
    else:
        logger.info("Schedule file successfully parsed. No errors.")

    return schedule


def load_schedule(filename):
    """
    Load schedule from file.

    Parameters
    ----------
    filename : str
        Filename of the schedule file to load.

    Returns
    -------
    schedule : list
        List of SchedItems.

    Raises
    ------
    RuntimeError
        If schedule file does not exist or cannot be parsed.
    """

    total = []

    # check if file exists
    if os.path.isfile(filename):
        with open(filename, "r") as f:
            reader = csv.reader(f, delimiter=";")
            for line in reader:
                # strip whitespace
                stripped_line = [x.strip() for x in line]
                # strip tabs
                stripped_line = [x.strip("\t") for x in stripped_line]
                total.append(stripped_line)

    else:
        raise RuntimeError("The schedule file does not exist: {0}".format(filename))

    try:
        schedule = parse_schedule(total)
    except RuntimeError as e:
        raise RuntimeError(
            "Could not parse schedule file: {0}. {1}".format(
                os.path.basename(filename), str(e)
            )
        )

    return schedule


def split_max_tobs(schedule, max_tobs):
    """
    Split very long observation into multiple chunks.

    Parameters
    ----------
    schedule : list
        List of SchedItems.
    max_tobs : float
        Maximum observing time given in seconds.

    Returns
    -------
    schedule : list
        List of SchedItem that is split into multiple chunks that are less than
        max_tobs.

    Raises
    ------
    RuntimeError
        If the split failed.
    """

    new_schedule = []

    # affected modes
    split_modes = ["TB", "INDIV", "FB"]

    for item in schedule:
        if item.obs_mode in split_modes and item.tmax > max_tobs:
            tmax = item.tmax

            while tmax > max_tobs:
                # make a copy of the object
                new_item = copy.deepcopy(item)
                new_item.tmax = max_tobs
                new_schedule.append(new_item)

                tmax = tmax - max_tobs

            # add the remaining tobs
            new_item = copy.deepcopy(item)
            new_item.tmax = tmax
            new_schedule.append(new_item)
        else:
            new_schedule.append(item)

    # sanity check that all was done correctly
    for item in new_schedule:
        if item.obs_mode in split_modes and item.tmax <= 0:
            raise RuntimeError("tmax is smaller than zero: {0}".format(item.tmax))

    return new_schedule


def calculate_times(schedule):
    """
    Calculate times and statistics for the current schedule.

    Parameters
    ----------
    schedule : list
        List of SchedItems.
    """

    total = 0

    for line in schedule:
        if line.obs_mode in ["INDIV", "TB", "TBSNR", "TBTOA" "MODTB", "CORR", "FB"]:
            total += line.tmax

    print()
    print("Total observation time: {0} hh:mm:ss.".format(timedelta(seconds=total)))
    print(
        "This schedule will run until: {0} AEST ({1} UTC)".format(
            ephem.Date(ephem.now() + total * ephem.second + 10 * ephem.hour),
            ephem.Date(ephem.now() + total * ephem.second),
        )
    )


def test_schedule_file(filename):
    """
    Test the schedule file for errors.

    Parameters
    ----------
    filename : str
        Filename of schedule file to process.

    Raises
    ------
    RuntimeError
        If errors occurred.
    """

    print()
    print("Dry run. Testing the schedule file.")
    print()
    schedule = load_schedule(filename)

    # limit the maximum observation time per single integration
    schedule = split_max_tobs(schedule, 3600)

    print()
    print("Schedule")
    print("========")
    for line in schedule:
        print(
            line.obs_mode,
            line.name,
            line.ra,
            line.dec,
            line.tmax,
            line.snr,
            line.tmin,
            line.tmax,
        )

    calculate_times(schedule)
