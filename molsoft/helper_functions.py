from __future__ import print_function
from string import Template


#########################
# For control of the MPSR backend
########################

# add modtb_params to MPSR_MESSAGE
MPSR_MESSAGE = Template(
    """<?xml version='1.0' encoding='ISO-8859-1'?>
<mpsr_tmc_message>
    <command>$command</command>$parameters$tb_params$modtb_params$corr_params
</mpsr_tmc_message>"""
)

MPSR_PARAMS = Template(
    """<west_arm_parameters>
      <tracking>$west_tracking</tracking>
      <ns_tilt units='$west_ns_tilt_units'>$west_ns_tilt</ns_tilt>
      <md_angle units='$west_md_angle_units'>$west_md_angle</md_angle>
   </west_arm_parameters>
   <east_arm_parameters>
      <tracking>$east_tracking</tracking>
      <ns_tilt units='$east_ns_tilt_units'>$east_ns_tilt</ns_tilt>
      <md_angle units='$east_md_angle_units'>$east_md_angle</md_angle>
   </east_arm_parameters>
   <signal_parameters>
        <nchan>$nchan</nchan>
        <nbit>$nbits</nbit>
        <ndim>$ndim</ndim>
        <npol>$npol</npol>
        <nant>$nant</nant>
        <bandwidth units='$bw_units'>$bw</bandwidth>
        <centre_frequency units='$cfreq_units'>$cfreq</centre_frequency>
    </signal_parameters>
    <pfb_parameters>
        <oversampling_ratio>$oversampling_ratio</oversampling_ratio>
        <sampling_time units='$tsamp_units'>$tsamp</sampling_time>
        <channel_bandwidth units='$foff_units'>$foff</channel_bandwidth>
        <dual_sideband>$dual_sideband</dual_sideband>
        <resolution>$resolution</resolution>
    </pfb_parameters>
    <observation_parameters>
        <observer>$observer</observer>
        <tobs>$tobs</tobs>
    </observation_parameters>
    <boresight_parameters>
        <project_id>$bs_pid</project_id>
        <name epoch='$bs_epoch'>$bs_source_name</name>
        <ra units='$bs_ra_units'>$bs_ra</ra>
        <dec units='$bs_dec_units'>$bs_dec</dec>
        <rfi_mitigation>$rfi_mitigation</rfi_mitigation>
        <antenna_weights>$antenna_weights</antenna_weights>
        <delay_tracking>$delay_tracking</delay_tracking>
        <processing_file>$bs_proc_file</processing_file>
    </boresight_parameters>
    <fan_beams_parameters>
      <project_id>$fb_pid</project_id>
      <mode>$fb_mode</mode>
      <nbeams>$fb_nbeams</nbeams>
      <beam_spacing units='$fb_beam_spacing_units'>$fb_beam_spacing</beam_spacing>
    </fan_beams_parameters>
"""
)

# Resolution units missing and project id does not match (P000 and P999)?
MPSR_NOFB_PARAMS = Template(
    """<west_arm_parameters>
      <tracking>$west_tracking</tracking>
      <ns_tilt units='$west_ns_tilt_units'>$west_ns_tilt</ns_tilt>
      <md_angle units='$west_md_angle_units'>$west_md_angle</md_angle>
   </west_arm_parameters>
   <east_arm_parameters>
      <tracking>$east_tracking</tracking>
      <ns_tilt units='$east_ns_tilt_units'>$east_ns_tilt</ns_tilt>
      <md_angle units='$east_md_angle_units'>$east_md_angle</md_angle>
   </east_arm_parameters>
   <signal_parameters>
        <nchan>$nchan</nchan>
        <nbit>$nbits</nbit>
        <ndim>$ndim</ndim>
        <npol>$npol</npol>
        <nant>$nant</nant>
        <bandwidth units='$bw_units'>$bw</bandwidth>
        <centre_frequency units='$cfreq_units'>$cfreq</centre_frequency>
    </signal_parameters>
    <pfb_parameters>
        <oversampling_ratio>$oversampling_ratio</oversampling_ratio>
        <sampling_time units='$tsamp_units'>$tsamp</sampling_time>
        <channel_bandwidth units='$foff_units'>$foff</channel_bandwidth>
        <dual_sideband>$dual_sideband</dual_sideband>
        <resolution>$resolution</resolution>
    </pfb_parameters>
    <observation_parameters>
        <observer>$observer</observer>
        <tobs>$tobs</tobs>
    </observation_parameters>
    <boresight_parameters>
        <project_id>$bs_pid</project_id>
        <name epoch='$bs_epoch'>$bs_source_name</name>
        <ra units='$bs_ra_units'>$bs_ra</ra>
        <dec units='$bs_dec_units'>$bs_dec</dec>
        <rfi_mitigation>$rfi_mitigation</rfi_mitigation>
        <antenna_weights>$antenna_weights</antenna_weights>
        <delay_tracking>$delay_tracking</delay_tracking>
        <processing_file>$bs_proc_file</processing_file>
    </boresight_parameters>
"""
)

MPSR_TB_PARAMS = Template(
    """<tied_beam_0_parameters>
        <project_id>$tb0_pid</project_id>
        <mode>$tb0_mode</mode>
        <processing_file>$tb0_proc_file</processing_file>
        <name epoch='$tb0_epoch'>$tb0_source_name</name>
        <ra units='$tb0_ra_units'>$tb0_ra</ra>
        <dec units='$tb0_dec_units'>$tb0_dec</dec>
</tied_beam_0_parameters>"""
)

MPSR_MODTB_PARAMS = Template(
    """<mod_beams_parameters>
        <mode>$tb0_mode</mode>
        <project_id>$tb0_pid</project_id>
</mod_beams_parameters>"""
)

MPSR_CORR_PARAMS = Template(
    """<correlation_parameters>
       <mode>$corr_mode</mode>
       <project_id>$tb0_pid</project_id>
       <type>$type_mode</type>
       <processing_file>$bf_proc_file</processing_file>
       <dump_time units='$dumptime_units'>$dump_time</dump_time>
</correlation_parameters>"""
)

MPSR_PARAM_DEFAULTS = dict(
    bs_epoch="J2000",
    tb0_epoch="J2000",
    position_epoch="J2000",
    position_error="0.000001",
    position_error_units="degrees",
    bs_source_name="J0835-4510",
    tb0_source_name="J0835-4510",
    bs_ra="08:35:20.61149",
    tb0_ra="08:35:20.61149",
    bs_dec="-45:10:34.8751",
    tb0_dec="-45:10:34.8751",
    bs_ra_units="hh:mm:ss",
    tb0_ra_units="hh:mm:ss",
    bs_dec_units="hh:mm:ss",
    tb0_dec_units="hh:mm:ss",
    nbits="8",
    ndim="2",
    npol="1",
    nant="8",
    nchan="320",
    bw="31.25",
    bw_units="MHz",
    cfreq="835.5957031",
    cfreq_units="MHz",
    foff="0.09765625",
    foff_units="MHz",
    tsamp="10.24",
    tsamp_units="microseconds",
    oversampling_ratio="1",
    dual_sideband="1",
    resolution="5120",
    rfi_mitigation="true",
    antenna_weights="true",  # should the antenna weights be disabled?
    delay_tracking="true",
    bs_proc_file="mopsr.aqdsp.hires.gpu",
    tb0_proc_file="mopsr.dspsr.cpu",
    fb_nbeams="352",
    fb_beam_spacing="0.01139601",
    fb_beam_spacing_units="degrees",
    observer="AJ",
    bs_pid="P000",
    tb0_pid="P000",
    fb_pid="P000",
    tb="true",  # (newly added for TB_PARAMS)
    modtb="true",
    tobs="-1",
    tb0_mode="PSR",
    fb_mode="PSR",
    west_tracking="true",
    east_tracking="true",
    west_md_angle="0.0",
    west_ns_tilt="0.0",
    west_ns_tilt_units="degrees",
    west_md_angle_units="degrees",
    east_md_angle="0.0",
    east_ns_tilt="0.0",
    east_ns_tilt_units="degrees",
    east_md_angle_units="degrees",
    corr_mode="CORR",
    type_mode="FX",
    bf_proc_file="mopsr.calib.hires.pref16.gpu",
    dumptime_units="seconds",
    dump_time="60",
)


MPSR_NOFB_PARAM_DEFAULTS = dict(
    bs_epoch="J2000",
    tb0_epoch="J2000",
    position_epoch="J2000",
    position_error="0.000001",
    position_error_units="degrees",
    bs_source_name="J0835-4510",
    tb0_source_name="J0835-4510",
    bs_ra="08:35:20.61149",
    tb0_ra="08:35:20.61149",
    bs_dec="-45:10:34.8751",
    tb0_dec="-45:10:34.8751",
    bs_ra_units="hh:mm:ss",
    tb0_ra_units="hh:mm:ss",
    bs_dec_units="hh:mm:ss",
    tb0_dec_units="hh:mm:ss",
    nbits="8",
    ndim="2",
    npol="1",
    nant="8",
    nchan="320",
    bw="31.25",
    bw_units="MHz",
    cfreq="835.5957031",
    cfreq_units="MHz",
    foff="0.09765625",
    foff_units="MHz",
    tsamp="10.24",
    tsamp_units="microseconds",
    oversampling_ratio="1",
    dual_sideband="1",
    resolution="5120",
    rfi_mitigation="true",
    antenna_weights="false",  # should the antenna weights be disabled?
    delay_tracking="true",
    bs_proc_file="mopsr.aqdsp.hires.gpu",
    tb0_proc_file="mopsr.dspsr.cpu",
    observer="AJ",
    bs_pid="P000",
    tb0_pid="P000",
    tb="true",  # (newly added for TB_PARAMS)
    modtb="true",
    tobs="-1",
    tb0_mode="PSR",
    west_tracking="true",
    east_tracking="true",
    west_md_angle="0.0",
    west_ns_tilt="0.0",
    west_ns_tilt_units="degrees",
    west_md_angle_units="degrees",
    east_md_angle="0.0",
    east_ns_tilt="0.0",
    east_ns_tilt_units="degrees",
    east_md_angle_units="degrees",
    corr_mode="CORR",
    type_mode="FX",
    bf_proc_file="mopsr.calib.hires.pref16.gpu",
    dumptime_units="seconds",
    dump_time="60",
)


####################################
# Talking to am_helpers.py
####################################


def mobs_to_mpsr_driver_encoder(command, parameters=None):
    tb_str = ""
    modtb_str = ""
    corr_str = ""

    if parameters is None:
        parameters = {}

    if command == "prepare":
        new_parameters = MPSR_PARAM_DEFAULTS.copy()
        new_parameters.update(parameters)
        modtb = new_parameters["modtb"]
        tb = new_parameters["tb"]
        corr = new_parameters["corr"]

        if modtb == "true":
            modtb_parameters = MPSR_NOFB_PARAM_DEFAULTS.copy()
            modtb_parameters.update(parameters)
            param_str = MPSR_NOFB_PARAMS.substitute(modtb_parameters)
            modtb_str = MPSR_MODTB_PARAMS.substitute(modtb_parameters)

            if tb == "true":
                tb_str = MPSR_TB_PARAMS.substitute(modtb_parameters)

        elif corr == "true":
            corr_params = MPSR_NOFB_PARAM_DEFAULTS.copy()
            corr_params.update(parameters)
            param_str = MPSR_NOFB_PARAMS.substitute(corr_params)
            corr_str = MPSR_CORR_PARAMS.substitute(corr_params)

        else:
            param_str = MPSR_PARAMS.substitute(new_parameters)
            tb = new_parameters["tb"]

            if tb == "true":
                tb_str = MPSR_TB_PARAMS.substitute(new_parameters)
    else:
        param_str = ""

    message = MPSR_MESSAGE.substitute(
        command=command,
        parameters=param_str,
        tb_params=tb_str,
        modtb_params=modtb_str,
        corr_params=corr_str,
    )

    message = message.replace("\n", "") + "\r\n"  # for Andrew's parser

    return message
