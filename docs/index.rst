.. Molonglo Observing Software documentation master file, created by
   sphinx-quickstart on Wed Jan 24 14:34:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Molonglo Observing Software's documentation!
=======================================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    README
    grammar
    api
    changes
    todo

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
