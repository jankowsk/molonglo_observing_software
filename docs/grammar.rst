****************************
Grammar in the schedule file
****************************

Schedule files contain one command per line and keywords are semicolon
delimited. All equatorial coordinates (RA, Dec) need to be given in J2000
format in hh:mm:ss notation, of which the seconds are optional. Blank lines and
lines starting with a hash sign (#) are ignored. All commands are case
insensitive.

Telescope observing modes
=========================

In the sections below, `$type` has one of the following values:

+-----------------------+------------------------------------+
|  Value                |   Meaning                          |
+=======================+====================================+
|  tracking             |  Telescope tracks an object both   |
|                       |  physically and electronically.    |
+-----------------------+------------------------------------+
|  transiting           |  Backend tracks object transiting  |
|                       |  through primary beam              |
|                       |  electronically.                   |
+-----------------------+------------------------------------+
|  stationary           |  Synthesised beam is stationary.   |
+-----------------------+------------------------------------+

Telescope movement modes
------------------------

The following three movement modes of the telescope are supported:

+----------------------+------------------------------------+
|  Command             |   Meaning                          |
+======================+====================================+
|   track; $ra; $dec   |   Track given ra and dec.          |
+----------------------+------------------------------------+
|   posn; $ra; $dec    |   Position at given ra, dec, but   |
|                      |   do not track.                    |
+----------------------+------------------------------------+
|   park               |   Move telescope to park position. |
+----------------------+------------------------------------+

Pulsar observing modes
----------------------

A multitude of pulsar observing modes are available, with the `fold*` modes
combining pulsar tracking (`track` mode in `tracking` configuration) and folded
tied-array beam observing in a single line for convenience, compared with their
`tb*` counterparts.

+-----------------------------------------+------------------------------------+
|  Command                                |   Meaning                          |
+=========================================+====================================+
|   indiv; $type; $jname; $tmax           |   Start backend in folded          |
|                                         |   individual module mode.          |
+-----------------------------------------+------------------------------------+
|   tb; $type; $jname; $tmax              |   Start backend in tied-array      |
|                                         |   beam mode.                       |
+-----------------------------------------+------------------------------------+
|   tbsnr; $type; $jname; $snr; $tmin;    |   Same as foldsnr, but without     |
|   $tmax                                 |   physical tracking the source.    |
+-----------------------------------------+------------------------------------+
|   tbtoa; $type; $jname; $tmin; $tmax    |   Same as foldtoa, but without     |
|                                         |   physical tracking the source.    |
+-----------------------------------------+------------------------------------+
|   modtb; $type; $name; $tmax            |   Observe in filterbank individual |
|                                         |   module plus tied-array beam      |
|                                         |   mode.                            |
+-----------------------------------------+------------------------------------+
|   fold; $jname; $tmax                   |   Observe pulsar (combines track   |
|                                         |   and tb).                         |
+-----------------------------------------+------------------------------------+
|   foldsnr; $jname; $snr; $tmin; $tmax   |   Observe pulsar for at least      |
|                                         |   $tmin and maximum $tmax          |
|                                         |   seconds until the S/N            |
|                                         |   $snr is reached (combines track  |
|                                         |   and tb).                         |
+-----------------------------------------+------------------------------------+
|   foldtoa; $jname; $tmin; $tmax         |   Time pulsar until saturation     |
|                                         |   and for at least $tmin and       |
|                                         |   maximum $tmax seconds.           |
+-----------------------------------------+------------------------------------+
|   foldint; $jname; $tmin; $tmax         |   Observe intermittent pulsar for  |
|                                         |   $tmin seconds. If it is on,      |
|                                         |   observe it further until $tmax   |
|                                         |   is reached.                      |
+-----------------------------------------+------------------------------------+

FRB search modes
----------------

+-----------------------------------------+------------------------------------+
|  Command                                |   Meaning                          |
+=========================================+====================================+
|   fb; $type; $name; $ra; $dec; $tmax    |   Do a fan beam FRB search at the  |
|                                         |   given ra and dec.                |
+-----------------------------------------+------------------------------------+

Synthesis imaging modes
-----------------------

+-----------------------------------------+------------------------------------+
|  Command                                |   Meaning                          |
+=========================================+====================================+
|   corr; $type; $name; $ra, $dec, $tmax  |   Do a correlation observation of  |
|                                         |   an object at ra and dec.         |
+-----------------------------------------+------------------------------------+
|   map; $name; $ra; $dec; $tmax          |   Map an object at ra, dec         |
|                                         |   (combines track and corr).       |
+-----------------------------------------+------------------------------------+

Wait modes
----------

+----------------------------------+------------------------------------+
|  Command                         |   Meaning                          |
+==================================+====================================+
|   wait; sec; $seconds            |   Wait for a number of seconds.    |
+----------------------------------+------------------------------------+
|   wait; lst; $lst                |   Wait until a given LST in        |
|                                  |   hh:mm:ss notation. If the LST is |
|                                  |   over for the current 24 hour     |
|                                  |   LST range, automatically wrap    |
|                                  |   to the next one.                 |
+----------------------------------+------------------------------------+
|   wait; transit; $jname; $offset |   Wait until transit of a given    |
|                                  |   pulsar. The $offset from the     |
|                                  |   transit is given in seconds and  |
|                                  |   can be both negative (earlier)   |
|                                  |   and positive (later).            |
+----------------------------------+------------------------------------+

Example usage
=============

Create a schedule file `test.sch` in your current working directory. You can
use all the commands above. Also, have a look at the example schedules below.
Then, test the schedule file using:

.. code-block:: console

    automatic_mode.py -t test.sch

This will not do anything, it will just parse the schedule file and check it
for errors. If there are no errors displayed, you can run the schedule file
like this:

.. code-block:: console

    automatic_mode.py test.sch

This will execute the commands in the schedule file on the telescope and
start the observation.

Example schedules
=================

1) Observe the Vela pulsar for 10 minutes, then park the telescope:

.. code-block:: none

    fold; J0835-4510; 600
    park

2) Wait for 30 minutes, then do a drift scan on J1644 and park the telescope:

.. code-block:: none

    wait; sec; 1800
    posn; 16:14:49.281; -45:59:09.5
    fold; J1644-4559; 1800
    park

3) Wait until 16:00 LST then observe J1644 for one hour:

.. code-block:: none

    wait; lst; 16:00
    fold; J1644-4559; 3600

4) Wait until 30 minutes before Vela transit, then observe it for one hour:

.. code-block:: none

    wait; transit; J0835-4510; -1800
    fold; J0835-4510; 3600

5) Observe the CJ1218-4600 quasar for 20 minutes around transit:

.. code-block:: none

    wait; lst; 12:08:06
    corr; transiting; CJ1218-4600; 12:18:06.0; -46:00:29.2; 1200

