#!/usr/bin/env bash
#
# Add version string from git into code. 
# 2018 - 2019 Fabian Jankowski
#

VERSIONFILE="molsoft/version.py"

version=$(git describe --long --dirty)

sed -i '' -e 's/\$Revision\$/'"${version}"'/g' ${VERSIONFILE}
