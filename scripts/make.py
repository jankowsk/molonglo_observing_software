#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   2016-2018 Fabian Jankowski
#   Substitute the current version for production.
#

from __future__ import print_function
import glob
import os.path
import shlex
import shutil
import subprocess


def get_version():
    """
    Get the current version from git.
    """

    command = "git describe --long --dirty"

    args = shlex.split(command)

    version = subprocess.check_output(args)
    version = version.strip()

    return version


def substitute_keyword(filename, keyword, substitute):
    """
    Substitute keyword with replacement in file.
    """

    with open(filename, "r") as f:
        lines = f.read()

    if keyword in lines:
        lines = lines.replace(keyword, substitute)

    with open(filename, "w") as f:
        f.write(lines)


#
# MAIN
#


def main():
    this_file = os.path.basename(__file__)
    version = get_version()

    print("Version: {0}".format(version))

    keyword = r"$Revision$"
    proddir = "production_temp"

    files = glob.glob("*.py")
    files = sorted(files)

    if this_file in files:
        files.remove(this_file)

    # copy files to production dir
    if not os.path.exists(proddir):
        os.mkdir(proddir)

    for item in files:
        shutil.copy(item, os.path.join(proddir, item))

    for item in files:
        substitute_keyword(os.path.join(proddir, item), keyword, version)

    print("All done.")


# if run directly
if __name__ == "__main__":
    main()
