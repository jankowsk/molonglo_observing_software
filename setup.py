import os.path
from setuptools import Extension, find_packages, setup


def get_version():
    version_file = os.path.join(os.path.dirname(__file__), "molsoft", "version.py")

    with open(version_file, "r") as f:
        raw = f.read()

    items = {}
    exec(raw, None, items)

    return items["__version__"]


def get_long_description():
    with open("README.md", "r") as fd:
        long_description = fd.read()

    return long_description


setup(
    name="molsoft",
    version=get_version(),
    description="Observing software for the Molonglo Observatory Synthesis Radio Telescope (MOST).",
    long_description=get_long_description(),
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/jankowsk/molonglo_observing_software/",
    author="Fabian Jankowski",
    author_email="fjankowsk at gmail.com",
    license="MIT",
    packages=find_packages(),
    package_data={
        "molsoft": ["config/*.yml"],
    },
    dependency_links=["https://github.com/ewanbarr/anansi/master"],
    install_requires=[
        "lxml",
        "matplotlib>=1.4",
        "numpy>=1.9",
        "pandas",
        "pyephem>=3.7.6",
        "pyyaml",
    ],
    extras_require={
        "develop": [
            "black",
            "pytest",
            "pytest-cov",
        ],
        "docs": ["myst-parser", "sphinx", "sphinx_rtd_theme"],
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    zip_safe=False,
)
