BLK         =   black
MAKE        =   make
PIP         =   pip
SPHINXBUILD =   sphinx-build

BASEDIR     =   $(CURDIR)
SRCDIR      =   ${BASEDIR}/molsoft
DOCDIR      =   ${BASEDIR}/docs
BUILDDIR    =   ${DOCDIR}/_build
TESTDIR     =   ${BASEDIR}/tests

help:
	@echo 'Makefile for Molonglo observing software'
	@echo 'Usage:'
	@echo 'make black           reformat the code using black code formatter'
	@echo 'make clean           remove the production copies'
	@echo 'make docs            produce documentation'
	@echo 'make install         install the package locally'
	@echo 'make production      produce local installation'
	@echo 'make test            run the regression tests'
	@echo 'make all             produce production copy and documentation'

black:
	${BLK} *.py */*.py

clean:
	rm -f ${SRCDIR}/*.pyc
	rm -rf ${SRCDIR}/__pycache__
	rm -f ${TESTDIR}/*.pyc
	rm -rf ${TESTDIR}/__pycache__
	rm -rf build
	rm -rf dist
	rm -rf molsoft.egg-info
	rm -rf ${BUILDDIR}

docs:
	${SPHINXBUILD} -M dirhtml ${DOCDIR} ${BUILDDIR}

install:
	${MAKE} clean
	${PIP} install .
	${MAKE} clean

production:
	scripts/add_version_string.sh
	pip install -e .

test:
	pytest --verbose

all: test production docs

.PHONY: help black clean docs install production test all
