# Molsoft: Molonglo Telescope Observing Software #

[![Documentation](https://readthedocs.org/projects/molsoft/badge/?version=latest)](https://molsoft.readthedocs.io/en/latest/?badge=latest)
[![License - MIT](https://img.shields.io/badge/license-MIT-green.svg)](https://bitbucket.org/jankowsk/molonglo_observing_software/src/master/LICENSE)
[![Paper link](https://img.shields.io/badge/paper-10.1093/mnras/sty3390-blue.svg)](https://doi.org/10.1093/mnras/sty3390)
[![arXiv link](https://img.shields.io/badge/arXiv-1812.04038-blue.svg)](https://arxiv.org/abs/1812.04038)

This repository contains high-level observing software used at the refurbished Molonglo Observatory Synthesis Radio Telescope (MOST), that was developed as part of the [UTMOST project](http://astronomy.swin.edu.au/research/utmost/).

## Authors ##

The software is primarily developed by Fabian Jankowski. For more information feel free to contact me via: fabian.jankowski at manchester.ac.uk.

Further authors are:

* Aditya Parthasarathy
* Wael Farah
* Chris Flynn

## Citation ##

If you make use of the software, please add a link to this repository and cite our paper: [Jankowski et al. 2019](https://ui.adsabs.harvard.edu/abs/2019MNRAS.484.3691J)

For the BibTeX citation, see the CITATION.bib file.

## Requirements ##

* [PSRCHIVE](http://psrchive.sourceforge.net/) and its Python bindings
* [ATNF pulsar catalogue](http://www.atnf.csiro.au/research/pulsar/psrcat/) (>= 1.54)

All the other requirements are automatically installed via Python's `pip`.

## Installation ##

The software is packaged as a Python module, so it can be installed via `pip`. You can also install a local version from the git checkout by running `make production`.

## Documentation ##

Online documentation for users and an API reference for developers is available on the following website: [online documentation](https://molsoft.readthedocs.io/en/latest/).
