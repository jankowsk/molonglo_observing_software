# Version history #

## HEAD ##

* Corrected list of required external Python modules.
* Further formatting improvements of the code.

## 2019-07-11 ##

* Improve packaging of the module. Add requirements to setup file.
* Remove unused code.
* Store configuration parameters in a central YAML configuration file.
* Store the version string in central place.
* Cleaned up and sorted imports.
* Converted the code to a proper Python module.
* First steps for Python 3 compatibility.
* Further code clean-up to satisfy `pycodestyle` requirements. Removal of unused code.
* Addition of unit tests using `nose2` for some of the helper functions. This has discovered some bugs in the schedule file parsing and processing.

## 2018-01-29 ##

* Switch to transit operation of the telescope. The transit scheduler logic is much simpler than the original implementation and still requires significant work. Transit mode has become the default operation mode in all software components.
* Documentation was added both for users and an API reference for developers. Both are generated using `sphinx`.
* Extensive docstring cleanup to produce API documentation.
* Clean up code so that it is compatible with PEP8 style.
* Convert tabs to 4 spaces and unify formatting.
* Make code pass the most important style checks in `pycodecheck`.
* Switch to date-based software versions.

## v1.1.1 (2016-10-18) ##

* Drastic improvement in path evaluation performance (about a factor of 3).
* First stab at an async scoring process. Not hooked up to dynamic mode yet.

## v1.1 (2016-09-19) ##

* Better cadence and slewtime weights for the scores in the scheduler.
* Various bug fixed in `automatic_mode` and `scheduler`.
* Merged branch "dirtydb":
    * `snr_extractor_server` writes a dirty (as in live at the telescope) database that is used in `scheduler` and `make_schedule` to keep track of cadence-based target selection and scoring.
* Merged branch "newstrat":
    * Implemented new database-based observing strategy.
    * Properly honour observing strategy in `scheduler` and `make_schedule`.
* Make `automatic_mode` functions more modular and convert to a library.
* Merged branch "multithreadautomaticmode".
    * `automatic_mode` was split into client and server. The stand-alone version is also available.
    * `automatic_mode_server` is fully multi-threaded now.
    * `automatic_mode_server`, `client` and `scheduler` can communicate via TCP sockets.
    * Various small changes to the `scheduler`.

## v1.0 (2016-04-29) ##

* First stable and feature-complete version. This includes the stand-alone single-threaded `automatic_mode`.
